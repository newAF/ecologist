<?php 
    require "include/db.php"
 ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Довідка.Ecologist</title>
<script>document.write('<script src="http://' + (location.host || 'localhost').split(':')[0] + ':35729/livereload.js?snipver=1"></' + 'script>')</script>
<meta name="keywords" content="green home, contact form, contact page, submit button, CSS, HTML" />
<meta name="description" content="Green Home Theme - Contact Page, Contact Form, Submit Button" />
<link href="templatemo_style.css" rel="stylesheet" type="text/css" />

</head>
<body>

<div id="templatemo_document_wrapper">
<div id="templatemo_wrapper">

	<?php include'include/menu_contact.php' ?>
    
	<div id="templatemo_header">
        <div id="site_title">
	        <h1><img src="images/templatemo_logo.png" alt="Logo" /></h1>
        </div> <!-- end of site_title -->    
        <div id="header_content">
                <p>
                    "Головна схильність людини спрямована на те, що відповідає природі."<br>
                    <em><span class="autor_1">-Цицерон-</span></em>  </p>
                
            </div>
    </div>
    
    <div id="templatemo_main">
    
		
		
        <div class="cleaner_h30"></div>
        
        <div class="col_w450">
        	<div id="contact_form">

                <h4><strong> Quick Contact Form</strong></h4>
                
           	  	<form method="post" name="contact" action="#">

					<label for="author">Имя:</label> <input type="text" class="required input_field" name="author" id="author" />
					<div class="cleaner_h10"></div>
							
					<label for="email">Email:</label> <input type="text" class="validate-email required input_field" name="email" id="email" />
					<div class="cleaner_h10"></div>
					
			<label for="subject">Название сообщения:</label> <input type="text" class="validate-subject required input_field" name="subject" id="subject"/>				               
					<div class="cleaner_h10"></div>
	
					<label for="text">Сообщение:</label> <textarea id="text" name="text" rows="0" cols="0" class="required"></textarea>
					<div class="cleaner_h10"></div>                
						
					<input type="submit" value="Отправить" id="submit" name="submit" class="submit_btn float_l" />
					<input type="reset" value="Очистить" id="reset" name="reset" class="submit_btn float_r" />
                    
              	</form>
			  
              	<div class="cleaner"></div>
            
            </div>
        </div>
        
        <div class="col_w450 last_box">
		
			<h4>Поштова адреса</h4>

            <h6>Location One</h6>
            <br /><br />
			
            Tel: 000-000-0000<br />
			Fax: 080-080-0080<br />

			<div class="cleaner_h30"></div>
            
            <h6>Location Two</h6>
            <br /><br />
			
			Tel: 000-000-0000<br />
			Fax: 000-000-0000<br />

        </div>
        <script type="text/javascript">
            <div class="pd-embed" id="pd_1486031092"></div>
<script type="text/javascript">
  var _polldaddy = [] || _polldaddy;

  _polldaddy.push( {
    type: "iframe",
    auto: "1",
    domain: "newlifeeee.polldaddy.com/s/",
    id: "%D1%82%D0%B5%D1%81%D1%82",
    placeholder: "pd_1486031092"
  } );

  (function(d,c,j){if(!document.getElementById(j)){var pd=d.createElement(c),s;pd.id=j;pd.src=('https:'==document.location.protocol)?'https://polldaddy.com/survey.js':'http://i0.poll.fm/survey.js';s=document.getElementsByTagName(c)[0];s.parentNode.insertBefore(pd,s);}}(document,'script','pd-embed'));
</script>
        </script>
                
        <div class="cleaner"></div>
	</div>
</div> <!-- end of wrapper -->
</div>

<div id="templatemo_footer_wrapper">
    <div id="templatemo_footer">
    
        <a href="index.php">Головна</a> | <a href="lectures.php">Лекції</a> | <a href="lab_works.php">Лабораторні роботи</a> | <a href="other.php">Інше</a> | <a href="contact.php">Довідка</a><br /><br />

        ХДУ © 2017 <a href="http://ksuonline.kspu.edu/?lang=ru">KSU Online</a> | NewLife <a href="http://www.kspu.edu/default.aspx?lang=uk" target="_parent">ХДУ</a>
    
    </div> <!-- end of templatemo_footer -->
</div>

</body>
</html>