<?php 
	require "../include/db_2.php";
	$lvl_a = $_SESSION['logged_user']->lvl_a;
	if ($lvl_a <= 1) {
		header('location: /errors/404.php');
	}
	if (empty($_SESSION['logged_user'])) {
 	  	header('location: /errors/404.php');
 	}
?>

 <!DOCTYPE html PUBLIC>
<html>
<head>
<meta name="viewport" content='width=1000' />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Адмінка. Ecologist</title>
<script>document.write('<script src="http://' + (location.host || 'localhost').split(':')[0] + ':35729/livereload.js?snipver=1"></' + 'script>')</script>
<meta name="keywords" content="green home website, free web template, free templates, CSS, HTML" />
<meta name="description" content="Green Home - free HTML CSS template by templatemo.com" />
<link rel="stylesheet" type="text/css" href="style_a.css">
</head>
<body>
	<div class="header">
		<div class="center">
			<ul>
			<li><a href="/index.php">Головна</a></li>
			<li><a href="/admin/test_a.php">Тести</a></li>
			<li><a href="/admin/rating_a.php">Рейтинг</a></li>
			<li><a href="/admin/other_a.php">Інше</a></li>
		</div>
		<span class="prof"><?php echo $_SESSION['logged_user']->name;?><a href="/logout.php"><img src="/images/logout.png"></a></span>
	</div>
	<div class="menu">
		
	</div>
	<div class="footer">
		
	</div>
</body>
</html>