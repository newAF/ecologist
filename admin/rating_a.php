<?php 
	require "../include/db_2.php";
	$lvl_a = $_SESSION['logged_user']->lvl_a;
	if ($lvl_a <= 1) {
		header('location: /errors/404.php');
	}
	if (empty($_SESSION['logged_user'])) {
 	  	header('location: /errors/404.php');
 	}?>
 <?php 
 		$data = $_POST;
 		$search_group = 0;
 		if (isset($data['search'])) {
 			$search_group = $data['search_group'];
 		}
		$result = R::getAll("SELECT * FROM `rating` WHERE id_group='$search_group'");
		//$result = R::getAll("SELECT * FROM `rating` WHERE number_book='$search_group'");
		$result1 = R::getAll("SELECT * FROM `rating`");
 ?>
 <!DOCTYPE html PUBLIC>
<html>
<head>
<meta name="viewport" content='width=1000' />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Адмінка. Ecologist</title>
<script>document.write('<script src="http://' + (location.host || 'localhost').split(':')[0] + ':35729/livereload.js?snipver=1"></' + 'script>')</script>
<meta name="keywords" content="green home website, free web template, free templates, CSS, HTML" />
<meta name="description" content="Green Home - free HTML CSS template by templatemo.com" />
<link rel="stylesheet" type="text/css" href="style_a.css">
</head>
<body>
	<div class="header">
		<div class="center">
			<ul>
			<li><a href="/index.php">Головна</a></li>
			<li><a href="/admin/test_a.php">Тести</a></li>
			<li><a href="/admin/rating_a.php">Рейтинг</a></li>
			<li><a href="/admin/other_a.php">Інше</a></li>
		</ul>
		</div>
		<span class="prof"><?php echo $_SESSION['logged_user']->name;?><a href="/logout.php"><img src="/images/logout.png"></a></span>
	</div>
	<div class="menu">
		<div class="title">Рейтинг
			<div class="search_group">
				<form action="" method="post">
					<input type="text" name="search_group" placeholder="Група або № з.к.">
					<button name="search" class="search">Пошук</button>
				</form>
			</div>
		</div>

		<div class="main">
			<form action="" method="POST">
				<table class="table" cellspacing="10">
				<thead>
					<tr>
						<td>№ Тесту</td>
						<td>Назва тесту</td>
						<td>Им'я Прізвище</td>
						<td>Оцінка</td>
						<td>Група</td>
						<td>№ залікової книжки</td>
					</tr>
				</thead>
				<?php 
					if (isset($data['search'])) {
						for ($i=0; $i < count($result); $i++) { 
						echo '
							<tbody>
								<tr>
									<td>' . $result[$i]["number_test"] . '</td>
									<td>' . $result[$i]["title"] . '</td>
									<td>' . $result[$i]["name"] . ' ' . $result[$i]["surname"] . '</td>
									<td>' . $result[$i]["rating"] . '</td>
									<td>' . $result[$i]["id_group"] . '</td>
									<td>' . $result[$i]["number_book"] . '</td>
						 		</tr>
							</tbody>';
					} 
					} else
						for ($i=0; $i < count($result1); $i++) { 
							echo '
								<tbody>
									<tr>
										<td>' . $result1[$i]["number_test"] . '</td>
										<td>' . $result1[$i]["title"] . '</td>
										<td>' . $result1[$i]["name"] . ' ' . $result1[$i]["surname"] . '</td>
										<td>' . $result1[$i]["rating"] . '</td>
										<td>' . $result1[$i]["id_group"] . '</td>
										<td>' . $result1[$i]["number_book"] . '</td>
							 		</tr>
								</tbody>';
						}
				 ?>	
				 
			</table>
			</form>
		</div>
	</div>
	<div class="footer">
		
	</div>
</body>
</html>