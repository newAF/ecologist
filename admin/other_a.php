<?php 
	require "../include/db_2.php";
	$lvl_a = $_SESSION['logged_user']->lvl_a;
	if ($lvl_a <= 1) {
		header('location: /errors/404.php');
	}
	if (empty($_SESSION['logged_user'])) {
 	  	header('location: /errors/404.php');
 }?>
<!DOCTYPE html PUBLIC>
<html>
<head>
<meta name="viewport" content='width=1000' />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Адмінка. Ecologist</title>
<script>document.write('<script src="http://' + (location.host || 'localhost').split(':')[0] + ':35729/livereload.js?snipver=1"></' + 'script>')</script>
<meta name="keywords" content="green home website, free web template, free templates, CSS, HTML" />
<meta name="description" content="Green Home - free HTML CSS template by templatemo.com" />
<link rel="stylesheet" type="text/css" href="style_a.css">
</head>
<body>
	<div class="header">
		<div class="center">
			<ul>
			<li><a href="/index.php">Головна</a></li>
			<li><a href="/admin/test_a.php">Тести</a></li>
			<li><a href="/admin/rating_a.php">Рейтинг</a></li>
			<li><a href="/admin/other_a.php">Інше</a></li>
		</ul>
		</div>
		<span class="prof"><?php echo $_SESSION['logged_user']->name;?><a href="/logout.php"><img src="/images/logout.png"></a></span>
	</div>
	<div class="menu">
		<div class="title">Внесення списку студентів</div>
		<div class="menu_other">
<?php 
 	$data = $_POST;
 	if (isset($data['send'])) {

 		$errors = array();
 		if ($data['group'] == '')  {
 			$errors[] = "<strong>Поле 'Група' не може бути пустим!</strong>";
 		}
 		if ($data['name'] == '')  {
 			$errors[] = "<strong>Поле 'Прізвище Ім'я' не може бути пустим!</strong>";
 		}
 		if ($data['number_book'] == '')  {
 			$errors[] = "<strong>Поле '№ залікової книжки' не може бути пустим!</strong>";
 		}
 		if (R::count('students', "number_book = ?", array($data['number_book'])) > 0 ) {
            $errors[] = '<strong>Студент з таким номером залікової книжки вже існує!</strong>';
        }
 		if (empty($errors)) {
 			$info = R::dispense('students');
        	$info->group = $data['group'];
        	$info->name = $data['name'];
        	$info->number_book = $data['number_book'];
 
        	R::store($info);
 		} else 
 		{
 			echo '<div style="color: red;">'.array_shift($errors).'</div><hr>';
 		}
 	}
?>
<?php 
	$result = R::getAll("SELECT * FROM `students` ORDER BY id DESC");

	function resultToArray ($result) {
		$array = array ();
		while (($row = $result->fetch_assoc()) != false) 
			$array[] = $row;
		return $array;
	}
?>
			<form action="" method="post">
			<table for="id_group">Група:</table>
			<input type="text" name="group" placeholder="Група">
			<table for="id_group">Прізвище Ім'я:</table>
			<input type="text" name="name" placeholder="Прізвище Ім'я">
			<table for="id_group">Номер залікової книжки:</table>
			<input type="text" name="number_book" placeholder="Номер залікової книжки">
			<button name="send" class="send">Записати</button>
		</form>
		</div>
		<div class="conclusion">
			<form action="" method="POST">
				<table class="table" cellspacing="10">
				<thead>
					<tr>
						<td>Група</td>
						<td>Прізвище, Ім'я</td>
						<td>№ залікової книжки</td>
						<td>Дії</td>
					</tr>
				</thead>
				<?php 
					for ($i=0; $i < count($result); $i++) { 
						echo '
							<tbody>
								<tr>
									<td>' . $result[$i]["group"] . '</td>
									<td>' . $result[$i]["name"] . '</td>
									<td>' . $result[$i]["number_book"] . '</td>
									<td><button class="button_c" name="' . $result[$i]["number_book"] . '">Delete</button></td>
						 		</tr>
							</tbody>';
							$data = $_POST;
							$f = $result[$i]["number_book"];
							if (isset($data[$f])) {
								$delete = R::getAll("DELETE FROM `students` WHERE number_book='$f'");
							}
					}
						
					?>	 
			</table>
			</form>
		</div>
	</div>
	<div class="footer">
		
	</div>
</body>
</html>