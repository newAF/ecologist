<?php 
	require "../include/db_2.php";
	$lvl_a = $_SESSION['logged_user']->lvl_a;
	if ($lvl_a <= 1) {
		header('location: /errors/404.php');
	}
	if (empty($_SESSION['logged_user'])) {
 	  	header('location: /errors/404.php');
 	}
?>
<?php 	
	$date = date("d.m.y / H:m:s");
	if ( isset($_POST['button_o0']) ) {
		$cat = R::load('tests', 1 );
		$cat->status = 'Відкритий';
		R::store($cat);
		$date_o = R::load('tests', 1);
		$date_o->date_o = $date;
		R::store($date_o);
	}  elseif (isset($_POST['button_c0'])) {
		$cat = R::load('tests', 1);
	 	$cat->status = 'Закритий';
	 	R::store($cat);
	 	$date_c = R::load('tests', 1);
		$date_c->date_c = $date;
	 	R::store($date_c);
	}
	if ( isset($_POST['button_o1']) ) {
		$cat = R::load('tests', 2 );
		$cat->status = 'Відкритий';
		R::store($cat);
		$date_o = R::load('tests', 2);
		$date_o->date_o = $date;
		R::store($date_o);
	}  elseif (isset($_POST['button_c1'])) {
		$cat = R::load('tests', 2);
	 	$cat->status = 'Закритий';
	 	R::store($cat);
	 	$date_c = R::load('tests', 2);
		$date_c->date_c = $date;
	 	R::store($date_c);
	}
	if ( isset($_POST['button_o2']) ) {
		$cat = R::load('tests', 3 );
		$cat->status = 'Відкритий';
		R::store($cat);
		$date_o = R::load('tests', 3);
		$date_o->date_o = $date;
		R::store($date_o);
	}  elseif (isset($_POST['button_c2'])) {
		$cat = R::load('tests', 3);
	 	$cat->status = 'Закритий';
	 	R::store($cat);
	 	$date_c = R::load('tests', 3);
		$date_c->date_c = $date;
	 	R::store($date_c);
	}
	if ( isset($_POST['button_o3']) ) {
		$cat = R::load('tests', 4 );
		$cat->status = 'Відкритий';
		R::store($cat);
		$date_o = R::load('tests', 4);
		$date_o->date_o = $date;
		R::store($date_o);
	}  elseif (isset($_POST['button_c3'])) {
		$cat = R::load('tests', 4);
	 	$cat->status = 'Закритий';
	 	R::store($cat);
	 	$date_c = R::load('tests', 4);
		$date_c->date_c = $date;
	 	R::store($date_c);
	}
	if ( isset($_POST['button_o4']) ) {
		$cat = R::load('tests', 5 );
		$cat->status = 'Відкритий';
		R::store($cat);
		$date_o = R::load('tests', 5);
		$date_o->date_o = $date;
		R::store($date_o);
	}  elseif (isset($_POST['button_c4'])) {
		$cat = R::load('tests', 5);
	 	$cat->status = 'Закритий';
	 	R::store($cat);
	 	$date_c = R::load('tests', 5);
		$date_c->date_c = $date;
	 	R::store($date_c);
	}
	if ( isset($_POST['button_o5']) ) {
		$cat = R::load('tests', 6 );
		$cat->status = 'Відкритий';
		R::store($cat);
		$date_o = R::load('tests', 6);
		$date_o->date_o = $date;
		R::store($date_o);
	}  elseif (isset($_POST['button_c5'])) {
		$cat = R::load('tests', 6);
	 	$cat->status = 'Закритий';
	 	R::store($cat);
	 	$date_c = R::load('tests', 6);
		$date_c->date_c = $date;
	 	R::store($date_c);
	}
	if ( isset($_POST['button_o6']) ) {
		$cat = R::load('tests', 7 );
		$cat->status = 'Відкритий';
		R::store($cat);
		$date_o = R::load('tests', 7);
		$date_o->date_o = $date;
		R::store($date_o);
	}  elseif (isset($_POST['button_c6'])) {
		$cat = R::load('tests', 7);
	 	$cat->status = 'Закритий';
	 	R::store($cat);
	 	$date_c = R::load('tests', 7);
		$date_c->date_c = $date;
	 	R::store($date_c);
	}
?>
<?php 
		$result = R::getAll("SELECT * FROM `tests`");

	function resultToArray ($result) {
		$array = array ();
		while (($row = $result->fetch_assoc()) != false) 
			$array[] = $row;
		return $array;
	}
 ?>
 
 <!DOCTYPE html PUBLIC>
<html>
<head>
<meta name="viewport" content='width=1000' />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Адмінка. Ecologist</title>
<script>document.write('<script src="http://' + (location.host || 'localhost').split(':')[0] + ':35729/livereload.js?snipver=1"></' + 'script>')</script>
<meta name="keywords" content="green home website, free web template, free templates, CSS, HTML" />
<meta name="description" content="Green Home - free HTML CSS template by templatemo.com" />
<link rel="stylesheet" type="text/css" href="style_a.css">
</head>
<body>
	<div class="header">
		<div class="center">
			<ul>
			<li><a href="/index.php">Головна</a></li>
			<li><a href="/admin/test_a.php" class="illumination">Тести</a></li>
			<li><a href="/admin/rating_a.php">Рейтинг</a></li>
			<li><a href="/admin/other_a.php">Інше</a></li>
		</div>
		<span class="prof"><?php echo $_SESSION['logged_user']->name;?><a href="/logout.php"><img src="/images/logout.png"></a></span>
	</div>
	<div class="menu">
		<div class="title">ТЕСТИ</div>
		
  
		<div class="main">
			<form action="" method="POST">
				<table class="table" cellspacing="10">
				<thead>
					<tr>
						<td>№ Тесту</td>
						<td>Назва тесту</td>
						<td>Статус</td>
						<td>Дата відкриття</td>
						<td>Дата закриття</td>
						<td>Посилання</td>
						<td>Управління тестами</td>
					</tr>
				</thead>
				<?php 
					for ($i=0; $i < count($result); $i++) { 
						echo '
							<tbody>
								<tr>
									<td>' . $result[$i]["id"] . '</td>
									<td>' . $result[$i]["title"] . '</td>
									<td>' . $result[$i]["status"] . '</td>
									<td>' . $result[$i]["date_o"] . '</td>
									<td>' . $result[$i]["date_c"] . '</td>
									<td><a href="' . $result[$i]["test_url"] . '" class="url">::Тест ' . $result[$i]["id"] . '::</a></td>
									 <td><button class="button_o" name="button_o' . $i . '">Відкрити</button> <button class="button_c" name="button_c' . $i . '">Закрити</button></td>
						 		</tr>
							</tbody>';
						
					}
				 ?>	
				 
			</table>
			</form>
		</div>
	</div>
	<div class="footer">
		
	</div>
</body>
</html>