<?php 
    require "../include/db_2.php";
 ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Інше.Питання що віносяться...</title>
<script>document.write('<script src="http://' + (location.host || 'localhost').split(':')[0] + ':35729/livereload.js?snipver=1"></' + 'script>')</script>
<meta name="keywords" content="green home, contact form, contact page, submit button, CSS, HTML" />
<meta name="description" content="Green Home Theme - Contact Page, Contact Form, Submit Button" />
<link href="../templatemo_style.css" rel="stylesheet" type="text/css" />
<link href="../labs/style_lecture.css" rel="stylesheet" type="text/css" />

</head>
<body>

<div id="templatemo_document_wrapper">
<div id="templatemo_wrapper">

	<?php include '../include/menu/menu_others.php'; ?>
    
	<div id="templatemo_header">
        <div id="site_title">
	        <h1><a href="http://www.templatemo.com"><img src="../images/templatemo_logo.png" alt="Logo" /></a></h1>
        </div> <!-- end of site_title -->    
        <div id="header_content">
        	   <p>"Главная склонность человека направлена на то, что соответствует природе."
                <em><span class="autor_1">Цицерон</span></em></p>
        </div>
    </div>
    
    <div id="templatemo_main">
        <center><span class="zagolovok">Питання, що виносяться на залік з дисципліни «Інформатика та системологія».</span></center></br><hr>
            </span>

        <span class="text">
            <p class="text_ab">
           <div class="vopros">
                <strong><center></center></strong>
            1. Поняття «інформаційна система», «інформаційна технологія».<br>
            2. Складові інформаційних технологій.<br>
            3. Поняття про інформаційні ресурси.<br>
            4. Призначення програмного забезпечення.<br>
            5. Основні функції та призначення операційних систем.<br>
            6. Введення в соціальне значення  комп’ютеризації.<br>
            7. Склад інформаційної системи.<br>
            8. Що таке буфер обміну?<br>
            9. Робота з об’єктами WINDOWS.<br>
            10. Основні операції в оболонці Windows.<br>
            11. Особливості процесору Microsoft Word, його призначення та можливості.<br>
            12. Створення, завантаження та збереження файлів-документів.<br>
            13. Поняття про шаблони і стилі оформлення.<br>
            14. Введення, редагування та форматування тексту.<br>
            15. Включення до тексту різнотипних об’єктів (таблиць, малюнків, діаграм).<br>
            16. Електронні таблиці Microsoft Excel.<br>
            17. Створення, завантаження та збереження файлів-документів (книг).<br>
            18. Розрахункові операції в Excel.<br>
            19. Робота з формулами та функціями.<br>
            20. Основні статистичні та математичні функції Excel.<br>
            21. Додаткові можливості Excel.<br>
            22. Графічний редактор PAINT.<br>
            23. Комп’ютерні мережі. Internet-технології.<br>
            24. Основні поняття та об’єкти програми Microsoft Power Point.<br>
            25. Прийоми створення презентацій. Прийоми оформлення презентацій.<br>
            26. Шаблони презентацій, дизайн слайдів.<br>
            27. Робота з малюнками, текстом, звуком. Анімація об’єктів презентації.<br>
            28. Настроювання зміни слайдів в презентаціях. Настроювання показу слайдів.<br>
            29. Безкоштовні сервіси Web 2.0 для створення презентацій і представлення інформації в мережі інтернет.<br>
            30. Робота з таблицями в базі даних Access. Зв’язування таблиць бази даних.<br>
            31. Схема даних в БД Access. Простий запит. Підсумковий запит.<br>
            32. Фільтрація та сортування даних в базі даних Access.<br>

           </div>
        </p>
        </span>

    </div>
    </div>

<div id="templatemo_footer_wrapper">
    <div id="templatemo_footer">
    
        <a href="index.php">Головна</a> | <a href="lectures.php">Лекції</a> | <a href="lab_works.php">Лабораторні роботи</a> | <a href="other.php">Інше</a> | <a href="contact.php">Довідка</a><br /><br />

        ХДУ © 2017 <a href="http://ksuonline.kspu.edu/?lang=ru">KSU Online</a> | NewLife <a href="http://www.kspu.edu/default.aspx?lang=uk" target="_parent">ХДУ</a>
    
    </div> <!-- end of templatemo_footer -->
</div>

</body>
</html>