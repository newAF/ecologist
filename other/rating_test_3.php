<?php 
	require "../include/db_2.php"
 ?>
 <?php 
 		$data = $_POST;
 		$search_group = 0;
 		if (isset($data['search'])) {
 			$search_group = $data['search_group'];
 		}
		$result = R::getAll("SELECT * FROM `rating` WHERE id_group='$search_group'");
		//$result = R::getAll("SELECT * FROM `rating` WHERE number_book='$search_group'");
		$result1 = R::getAll("SELECT * FROM `rating` WHERE number_test='3'");
 ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><Інше.Ecologist</title>
<script>document.write('<script src="http://' + (location.host || 'localhost').split(':')[0] + ':35729/livereload.js?snipver=1"></' + 'script>')</script>
<meta name="keywords" content="green home, services, free web template, free templates, website templates, CSS, HTML" />
<meta name="description" content="Green Home Services - free css template provided by templatemo.com" />
<link href="/templatemo_style.css" rel="stylesheet" type="text/css" />

</head>
<body>

<div id="templatemo_document_wrapper">
	<div id="templatemo_wrapper">
	
		<?php include'../include/menu/menu_others.php' ?>
		
		<div id="templatemo_header">
			<div id="site_title">
				<h1><img src="/images/templatemo_logo.png" alt="Logo" /></h1>
			</div> <!-- end of site_title -->    
			<div id="header_content">
				<p>
					"Головна схильність людини спрямована на те, що відповідає природі."<br>
					<em><span class="autor_1">-Цицерон-</span></em>  </p>
				
			</div>
		</div>
		
		<div id="templatemo_main">
			<div class="menu">
			<div class="title">Результати тестування. Тест-3
			<div class="search_group">
				<form action="" method="post">
					<input type="text" name="search_group" placeholder="Група або № з.к.">
					<button name="search" class="search">Пошук</button>
				</form>
			</div>
			</div>

			<div class="main">
				<form action="" method="POST">
				<table class="table" cellspacing="10">
				<thead>
					<tr>
						<td>№ Тесту</td>
						<td>Назва тесту</td>
						<td>Им'я Прізвище</td>
						<td>Оцінка</td>
						<td>Група</td>
						<td>№ залікової книжки</td>
					</tr>
				</thead>
				<?php 
					if (isset($data['search'])) {
						for ($i=0; $i < count($result); $i++) { 
						echo '
							<tbody>
								<tr>
									<td>' . $result[$i]["number_test"] . '</td>
									<td>' . $result[$i]["title"] . '</td>
									<td>' . $result[$i]["name"] . '</td>
									<td>' . $result[$i]["rating"] . '</td>
									<td>' . $result[$i]["id_group"] . '</td>
									<td>' . $result[$i]["number_book"] . '</td>
						 		</tr>
							</tbody>';
					} 
					} else
						for ($i=0; $i < count($result1); $i++) { 
							echo '
								<tbody>
									<tr>
										<td>' . $result1[$i]["number_test"] . '</td>
										<td>' . $result1[$i]["title"] . '</td>
										<td>' . $result1[$i]["name"] . '</td>
										<td>' . $result1[$i]["rating"] . '</td>
										<td>' . $result1[$i]["id_group"] . '</td>
										<td>' . $result1[$i]["number_book"] . '</td>
							 		</tr>
								</tbody>';
						}
				 ?>	
			</div>
				 
			</table>
			</form>
		</div>
	</div> <!-- end of wrapper -->
</div>



<div id="templatemo_footer_wrapper">
    <div id="templatemo_footer">
    
        <a href="index.php">Головна</a> | <a href="lectures.php">Лекції</a> | <a href="lab_works.php">Лабораторні роботи</a> | <a href="other.php">Інше</a> | <a href="contact.php">Довідка</a><br /><br />

        ХДУ © 2017 <a href="http://ksuonline.kspu.edu/?lang=ru">KSU Online</a> | NewLife <a href="http://www.kspu.edu/default.aspx?lang=uk" target="_parent">ХДУ</a>
    
    </div> <!-- end of templatemo_footer -->
</div>

</body>
</html>