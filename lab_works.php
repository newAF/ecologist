<?php 
	require "include/db.php"
 ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Лабораторні роботи.Ecologist</title>
<script>document.write('<script src="http://' + (location.host || 'localhost').split(':')[0] + ':35729/livereload.js?snipver=1"></' + 'script>')</script>
<meta name="keywords" content="green home, web design gallery, free template, CSS, HTML" />
<meta name="description" content="Green Home - Web Design Gallery | free css template by templatemo.com" />
<link href="templatemo_style.css" rel="stylesheet" type="text/css" />

<!--////// CHOOSE ONE OF THE 3 PIROBOX STYLES  \\\\\\\-->
<link href="css_pirobox/white/style.css" media="screen" title="shadow" rel="stylesheet" type="text/css" />
<!--<link href="css_pirobox/white/style.css" media="screen" title="white" rel="stylesheet" type="text/css" />
<link href="css_pirobox/black/style.css" media="screen" title="black" rel="stylesheet" type="text/css" />-->
<!--////// END  \\\\\\\-->

<!--////// INCLUDE THE JS AND PIROBOX OPTION IN YOUR HEADER  \\\\\\\-->
<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/piroBox.1_2.js"></script>
<script type="text/javascript">
$(document).ready(function() {
	$().piroBox({
			my_speed: 600, //animation speed
			bg_alpha: 0.5, //background opacity
			radius: 0, //caption rounded corner
			scrollImage : true, // true == image follows the page, false == image remains in the same open position
			pirobox_next : 'piro_next', // Nav buttons -> piro_next == inside piroBox , piro_next_out == outside piroBox
			pirobox_prev : 'piro_prev',// Nav buttons -> piro_prev == inside piroBox , piro_prev_out == outside piroBox
			close_all : '.piro_close',// add class .piro_overlay(with comma)if you want overlay click close piroBox
			slideShow : 'slideshow', // just delete slideshow between '' if you don't want it.
			slideSpeed : 3 //slideshow duration in seconds(3 to 6 Recommended)
	});
});
</script>
<!--////// END  \\\\\\\-->

</head>
<body>

<div id="templatemo_document_wrapper">
	<div id="templatemo_wrapper">
	
		<?php include'include/menu_lab_works.php' ?>

		<div id="templatemo_header">
			<div id="site_title">
				<h1><img src="images/templatemo_logo.png" alt="Logo" /></h1>
			</div> <!-- end of site_title -->    
			<div id="header_content">
				<p>
					"Людина не стане паном природи, поки вона не стала паном самого себе."
					<em><span class="autor_1">-Георг Гегель-</span></em>  </p>
			</div>
		</div>
		
		<div id="templatemo_main">
			<span class="zagolovok"><strong>Лабораторні роботи з дисципліни "ІНФОРМАТИКА І СИСТЕМОЛОГІЯ".</strong></span>
			<div class="cleaner_h30"></div>
		
			<div id="gallery">
				<div class="gallery_box gb_even">
					<div class="left">
						 <img src="images/icons_lab.png"/>
					</div>
					<div class="right">
					
						<div><a  style="text-decoration: none;"  href="labs/lab_1.php">
							<h5>Лабораторна робота №1.</h5>
							<h6>>> Робота з об'єктами Windows.</h6></a><br>
						</div>
						<div class="button">
							<a href="labs/lab_1.php" class="button_d"/>Детальніше</a>
							<a   href="download/Laboratory_works/ЛАБОРАТОРНА РОБОТА 1.docx" class="button_d" download/>Завантажити</a>
						</div>
					</div>
					<div class="cleaner"></div>
				</div>
					
				<div class="gallery_box gb_odd">
					<div class="left">
						<img src="images/icons_lab.png">
					</div>
					<div class="right">
					<div><a  style="text-decoration: none;"  href="labs/lab_2.php">
					  <h5>Лабораторна робота №2.</h5>
					  <h6>Створення та редагування графічних файлів. Редактор Paint.</h6></a>
					  </div>
					   <div class="button">
							<a href="labs/lab_2.php" class="button_d"/>Детальніше</a>
							<a   href="download/Laboratory_works/ЛАБОРАТОРНА РОБОТА 2.docx" class="button_d" download/>Завантажити</a>
						</div>
					</div>
					<div class="cleaner"></div>
				</div>

				<div class="gallery_box gb_even">
					<div class="left">
						 <img src="images/icons_lab.png"/>
					</div>
					<div class="right">
					
						<div><a  style="text-decoration: none;"  href="labs/lab_3.php">
							<h5>Лабораторна робота №3.</h5>
							<h6> Вікно процесора Word. Введення та ркдагування тексту у текстовому процесорі Word.</h6></a>
						</div>
						<div class="button">
							<a href="labs/lab_3.php" class="button_d"/>Детальніше</a>
							<a   href="download/Laboratory_works/ЛАБОРАТОРНА РОБОТА 3.docx" class="button_d" download/>Завантажити</a>
						</div>
					</div>
					<div class="cleaner"></div>
				</div>
					
				<div class="gallery_box gb_odd">
					<div class="left">
						<img src="images/icons_lab.png">
					</div>
					<div class="right">
					<div><a  style="text-decoration: none;"  href="labs/lab_4.php">
					  <h5>Лабораторна робота №4.</h5>
					  <h6>Створення комплексних текстових документів засобами текстового процесора Word.</h6></a>
					  </div>
					   <div class="button">
							<a href="labs/lab_4.php" class="button_d"/>Детальніше</a>
							<a   href="download/Laboratory_works/ЛАБОРАТОРНА РОБОТА 4.docx" class="button_d" download/>Завантажити</a>
						</div>
					</div>
					<div class="cleaner"></div>
				</div>

				<div class="gallery_box gb_even">
					<div class="left">
						 <img src="images/icons_lab.png"/>
					</div>
					<div class="right">
					
						<div><a  style="text-decoration: none;"  href="labs/lab_5.php">
							<h5>Лабораторна робота №5.</h5>
							<h6> Використання діаграм та малюнків у документах.</h6></a>
						</div>
						<div class="button">
							<a href="labs/lab_5.php" class="button_d"/>Детальніше</a>
							<a   href="download/Laboratory_works/ЛАБОРАТОРНА РОБОТА 5.docx" class="button_d" download/>Завантажити</a>
						</div>
					</div>
					<div class="cleaner"></div>
				</div>
					
				<div class="gallery_box gb_odd">
					<div class="left">
						<img src="images/icons_lab.png">
					</div>
					<div class="right">
					<div><a  style="text-decoration: none;"  href="labs/lab_6.php">
					  <h5>Лабораторна робота №6.</h5>
					  <h6>Робота з об'єктами в MS Wors.</h6></a>
					  </div>
					   <div class="button">
							<a href="labs/lab_6.php" class="button_d"/>Детальніше</a>
							<a   href="download/Laboratory_works/ЛАБОРАТОРНА РОБОТА 6.docx" class="button_d" download/>Завантажити</a>
						</div>
					</div>
					<div class="cleaner"></div>
				</div>

				<div class="gallery_box gb_even">
					<div class="left">
						 <img src="images/icons_lab.png"/>
					</div>
					<div class="right">
					
						<div><a  style="text-decoration: none;"  href="labs/lab_7.php">
							<h5>Лабораторна робота №7.</h5>
							<h6> Робота з багатосторінковими документами.</h6></a>
						</div>
						<div class="button">
							<a href="labs/lab_7.php" class="button_d"/>Детальніше</a>
							<a   href="download/Laboratory_works/ЛАБОРАТОРНА РОБОТА 7.docx" class="button_d" download/>Завантажити</a>
						</div>
					</div>
					<div class="cleaner"></div>
				</div>
					
				<div class="gallery_box gb_odd">
					<div class="left">
						<img src="images/icons_lab.png">
					</div>
					<div class="right">
					<div><a  style="text-decoration: none;"  href="labs/lab_8.php">
					  <h5>Лабораторна робота №8.</h5>
					  <h6>Технологія роботи з функціями в Ms Excel.</h6></a>
					  </div>
					   <div class="button">
							<a href="labs/lab_8.php" class="button_d"/>Детальніше</a>
							<a   href="download/Laboratory_works/ЛАБОРАТОРНА РОБОТА 8.docx" class="button_d" download/>Завантажити</a>
						</div>
					</div>
					<div class="cleaner"></div>
				</div>

				<div class="gallery_box gb_even">
					<div class="left">
						 <img src="images/icons_lab.png"/>
					</div>
					<div class="right">
					
						<div><a  style="text-decoration: none;"  href="labs/lab_9.php">
							<h5>Лабораторна робота №9.</h5>
							<h6>Створення вкладених функцій. Фільтрація даних в MS Excel.</h6></a>
						</div>
						<div class="button">
							<a href="labs/lab_9.php" class="button_d"/>Детальніше</a>
							<a   href="download/Laboratory_works/ЛАБОРАТОРНА РОБОТА 9.docx" class="button_d" download/>Завантажити</a>
						</div>
					</div>
					<div class="cleaner"></div>
				</div>
					
				<div class="gallery_box gb_odd">
					<div class="left">
						<img src="images/icons_lab.png">
					</div>
					<div class="right">
					<div><a  style="text-decoration: none;"  href="labs/lab_10.php">
					  <h5>Лабораторна робота №10.</h5>
					  <h6> Робота з діаграмами Ms Excel.</h6></a><br>
					  </div>
					   <div class="button">
							<a href="labs/lab_10.php" class="button_d"/>Детальніше</a>
							<a   href="download/Laboratory_works/ЛАБОРАТОРНА РОБОТА 10.docx" class="button_d" download/>Завантажити</a>
						</div>
					</div>
					<div class="cleaner"></div>
				</div>

				<div class="gallery_box gb_even">
					<div class="left">
						 <img src="images/icons_lab.png"/>
					</div>
					<div class="right">
					
						<div><a  style="text-decoration: none;"  href="labs/lab_11.php">
							<h5>Лабораторна робота №11.</h5>
							<h6> Використання умовного оператора.</h6></a>
						</div>
						<div class="button">
							<a href="labs/lab_11.php" class="button_d"/>Детальніше</a>
							<a   href="download/Laboratory_works/ЛАБОРАТОРНА РОБОТА 11.docx" class="button_d" download/>Завантажити</a>
						</div>
					</div>
					<div class="cleaner"></div>
				</div>
					
				<div class="gallery_box gb_odd">
					<div class="left">
						<img src="images/icons_lab.png">
					</div>
					<div class="right">
					<div><a  style="text-decoration: none;"  href="labs/lab_12.php">
					  <h5>Лабораторна робота №12.</h5>
					  <h6>Кореляційно-регресійний аналіз в MS Excel.</h6></a>
					  </div>
					   <div class="button">
							<a href="labs/lab_12.php" class="button_d"/>Детальніше</a>
							<a   href="download/Laboratory_works/ЛАБОРАТОРНА РОБОТА 12.docx" class="button_d" download/>Завантажити</a>
						</div>
					</div>
					<div class="cleaner"></div>
				</div>

				<div class="gallery_box gb_even">
					<div class="left">
						 <img src="images/icons_lab.png"/>
					</div>
					<div class="right">
					
						<div><a  style="text-decoration: none;"  href="labs/lab_13.php">
							<h5>Лабораторна робота №13.</h5>
							<h6> Проектування бази даних. Створення таблиць та форм в MS Access</h6></a>
						</div>
						<div class="button">
							<a href="labs/lab_13.php" class="button_d"/>Детальніше</a>
							<a   href="download/Laboratory_works/ЛАБОРАТОРНА РОБОТА 13.docx" class="button_d" download/>Завантажити</a>
						</div>
					</div>
					<div class="cleaner"></div>
				</div>
					
				<div class="gallery_box gb_odd">
					<div class="left">
						<img src="images/icons_lab.png">
					</div>
					<div class="right">
					<div><a  style="text-decoration: none;"  href="labs/lab_14.php">
					  <h5>Лабораторна робота №14.</h5>
					  <h6>Технологія створення запитів та звіт в Ms Access.</h6></a><br>
					  </div>
					   <div class="button">
							<a href="labs/lab_14.php" class="button_d"/>Детальніше</a>
							<a   href="download/Laboratory_works/ЛАБОРАТОРНА РОБОТА 14.docx" class="button_d" download/>Завантажити</a>
						</div>
					</div>
					<div class="cleaner"></div>
				</div>

				<div class="gallery_box gb_even">
					<div class="left">
						 <img src="images/icons_lab.png"/>
					</div>
					<div class="right">
					
						<div><a  style="text-decoration: none;"  href="labs/lab_15.php">
							<h5>Лабораторна робота №15.</h5>
							<h6>Створення претензій засобами пакету MS Power Point.</h6></a>
						</div>
						<div class="button">
							<a href="labs/lab_15.php" class="button_d"/>Детальніше</a>
							<a href="download/Laboratory_works/ЛАБОРАТОРНА РОБОТА 15.docx" class="button_d" download/>Завантажити</a>
						</div>
					</div>
					<div class="cleaner"></div>
				</div>
					
				<div class="gallery_box gb_odd">
					<div class="left">
						<img src="images/icons_lab.png">
					</div>
					<div class="right">
					<div>
					<h5>Лабораторні роботи 1-15.</h5>
					  <h6>Всі лабораторні роботи в одному файлі 1-15..</h6></a><br>
					  </div>
					   <div class="button">
							<a href="download/Laboratory_works/Лабораторні роботи 1-15, Інформатика і системологія .docx" class="button_d" download/>Завантажити</a> 
						</div>
					</div>
					<div class="cleaner"></div>
				</div>

				
				  
				</div> <!-- end of gallery -->
                
			<div class="cleaner"></div>
		</div>
		
	</div> <!-- end of wrapper -->
</div>

<div id="templatemo_footer_wrapper">
    <div id="templatemo_footer">
    
        <a href="index.php">Головна</a> | <a href="lectures.php">Лекції</a> | <a href="lab_works.php">Лабораторні роботи</a> | <a href="other.php">Інше</a> | <a href="contact.php">Довідка</a><br /><br />

        ХДУ © 2017 <a href="http://ksuonline.kspu.edu/?lang=ru">KSU Online</a> | NewLife <a href="http://www.kspu.edu/default.aspx?lang=uk" target="_parent">ХДУ</a>
    
    </div> <!-- end of templatemo_footer -->
</div>

</body>
</html>