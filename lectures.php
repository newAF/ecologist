<?php 
	require "include/db.php"
 ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Лекції.Ecologist</title>
<script>document.write('<script src="http://' + (location.host || 'localhost').split(':')[0] + ':35729/livereload.js?snipver=1"></' + 'script>')</script>
<meta name="keywords" content="green home, web design gallery, free template, CSS, HTML" />
<meta name="description" content="Green Home - Web Design Gallery | free css template by templatemo.com" />
<link href="templatemo_style.css" rel="stylesheet" type="text/css" />

<!--////// CHOOSE ONE OF THE 3 PIROBOX STYLES  \\\\\\\-->
<link href="css_pirobox/white/style.css" media="screen" title="shadow" rel="stylesheet" type="text/css" />
<!--<link href="css_pirobox/white/style.css" media="screen" title="white" rel="stylesheet" type="text/css" />
<link href="css_pirobox/black/style.css" media="screen" title="black" rel="stylesheet" type="text/css" />-->
<!--////// END  \\\\\\\-->

<!--////// INCLUDE THE JS AND PIROBOX OPTION IN YOUR HEADER  \\\\\\\-->
<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/piroBox.1_2.js"></script>
<script type="text/javascript">
$(document).ready(function() {
	$().piroBox({
			my_speed: 600, //animation speed
			bg_alpha: 0.5, //background opacity
			radius: 0, //caption rounded corner
			scrollImage : true, // true == image follows the page, false == image remains in the same open position
			pirobox_next : 'piro_next', // Nav buttons -> piro_next == inside piroBox , piro_next_out == outside piroBox
			pirobox_prev : 'piro_prev',// Nav buttons -> piro_prev == inside piroBox , piro_prev_out == outside piroBox
			close_all : '.piro_close',// add class .piro_overlay(with comma)if you want overlay click close piroBox
			slideShow : 'slideshow', // just delete slideshow between '' if you don't want it.
			slideSpeed : 3 //slideshow duration in seconds(3 to 6 Recommended)
	});
});
</script>
<!--////// END  \\\\\\\-->

</head>
<body>

<div id="templatemo_document_wrapper">
	<div id="templatemo_wrapper">
	
		<?php include'include/menu_lectures.php' ?>

		
		
		<div id="templatemo_header">
			<div id="site_title">
				<h1><img src="images/templatemo_logo.png" alt="Logo" /></h1>
			</div> <!-- end of site_title -->    
			<div id="header_content">
				<p>
					"Природа - це найкраща з книг, написана на особливій мові. Цю мова треба вивчати."<br>
					<em><span class="autor_1">-Н.Г. Гарін-Михайлівський-</span></em>  </p>
				
			</div>
		</div>
		
		<div id="templatemo_main">
			<span class="zagolovok"><strong>Конспект лекцій з дисципліни "ІНФОРМАТИКА І СИСТЕМОЛОГІЯ".</strong></span>
			<div class="cleaner_h30"></div>
		
			<div id="gallery">

				<div class="gallery_box gb_even">
					<div class="left">
						 <img src="images/icon-bab.png"/>
					</div>
					<div class="right">
					
						<div><a  style="text-decoration: none;"  href="lecture/lecture_1.php">
							<h5>Лекція 1.</h5>
							<h6> Поняття інформаційних технологій та інформації. Основи роботи в операційній системі Windows.</h6></a>
						</div>
						<div class="button">
							<a href="lecture/lecture_1.php" class="button_d"/>Детальніше</a>
							<a href="download\Ecologist_lectures\ЛЕКЦІЯ 1.docx" class="button_d" download/>Завантажити</a>  
						</div>
					</div>
					<div class="cleaner"></div>
				</div>
					
				<div class="gallery_box gb_odd">
					<div class="left">
						<img src="images/icon-bab.png">
					</div>
					<div class="right">
					<div><a  style="text-decoration: none;"  href="lecture/lecture_2.php">
					  <h5>Лекція 2.</h5>
					  <h6>Загальна характеристика операційної системи Windows.</h6></a>
					  </div>
					   <div class="button">
							<a href="lecture/lecture_2.php" class="button_d"/>Детальніше</a>
							<a href="download\Ecologist_lectures\ЛЕКЦІЯ 2.docx" class="button_d" download/>Завантажити</a>  
						</div>
					</div>
					<div class="cleaner"></div>
				</div>

				<div class="gallery_box gb_even">
					<div class="left">
						 <img src="images/icon-bab.png"/>
					</div>
					<div class="right">
					
						<div><a  style="text-decoration: none;"  href="lecture/lecture_3.php">
							<h5>Лекція 3.</h5>
							<h6> Загальна характеристика текстового процесора MS Word, введення та редагування тексту.</h6></a>
						</div>
						<div class="button">
							<a href="lecture/lecture_3.php" class="button_d"/>Детальніше</a>
							<a href="download\Ecologist_lectures\ЛЕКЦІЯ 3.docx" class="button_d" download/>Завантажити</a>  
						</div>
					</div>
					<div class="cleaner"></div>
				</div>
					
				<div class="gallery_box gb_odd">
					<div class="left">
						<img src="images/icon-bab.png">
					</div>
					<div class="right">
					<div><a  style="text-decoration: none;"  href="lecture/lecture_4.php">
					  <h5>Лекція 4.</h5>
					  <h6>Форматування тексту та символів у текстовому процесорі MS Word.</h6></a>
					  </div>
					   <div class="button">
							<a href="lecture/lecture_4.php" class="button_d"/>Детальніше</a>
							<a href="download\Ecologist_lectures\ЛЕКЦІЯ 4.docx" class="button_d" download/>Завантажити</a>  
						</div>
					</div>
					<div class="cleaner"></div>
				</div>

				<div class="gallery_box gb_even">
					<div class="left">
						 <img src="images/icon-bab.png"/>
					</div>
					<div class="right">
					
						<div><a  style="text-decoration: none;"  href="lecture/lecture_5.php">
							<h5>Лекція 5.</h5>
							<h6> Використання об'єктів в текстовому процесорі MS Word (малюнки, формули, таблиці, діаграми), вставка та форматування об'єктів.</h6></a>
						</div>
						<div class="button">
							<a href="lecture/lecture_5.php" class="button_d"/>Детальніше</a>
							<a href="download\Ecologist_lectures\ЛЕКЦІЯ 5.docx" class="button_d" download/>Завантажити</a>  
						</div>
					</div>
					<div class="cleaner"></div>
				</div>
					
				<div class="gallery_box gb_odd">
					<div class="left">
						<img src="images/icon-bab.png">
					</div>
					<div class="right">
					<div><a  style="text-decoration: none;"  href="lecture/lecture_6.php">
					  <h5>Лекція 6.</h5>
					  <h6>Використання таблиць у текстовому процесорі MS Word.</h6></a><br>
					  </div>
					   <div class="button">
							<a href="lecture/lecture_6.php" class="button_d"/>Детальніше</a>
							<a href="download\Ecologist_lectures\ЛЕКЦІЯ 6.docx" class="button_d" download/>Завантажити</a>  
						</div>
					</div>
					<div class="cleaner"></div>
				</div>

				<div class="jjj">
					<div class="gallery_box gb_even">
					<div class="left">
						 <img src="images/icon-bab.png"/>
					</div>
					<div class="right">
					
						<div><a  style="text-decoration: none;"  href="lecture/lecture_7.php">
							<h5>Лекція 7.</h5>
							<h6> Підготовка комплексних документів у текстовому процесорі MS Word, форматування сторінок, вставка номерів сторінок, полів, створення змісту, елементи комп'ютерної верстки.</h6></a>
						</div>
						<div class="button">
							<a href="lecture/lecture_7.php" class="button_d"/>Детальніше</a>
							<a href="download\Ecologist_lectures\ЛЕКЦІЯ 7.docx" class="button_d" download/>Завантажити</a>  
						</div>
					</div>
					<div class="cleaner"></div>
				</div>
				</div>
					
				<div class="gallery_box gb_odd">
					<div class="left">
						<img src="images/icon-bab.png">
					</div>
					<div class="right">
					<div><a  style="text-decoration: none;"  href="lecture/lecture_8.php">
					  <h5>Лекція 8.</h5>
					  <h6>Робота з діаграмами, використання графічних об'єктів у текстовому процесорі Microsoft Word.</h6></a><br><br>
					  </div>
					   <div class="button">
							<a href="lecture/lecture_8.php" class="button_d"/>Детальніше</a>
							<a href="download\Ecologist_lectures\ЛЕКЦІЯ 8.docx" class="button_d" download/>Завантажити</a>  
						</div>
					</div>
					<div class="cleaner"></div>
				</div>

				<div class="gallery_box gb_even">
					<div class="left">
						 <img src="images/icon-bab.png"/>
					</div>
					<div class="right">
					
						<div><a  style="text-decoration: none;"  href="lecture/lecture_9.php">
							<h5>Лекція 9.</h5>
							<h6> Загальна характеристика табличного процесора MS Exel, ввод, редагування, форматування даних та збереження електронних таблиць.</h6></a>
						</div>
						<div class="button">
							<a href="lecture/lecture_9.php" class="button_d"/>Детальніше</a>
							<a href="download\Ecologist_lectures\ЛЕКЦІЯ 9.docx" class="button_d" download/>Завантажити</a>  
						</div>
					</div>
					<div class="cleaner"></div>
				</div>
					
				<div class="gallery_box gb_odd">
					<div class="left">
						<img src="images/icon-bab.png">
					</div>
					<div class="right">
					<div><a  style="text-decoration: none;"  href="lecture/lecture_10.php">
					  <h5>Лекція 10.</h5>
					  <h6>Використання розрахунків у табличному процесорі MS Exel за допомогою формул та вбудованих функцій, побудова діаграм.</h6></a>
					  </div>
					   <div class="button">
							<a href="lecture/lecture_10.php" class="button_d"/>Детальніше</a>
							<a href="download\Ecologist_lectures\ЛЕКЦІЯ 10.docx" class="button_d" download/>Завантажити</a>  
						</div>
					</div>
					<div class="cleaner"></div>
				</div>

				<div class="gallery_box gb_even">
					<div class="left">
						 <img src="images/icon-bab.png"/>
					</div>
					<div class="right">
					
						<div><a  style="text-decoration: none;"  href="lecture/lecture_11.php">
							<h5>Лекція 11.</h5>
							<h6> Статистичний аналіз у табличному процесорі MS Exel використання статистичних функцій та засобу "Описова статистика".</h6></a>
						</div>
						<div class="button">
							<a href="lecture/lecture_11.php" class="button_d"/>Детальніше</a>
							<a href="download\Ecologist_lectures\ЛЕКЦІЯ 11.docx" class="button_d" download/>Завантажити</a>  
						</div>
					</div>
					<div class="cleaner"></div>
				</div>
					
				<div class="gallery_box gb_odd">
					<div class="left">
						<img src="images/icon-bab.png">
					</div>
					<div class="right">
					<div><a  style="text-decoration: none;"  href="lecture/lecture_12.php">
					  <h5>Лекція 12.</h5>
					  <h6>Кореляційний та регресійний аналіз у табличному процесоріMS Exel, використання засобу "Регресія".</h6></a>
					  </div>
					   <div class="button">
							<a href="lecture/lecture_12.php" class="button_d"/>Детальніше</a>
							<a href="download\Ecologist_lectures\ЛЕКЦІЯ 12.docx" class="button_d" download/>Завантажити</a>  
						</div>
					</div>
					<div class="cleaner"></div>
				</div>

				<div class="gallery_box gb_even">
					<div class="left">
						 <img src="images/icon-bab.png"/>
					</div>
					<div class="right">
					
						<div><a  style="text-decoration: none;"  href="lecture/lecture_13.php">
							<h5>Лекція 13.</h5>
							<h6> Системи управління базами даних. Створення баз даних.</h6></a>
						</div>
						<div class="button">
							<a href="lecture/lecture_13.php" class="button_d"/>Детальніше</a>
							<a href="download\Ecologist_lectures\ЛЕКЦІЯ 13.docx" class="button_d" download/>Завантажити</a>  
						</div>
					</div>
					<div class="cleaner"></div>
				</div>
					
				<div class="gallery_box gb_odd">
					<div class="left">
						<img src="images/icon-bab.png">
					</div>
					<div class="right">
					<div><a  style="text-decoration: none;"  href="lecture/lecture_14.php">
					  <h5>Лекція 14.</h5>
					  <h6>Робота за базами даних.</h6></a><br>
					  </div>
					   <div class="button">
							<a href="lecture/lecture_14.php" class="button_d"/>Детальніше</a>
							<a href="download\Ecologist_lectures\ЛЕКЦІЯ 14.docx" class="button_d" download/>Завантажити</a>  
						</div>
					</div>
					<div class="cleaner"></div>
				</div>

				<div class="gallery_box gb_even">
					<div class="left">
						 <img src="images/icon-bab.png"/>
					</div>
					<div class="right">
					
						<div><a  style="text-decoration: none;"  href="lecture/lecture_15.php">
							<h5>Лекція 15.</h5>
							<h6> Системи створення презентацій.</h6></a>
						</div><br>
						<div class="button">
							<a href="lecture/lecture_15.php" class="button_d"/>Детальніше</a>
							<a href="download\Ecologist_lectures\ЛЕКЦІЯ 15.docx" class="button_d" download/>Завантажити</a>  
						</div>
					</div>
					<div class="cleaner"></div>
				</div>
					
				<div class="gallery_box gb_odd">
					<div class="left">
						<img src="images/icon-bab.png">
					</div>
					<div class="right">
					<div><a  style="text-decoration: none;"  href="lecture/lecture_16.php">
					  <h5>Лекція 16.</h5>
					  <h6>Інтернет-технології. Класифікація та основні характеристики комп'ютерних мереж.</h6></a>
					  </div>
					   <div class="button">
							<a href="lecture/lecture_16.php" class="button_d"/>Детальніше</a>
							<a href="download\Ecologist_lectures\ЛЕКЦІЯ 16.docx" class="button_d" download/>Завантажити</a>  
						</div>
					</div>
					<div class="cleaner"></div>
				</div>


				</div> <!-- end of gallery -->
                
			<div class="cleaner"></div>
		</div>
		
	</div> <!-- end of wrapper -->
</div>


<div id="templatemo_footer_wrapper">
    <div id="templatemo_footer">
    
        <a href="index.php">Головна</a> | <a href="lectures.php">Лекції</a> | <a href="lab_works.php">Лабораторні роботи</a> | <a href="other.php">Інше</a> | <a href="contact.php">Довідка</a><br /><br />

        ХДУ © 2017 <a href="http://ksuonline.kspu.edu/?lang=ru">KSU Online</a> | NewLife <a href="http://www.kspu.edu/default.aspx?lang=uk" target="_parent">ХДУ</a>
    
    </div> <!-- end of templatemo_footer -->
</div>

</body>
</html>