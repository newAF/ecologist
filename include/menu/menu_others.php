<div id="templatemo_menu">
	<ul>
		<li><a href="../index.php">Головна</a></li>
		<li><a href="../lectures.php">Лекції</a></li>
		<li><a href="../lab_works.php">Лабораторні роботи</a></li>
		<li><a href="../other.php"  class="current">Інше</a></li>
		<li><a href="../test.php">Тести</a></li>
		<li><a href="../contact.php">Довідка</a></li>
				
	<?php if (isset($_SESSION['logged_user'])) : ?>
		<div style="display: block; padding-top: 15px; padding-right: 20px; float: right; font-size: 13px; font-weight: bold;">Ви ввійшли як: <span style="color: green;"><?php echo $_SESSION['logged_user']->name; ?></span><?php 
		 $lvl_a = $_SESSION['logged_user']->lvl_a;
		 if ($lvl_a == 2) {
		 	echo '<span style="color:#2542AA;">(admin)</span>'; 
		 }?><a href="/logout.php"> <img src="/images/logout.png" align="absmiddle"></a></div>

		<?php 
		 $lvl_a = $_SESSION['logged_user']->lvl_a;
		 if ($lvl_a == 2) {
		 	echo '<a href="/admin/admin.php" class="button_d" style="position:absolute; margin-top:10px; min-width:137px;">Адмін панель</a>'; 
		 }?>
		 
	<?php else : ?>
		<li><a href="../login.php">Вхід</a></li>
		<li><a href="../signup.php">Реєстрація</a></li>		
	<?php endif; ?>
	</ul>
			    	
</div> <!-- end of templatemo_menu -->  	