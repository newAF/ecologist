<?php 
    require "../include/db_2.php";
 ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Лабараторна робота.Ecologist</title>
<script>document.write('<script src="http://' + (location.host || 'localhost').split(':')[0] + ':35729/livereload.js?snipver=1"></' + 'script>')</script>
<meta name="keywords" content="green home, contact form, contact page, submit button, CSS, HTML" />
<meta name="description" content="Green Home Theme - Contact Page, Contact Form, Submit Button" />
<link href="../templatemo_style.css" rel="stylesheet" type="text/css" />
<link href="style_lecture.css" rel="stylesheet" type="text/css" />

</head>
<body>

<div id="templatemo_document_wrapper">
<div id="templatemo_wrapper">

    <?php include '../include/menu/menu_labs.php'; ?>
    
    <div id="templatemo_header">
        <div id="site_title">
            <h1><a href="http://www.templatemo.com"><img src="../images/templatemo_logo.png" alt="Logo" /></a></h1>
        </div> <!-- end of site_title -->    
        <div id="header_content">
               <p>"Главная склонность человека направлена на то, что соответствует природе."
                <em><span class="autor_1">Цицерон</span></em></p>
        </div>
    </div>

    <div id="templatemo_main">

        <center><span class="zagolovok">Лабораторна робота №5.</span></center></br>
                <span class="nazvanie">
                   <strong> Використання діаграм та малюнків у документах.</strong> <hr>
                </span>

       <span class="plan">
           <h5>План</h5>
                1. Вставка малюнків у документи.<br/>
                2. Використання фігурного тексту.<br/>    
                3. Вставка діаграм та формул.<br/>
       </span>

        <center><h4> Послідовність виконання роботи </h4></center><br>

         <span class="text">
         <img src="images_lab/lab_5.1.png">
         <p class="text_ab">Необхідно побудувати діаграму на базі даної таблиці.</p>
         <p class="text_ab">Створіть та заповніть таблицю за представленим зразком.</p>
         <p class="text_ab">Скопіюйте таблицю в буфер обміну.</p>
         <p class="text_ab">Вставте діаграму (<strong>Вставка – Об’єкт - Діаграма</strong>). Поряд з діаграмою розкриється її базова таблиця.</p>
         <p class="text_ab">Виділіть вміст таблиці клацанням на осередку у верхньому лівому куті таблиці.</p>
         <p class="text_ab">Замініть вміст базової таблиці вмістом своєї таблиці (<strong>Правка – Вставить</strong>).</p>
         <p class="text_ab">Зверніть увагу на зміни в діаграмі. Вони прийшли у відповідність з таблицею.</p>
         <p class="text_ab">На діаграмі виділіть область побудови. Клацніть правою кнопкою миші і в контекстному меню виберіть пункт <strong>Тип діаграми</strong>. Перегляньте інші види діаграм.</p>
         <p class="text_ab">Закрийте вікно <strong>Тип діаграми</strong>.</p>
         <p class="text_ab">Збережіть документ у папці Мої документи/Прізвище.</p>
                  
            <div class="vopros">
                <strong><center>КОНТРОЛЬНІ ЗАПИТАННЯ ТА ЗАВДАННЯ</center></strong><br>
                
                1. Опишіть алгоритм вставки малюнка в документ Word.<br>
                2. Перерахуйте формати графічних файлів, які можна вставляти в документ Word як малюнки<br>
                3. Як відредагувати малюнок, змінити положення, масштаб?<br>
                4. Як вибрати обрамлення, фон і обтікання малюнка текстом?<br>
                5. Опишіть алгоритм вставки об’єкту WordArt. Як відбувається зміна даного об’єкту?<br>
                6. Опишіть алгоритм вставки формул. Як працювати з панеллю інструментів <Формула>?<br>
            </div>
         </p>
         <p class="text_ab">
           <div class="vopros">
                <strong><center>ЗАВДАННЯ ДЛЯ САМОСТІЙНОГО ОПРАЦЮВАННЯ</center></strong>
            1. Призначення систем опрацювання тексту.<br>
            2. Порядок запуску текстового редактора Word.<br>
            3. Склад горизонтального меню.<br>
            4. Настройка екрану Word.<br>
            5. Використання клавіатури та мишки для редагування тексту.<br>
            6. Порядок копіювання, вставки та вилучення фрагментів тексту.<br>
            7. Форматування сторінок.<br>
            8. Форматування тексту.<br>
            9. Збереження документа у вигляді файлів.<br>
            10. Формати документів Word.<br>
            11. Пересилка документів Word на дискету.<br>
            12. Редагування таблиці, додавання та вилучення рядків, стовпчиків, окремих чарунок.<br>
            13. Об’єднання декількох чарунок в одну та розбиття окремої чарунки на декілька.<br>
            14. Сортування даних у таблицях.<br>
            15. Використання формул для виконання елементарних обчислень у таблицях.<br>
            16.  Копіювання, вставка та вилучення таблиці.<br>
            17. Форматування ліній таблиці.<br>
            18. Створення заголовків у багатосторінковій таблиці.<br>
            19. Вставка малюнку в документ.<br>
            20. Вставка файлу в документ.<br>
            21. Вставка в документ готового файлу.<br>
            22. Вставка фігурного тексту.<br>
            23. Форматування малюнків.<br>
            24. Форматування фігурного тексту.<br>
            25. Вставка та редагування формул.<br>
           </div>
        </p>
         </span>

    </div>
    </div>


    <div id="templatemo_footer_wrapper">
    <div id="templatemo_footer">
    
        <a href="index.html">Головна</a> | <a href="lectures.html">Лекції</a> | <a href="lab_works.html">Лабораторні роботи</a> | <a href="other.html">Інше</a> | <a href="contact.html">Довідка</a><br /><br />

        ХДУ © 2017 <a href="http://ksuonline.kspu.edu/?lang=ru">KSU Online</a> | NewLife <a href="http://www.kspu.edu/default.aspx?lang=uk" target="_parent">ХДУ</a>
    
    </div> <!-- end of templatemo_footer -->
</div>

</body>
</html>