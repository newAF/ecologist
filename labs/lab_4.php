<?php 
    require "../include/db_2.php";
 ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Лабараторна робота.Ecologist</title>
<script>document.write('<script src="http://' + (location.host || 'localhost').split(':')[0] + ':35729/livereload.js?snipver=1"></' + 'script>')</script>
<meta name="keywords" content="green home, contact form, contact page, submit button, CSS, HTML" />
<meta name="description" content="Green Home Theme - Contact Page, Contact Form, Submit Button" />
<link href="../templatemo_style.css" rel="stylesheet" type="text/css" />
<link href="style_lecture.css" rel="stylesheet" type="text/css" />

</head>
<body>

<div id="templatemo_document_wrapper">
<div id="templatemo_wrapper">

    <?php include '../include/menu/menu_labs.php'; ?>
    
    <div id="templatemo_header">
        <div id="site_title">
            <h1><a href="http://www.templatemo.com"><img src="../images/templatemo_logo.png" alt="Logo" /></a></h1>
        </div> <!-- end of site_title -->    
        <div id="header_content">
               <p>"Главная склонность человека направлена на то, что соответствует природе."
                <em><span class="autor_1">Цицерон</span></em></p>
        </div>
    </div>

    <div id="templatemo_main">

        <center><span class="zagolovok">Лабораторна робота №4.</span></center></br>
                <span class="nazvanie">
                   <strong> Створення комплексних текстових документів засобами текстового процесора WORD.</strong> <hr>
                </span>

       <span class="plan">
           <h5>План</h5>
                1. Проектування, форматування та ввод даних у таблиці.<br/>
                2. Вставка текстових та графічних файлів у документ.<br/>    
                3. Створення документів, які містять текст, таблиці та малюнки.<br/>
       </span>

        <center><h4> Завдання №1 </h4></center><br>

         <span class="text">

         <p class="text_ab"><strong>ПОСЛІДОВНІСТЬ ВИКОНАННЯ РОБОТИ</strong></p>
         <p class="text_ab">Познайомитись з командами пункту горизонтального меню Таблица.</p>
         <img src="images_lab/lab_4.1.png">
         <p class="text_ab">Відформатувати отриману таблицю, використовуючи горизонтальне меню. Вимоги: текст у колонках «№», «Прізвище та ініціали», «№ кабінету», «Службовий телефон» – Tіmes New Roman, розмір – 14 пт., в колонці «Посада» – висота – 12 пт., текст в усіх колонках, окрім колонки «Прізвище та ініціали» розташувати по центру чарунки, в колонці «Прізвище та ініціали» текст розташувати по лівому краю чарунки.</p>
         <p class="text_ab">Назви підрозділів виділити жирним шрифтом, розмір шрифту – 12 пт.</p>
         <p class="text_ab">Додати в таблицю колонки (використовуючи команди Добавить столбцы, Объединить ячейки або Разбить ячейки).</p>
         <p class="text_ab">Покажіть документ викладачу.</p>
         <center><h4> Завдання №2 </h4></center><br>
         <p class="text_ab">На малюнку представлено платіжне доручення банку. По своїй суті платіжне доручення є таблицею. Необхідно створити цю таблицю засобами Microsoft Word.</p>
         <img src="images_lab/lab_4.2.png">
         <p class="text_ab">Виберіть інструмент <strong>Намалювати таблицю</strong>.</p>
         <p class="text_ab">Методом протягування намалюйте прямокутник, ширина якого дорівнює ширині набору полоси.</p>
         <p class="text_ab">Проведіть п’ять вертикальних ліній.</p>
         <p class="text_ab">Проведіть 14 горизонтальних ліній.</p>
         <p class="text_ab">Методом перетягування вертикальних та горизонтальних границь створіть необхідне співвідношення стовпців та рядків таблиці.</p>
         <p class="text_ab">Використовуючи панелі інструментів <strong>Ластік, Форматування</strong> створіть та заповніть текстом бланк платіжного доручення.</p>
         <p class="text_ab">Збережіть документ впапці Мої документи/ Прізвище.</p>
         
         
            <div class="vopros">
                <strong><center>КОНТРОЛЬНІ ЗАПИТАННЯ ТА ЗАВДАННЯ</center></strong><br>
                
                1. Назвати елементи таблиці.<br>
                2. Яку інформацію ви маєте змогу вводити до чарунок таблиці?<br>
                3. Як додати декілька колонок або рядків до таблиці? Як їх вилучити?<br>
                4. Як проводити форматування таблиці?<br>
                5. Яку перевагу дає робота з текстом в таблицях?<br>
            </div>
         
         
         </span>

    </div>
    </div>


    <div id="templatemo_footer_wrapper">
    <div id="templatemo_footer">
    
        <a href="index.html">Головна</a> | <a href="lectures.html">Лекції</a> | <a href="lab_works.html">Лабораторні роботи</a> | <a href="other.html">Інше</a> | <a href="contact.html">Довідка</a><br /><br />

        ХДУ © 2017 <a href="http://ksuonline.kspu.edu/?lang=ru">KSU Online</a> | NewLife <a href="http://www.kspu.edu/default.aspx?lang=uk" target="_parent">ХДУ</a>
    
    </div> <!-- end of templatemo_footer -->
</div>

</body>
</html>