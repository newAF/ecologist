<?php 
    require "../include/db_2.php";
 ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Контакты.Ecologist</title>
<script>document.write('<script src="http://' + (location.host || 'localhost').split(':')[0] + ':35729/livereload.js?snipver=1"></' + 'script>')</script>
<meta name="keywords" content="green home, contact form, contact page, submit button, CSS, HTML" />
<meta name="description" content="Green Home Theme - Contact Page, Contact Form, Submit Button" />
<link href="../templatemo_style.css" rel="stylesheet" type="text/css" />
<link href="style_lecture.css" rel="stylesheet" type="text/css" />

</head>
<body>

<div id="templatemo_document_wrapper">
<div id="templatemo_wrapper">

	<?php include '../include/menu/menu_labs.php'; ?>
    
	<div id="templatemo_header">
        <div id="site_title">
	        <h1><a href="http://www.templatemo.com"><img src="../images/templatemo_logo.png" alt="Logo" /></a></h1>
        </div> <!-- end of site_title -->    
        <div id="header_content">
        	   <p>"Главная склонность человека направлена на то, что соответствует природе."
                <em><span class="autor_1">Цицерон</span></em></p>
        </div>
    </div>

    <div id="templatemo_main">

        <center><span class="zagolovok">Лабораторна робота №2.</span></center></br>
                <span class="nazvanie">
                   <strong> Створення та редагування графічних файлів. Редактор Paint.</strong> <hr>
                </span>

       <span class="plan">
           <h5>План</h5>
                1. Основні елементи вікна графічного редактора Paint.<br/>
                2. Створення малюнків в графічному редакторі.<br/>    
                3. Створення зображень екрану.<br/>
                4. Редагування графічних файлів.<br/>
       </span>

        <center><h4> Теоретичні відомості </h4></center><br>

         <span class="text">

         <p class="text_ab">Графічний редактор Paint входить до складу стандартних програм Windows і призначений для створення і обробки малюнків растрової графіки. Для запуску редактора слід відкрити головне меню Пуск і виконати команди Программы/Стандартные/Графический редактор Paint. Після запуску на екрані з’являється вікно редактора, яке містить ряд типових елементів для вікон програм-додатків Windows (Рис.1). Рядок заголовку містить назву програми Paint, ім’я графічного файла (за замовчуванням файл має ім’я - Безимени) та стандартні кнопки управління вікном.</p>
         <p class="text_ab"><strong>Палітра кольорів</strong> включає 28 зафарбованих квадратів (Рис.1). З цих кольорів користувач може вибрати колір лінії, якою виконуються побудови (лівою кнопкою миші), і колір фону (правою кнопкою миші). Ліворуч від палітри розміщується індикатор кольорів, який складається з двох квадратів (на передньому плані – колір лінії, на задньому - фон). Для вибору кольору лінії слід встановити курсор миші на потрібний колір палітри і натиснути ліву кнопку миші, щоб змінити колір фону треба використовувати праву кнопку миші. Палітра залежить від типу файлу і атрибутів файлу.</p>
         <p class="text_ab"><strong>Панель форми інструмента.</strong> Змінює своє наповнення в залежності від вибраного інструмента.</p>
         <center><img src="images_lab/lab_2.1.png"></center>
         <center>Рис.1 Вікно редактора</center>
         <p class="text_ab">Панель інструментів. Включає ряд кнопок, за допомогою яких можна вибрати необхідний інструмент, побудувати типові елементи малюнка, виділити певний фрагмент. При фіксації курсору миші на кнопці під нею з’являється її назва, а в рядку стану - коротка довідка про призначення кнопки. Розглянемо призначення основних інструментів малювання.</p>
         <p class="text_ab">
         ▼ Олівець (Карандаш) призначено для малювання "від руки", після вибору інструменту курсор миші набирає форму олівця. Для малювання слід натиснути кнопку миші, при натисненні лівої кнопки - малювання відбувається основним кольором, якщо натиснути праву - кольором фону.<br>
         ▼ Пензель (Кисть) призначено для малювання довільних фігур. Після вибору інструмента під панеллю з’являється вікно, у якому можна вибрати форму пензля. Для малювання слід встановити курсор у точку початку малювання і при затиснутій кнопці миші перемістити до кінцевої точки.<br>
         ▼ Розпилювач (Распылитель) призначено для створення плавних тонових зображень.<br>
         ▼ Гумка (Ластик) призначена для стирання частини малюнка. Після активізації під панеллю інструментів відкривається вікно, у якому можна вибрати розміри гумки.<br>
          Якщо після вибору вищеназваних інструментів малювати при затисненій клавіші [Shift], курсор буде переміщуватися тільки за горизонталлю або вертикаллю.<br>
         ▼ Валик (Заливка) призначено для заповнення обмежених областей малюнка основним кольором або кольором фону. Для заповнення слід перемістити курсор у будь-яку точку замкненої області і натиснути ліву (заповнення основним кольором) або праву (заповнення кольором фону) кнопку миші.<br>
         ▼ Лінія (Линия) - побудова прямих ліній. Товщину ліній можна змінювати (вікно під панеллю інструментів). Для побудови лінії слід встановити курсор у початкову точку лінії і при затиснутій лівій або правій кнопці миші перемістити курсор у кінцеву точку. Якщо при переміщенні курсору утримується затиснутою клавіша [Shift], то кут нахилу лінії буде кратний 45°.<br>
         ▼ Крива лінія (Кривая линия) призначено для малювання дуг. Для побудови кривої слід намалювати пряму лінію, встановити курсор миші у вершину дуги, натиснути ліву або праву кнопку і переміщенням курсору змінити кривизну.<br>
         ▼ Прямокутник (Прямоугольник) призначено для побудови прямокутників. Курсор слід встановити у точку, яка буде вершиною прямокутника і при затиснутій лівій або правій кнопці перемістити в протилежну вершину, ці точки визначають діагональ прямокутника. Відпускання кнопки миші фіксує побудову. Якщо при переміщенні курсору утримується затиснутою клавіша [Shift], то будуватиметься квадрат.<br>
         ▼ Округлений прямокутник (Округленный прямоугольник) призначено для побудови прямокутника з округленими кутами. Побудова здійснюється аналогічно інструменту прямокутник.<br>
         ▼ Еліпс (Эллипс) призначено для побудови еліпсів. Для побудови курсор слід встановити у точку, яка буде центром еліпса і при затиснутій лівій або правій кнопці переміщати курсор. На екрані зображується контур еліпса. Відпускання кнопки миші фіксує побудову еліпса. Якщо при переміщенні курсора утримувати затиснутою клавішу [Shift], то будується коло.<br>
         ▼ Многокутник (Многоугольник) призначено для побудови многокутників. Для побудови многокутника слід перемістити курсор миші за контуром многокутника, натискуючи ліву або праву кнопку миші в кожній з вершин. В останній вершині слід натиснути кнопку двічі. Якщо при переміщенні курсору утримується затисненою клавіша [Shift], то многокутник міститиме кути тільки 90° і 45°.<br>
         ▼ Інструмент Надпис (Надпись) дозволяє робити надписи до малюнка. Після активізації цього інструменту на екран виводиться Панель атрибутов текста, де можна встановити шрифт, його розмір та стиль написання. Якщо панель не з’явилась на екрані, її можна викликати за допомогою пункту горизонтального меню Вид. В області малюнка слід виділити прямокутну область, де буде вводитися текст. Текстовий курсор (вертикальна мерехтлива риска) переміщується за допомогою клавіш переміщення курсору, для редагування тексту використовуються клавіші [BackSpace], [Delete]. Тобто, у рамках цієї прямокутної області використовуються можливості текстового редактора.<br>
          На панелі інструментів є два інструменти, які дозволяють виділяти фрагменти малюнка для подальшого його редагування.<br>
         ▼ Выделение. Інструмент призначений для виділення або вирізання прямокутного фрагмента малюнка. За принципом побудови прямокутника виділяється необхідний фрагмент. Далі його можна переміщувати, вилучати, змінювати колір, розмір, вносити до буферу обміну.<br>
         ▼ Выделение произвольной области. На відміну від попереднього цей інструмент дозволяє виділяти довільну область. Після активізації цього інструмента слід встановити курсор у будь-яку точку, що лежить на контурі області, і при затисненій лівій кнопці описати курсором замкнений контур, відпустити кнопку миші. Виділена область оточується пунктирним прямокутником. Для зняття виділення слід встановити курсор за межі виділеної області і клацнути по лівій кнопці миші.<br>
         ▼ Вибір кольору (Выбор цвета). Цей інструмент встановлює колір для малювання. Його використовують в тих випадках, коли на малюнку є потрібний колір для подальшого малювання, а на палітрі його немає, або складно знайти. Алгоритм застосування:<br>
         • взяти інструмент Вибір кольору (курсор миші змінить свою форму);<br>
         • навести в малюнку на колір, яким потрібно малювати і клікнути лівою кнопкою миші (колір лінії зміниться на обраний);<br>
         • вибрати будь-який інструмент малювання, він буде малювати обраним кольором.<br>
         </p>
         <p class="text_ab"><strong>Горизонтальне меню.</strong> Відкрити пункт меню можна за допомогою миші, встановивши курсор на потрібному пункті меню і натиснути ліву кнопку миші. В назві пункту меню або підменю є підкреслена літера. Це дає можливість одразу вибрати пункт меню або підменю, натиснувши комбінацію клавіш [Alt+підкреслена літера підменю]. В підменю потрібний пункт може бути вибраний або за допомогою миші (встановити мишу на потрібний пункт меню і натиснути ліву кнопку), або за допомогою клавіатури (клавішами вертикального переміщення курсору вибрати потрібний пункт і натиснути [Enter]). Деякі пункти підменю праворуч від назви пункту містять позначення комбінації клавіш. За допомогою цих комбінацій можна вибрати відповідний пункт підменю. Назви деяких пунктів підменю мають сірий колір. Це означає, що такі пункти в даний момент недоступні.</p>
         <p class="text_ab">
             ▼ Файл - робота з графічними файлами:<br>
             ▼ Создать - створення нового файлу (на екрані з’являється чистий аркуш для побудови малюнка). Якщо перед вибором цієї команди у вікні малювання редагувався малюнок, то Paint запропонує зберегти його.<br>
             ▼ Открыть.. - відкриття вже існуючого файлу для внесення змін або друкування. Після вибору команди на екрані відкривається вікно діалогу, яке містить список файлів, що зберігаються в поточній папці. За допомогою списку, що розкривається, у віконці Папка, можна змінити папку і дисковод для відкриття інших файлів. Поле Тип файла визначає, які типи файлів виводяться в списку в основному полі вікна. Файли, що створені в редакторі Paint, мають розширення *.bmp. Для зміни типу файлу треба відкрити список і вказати інший тип. Для відкриття файлу слід виділити його у списку файлів (у основній частині вікна) або ввести ім’я файлу в полі Имя файла і натиснути кнопку Открыть. Якщо файл, що відкривається, містить малюнок, розміри якого перевищують поточні розміри області малювання, то Paint запропонує розширити поточні розміри області малювання до розмірів малюнка.<br>
             ▼ Сохранить - збереження файлу.<br>
              Після вибору команди на екрані відкривається вікно діалогу, в якому можна вказати ім’я і тип файлу, ім’я папки, в якій буде збережено файл. Якщо тип файлу не вказано, то за замовчуванням встановлюється *.bmp.<br>
             ▼ Сохранить как... - збереження файлу під новим іменем, або у новій папці і для зміни палітри існуючого графічного файлу (наприклад, можна кольоровий малюнок записати як чорно/білий). Палітра кольорів змінюється в полі Тип файла.<br>
             ▼ Печать - друкування малюнка. При виконанні цієї команди відкривається вікно діалогу, в якому можна вказати тип принтера (Принтер/Имя), діапазон друкованих сторінок (можна друкувати окремі сторінки), кількість копій. Прапорець Печать в файл дозволяє направити друк у файл, який можна надрукувати поза середовищем програми Paint.<br>
             ▼ Правка – робота з буфером обміну, відмінити-повторити дії.<br>
             ▼ Вид - управління зображенням деяких елементів вікна. Це меню має команди-перемикачі, у стані ввімкнено, тобто елемент присутній на екрані, команди-перемикачі позначаються галочкою. Команда Вид\Набор инструментов керує виведенням на екран панелі інструментів, команда Вид\Палитра - палітри, а команда Вид\Строка состояния - рядка стану. Панель інструментів і палітру можна перемістити в будь-яке місце екрана типовим для середовища Windows способом.<br>
             ▼ Рисунок - зміна малюнка (встановлення розмірів, поворотів, нахилів).<br>
             ▼ Параметры - завантаження, зміна і збереження палітри кольорів.<br>
             ▼ ? - виклик довідкової інформації.<br>
         </p>
         <p class="text_ab">Редагування виділеного фрагмента. Перед редагуванням фрагмент малюнка треба обов’язково виділити. Команди редагування будуть діяти тільки на виділений фрагмент.</p>
         <p class="text_ab">Переміщення. Слід встановити курсор на будь-яку точку всередині фрагменту, курсор змінить свій вигляд на  , і при затисненій лівій кнопці миші перемістити фрагмент у потрібне місце.</p>
         <p class="text_ab">Очищення. Для очищення виділеного фрагменту слід натиснути клавішу [Delete] або вибрати команду Правка/Очистить выделение. Після цього виділений фрагмент зафарбовується кольором фону.</p>
         <p class="text_ab">Копіювання. Копіювання здійснюється стандартним способом через буфер обміну. Після виділення фрагменту малюнка слід активізувати команду Правка/Копировать, фрагмент поміститься у буфер обміну. Після команди Правка/Вырезать фрагмент малюнка поміщується у буфер обміну, але при виконанні цієї команди відбувається вилучення фрагменту. Вставка фрагменту з буфера обміну здійснюється командою Правка/Вставить. Фрагмент вставляється у лівий верхній кут малюнка, за допомогою миші його можна перемістити у будь-яке місце малюнка.</p>
         <p class="text_ab">Копіювання фрагменту можна здійснювати за допомогою миші. Ця операція аналогічна переміщенню, тільки при буксуванні фрагмента малюнка слід утримувати затисненою клавішу [Ctrl].</p>
         <p class="text_ab">В деяких випадках в документи потрібно вставляти фрагменти копій екрану. Такі копії можна зробити кнопкою PrintScreen на клавіатурі або комбінацією Alt+PrintScreen (копія активного вікна). Потім в редакторі виконати команду Вставить і редагувати малюнок.</p>
         <p class="text_ab">Зміна розмірів. Після виділення фрагменту на пунктирній рамці, яка обмежує фрагмент, з’являються маркери розміру. Слід встановити курсор миші на маркер (курсор набере вигляду ↔ або ↕) і пересувати курсор при затисненій лівій кнопці миші. Фрагмент малюнка буде збільшувати або зменшувати свій розмір.</p>
         <p class="text_ab"></p>
         <p class="text_ab"></p>
         <p class="text_ab"></p>
         <p class="text_ab"></p>
         <p class="text_ab"></p>
         <p class="text_ab"></p>
         <p class="text_ab"></p>
         <p class="text_ab"></p>
         
            <div class="vopros">
                <strong><center>ЗАВДАННЯ</center></strong><br>
                
                1. Створіть простий дитячий кросворд, де питання подані в вигляді малюнків.<br>
                <center><img src="images_lab/lab_2.2.png"></center>
                <center><img src="images_lab/lab_2.3.png"></center><br>
                2. Заповніть клітинки кросворда.<br>
                3. За допомогою меню Файл встановити необхідні параметри для макету аркуша (формат - А4, орієнтація - горизонтальна, поля: зверху та знизу - 3см, зліва та справа - 2,5см).<br>
                4. Зберегти малюнок у файлі під назвою Кросворд_Прізвище.bmp у своїй папці на диску d:.<br>
                5. Збережіть цей же малюнок в форматі .gif, а потім в форматі .jpg.<br>
                6. Порівняйте розміри ваших файлів в форматі .gif, .jpg та .bmp, які вони займають на диску.<br>
                7. Відкрийте попередній огляд малюнка перед друком.<br>
                8. Вийдіть з графічного редактора.<br>
                9. Запустіть програму Калькулятор (Пуск-Программы-Стандартные).<br>
                10. Зображення вікна скопіюйте в буфер обміну (Alt+PrintScreen).<br>
                11.Закрийте калькулятор. <br>
                12. Відкрийте редактор Paint.<br>
                13. Виконайте команду Правка-Вставить. Зображення вікна калькулятора з’явиться в робочій області.<br>
                14. Змініть отримане зображення так, як вказано на зразку (залишивши тільки кнопки з примітивними командами і змінивши заголовок вікна). Використовуйте інструменти: Выбор цветов, Выделение, Надпись; команди: Копировать, Вставить.
                <center><img src="images_lab/lab_2.4.png"></center><br>
                15. Збережіть зображення в своїй папці.<br>
            </div>
         </p>
         <p class="text_ab">
           <div class="vopros">
                <strong><center>КОНТРОЛЬНІ ПИТАННЯ</center></strong>
            1. Які основні елементи містить вікно редактора Paint?<br>
            2. Які команди основного меню впливають на вигляд вікна редактора Paint?<br>
            3. Яка різниця у використанні команд збереження файла Сохранить і Сохранить как...?<br>
            4. Які типи файлів може відкривати, зберігати редактор Paint?<br>
            5. Як змінити тип файлу, палітру кольорів?<br>
            6. Охарактеризувати команди роботи з графічними файлами (меню Файл).<br>
            7. Яке призначення мають кнопки панелі інструментів вікна Предварительный просмотр?<br>
            8. Які параметри можна встановити при друкуванні малюнків?<br>
            9. Які основні інструменти використовують для побудови малюнка, як ними користуватися?<br>
            10. Як вибрати основний колір або колір фону з палітри?<br>
            11. Чим відрізняється малювання олівцем від малювання пензлем?<br>
            12. З якою метою при використанні інструментів використовують клавішу [Shift]?<br>
            13. Як зробити копію екрану?<br>
           </div>
        </p>
         </span>

    </div>
    </div>


    <div id="templatemo_footer_wrapper">
    <div id="templatemo_footer">
    
        <a href="index.html">Головна</a> | <a href="lectures.html">Лекції</a> | <a href="lab_works.html">Лабораторні роботи</a> | <a href="other.html">Інше</a> | <a href="contact.html">Довідка</a><br /><br />

        ХДУ © 2017 <a href="http://ksuonline.kspu.edu/?lang=ru">KSU Online</a> | NewLife <a href="http://www.kspu.edu/default.aspx?lang=uk" target="_parent">ХДУ</a>
    
    </div> <!-- end of templatemo_footer -->
</div>

</body>
</html>