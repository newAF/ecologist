<?php 
    require "../include/db_2.php";
 ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Лекції.Ecologist</title>
<script>document.write('<script src="http://' + (location.host || 'localhost').split(':')[0] + ':35729/livereload.js?snipver=1"></' + 'script>')</script>
<meta name="keywords" content="green home, contact form, contact page, submit button, CSS, HTML" />
<meta name="description" content="Green Home Theme - Contact Page, Contact Form, Submit Button" />
<link href="../templatemo_style.css" rel="stylesheet" type="text/css" />
<link href="style_lecture.css" rel="stylesheet" type="text/css" />

</head>
<body>

<div id="templatemo_document_wrapper">
<div id="templatemo_wrapper">
        <?php include '../include/menu/menu_lectures.php' ?>
    <div id="templatemo_header">
        <div id="site_title">
            <h1><a href="http://www.templatemo.com"><img src="../images/templatemo_logo.png" alt="Logo" /></a></h1>
        </div> <!-- end of site_title -->    
        <div id="header_content">
               <p>"Главная склонность человека направлена на то, что соответствует природе."
                <em><span class="autor_1">Цицерон</span></em></p>
        </div>
    </div>

    <div id="templatemo_main">

        <center><span class="zagolovok">Лекція 10</span></center></br>
                <span class="nazvanie">
                   <strong> Виконання розрахунків у табличному процесорі MS Excel за допомогою формул та вбудованих функцій, побудова діаграм.</strong> <hr>
                </span>


        <p><em><h5>План</h5>
                1.Обчислення в електронних таблицях за допомогою формул користувача.<br/>
                2.Використання вбудованих функцій.<br/>    
                3.Побудова діаграм.<br/> </em></p><hr>

                <center><h4> 1. Обчислення в електронних таблицях за допомогою формул користувача.</h4></center>

         <span class="text">

         <p class="text_ab">Обчислення в таблицях виконуються за допомогою <strong>формул</strong>. Формула може складатися з математичних операторів, значень, посилань на вічки та імена функцій. Результатом виконання формули є деяке нове значення, що міститься у вічку, де знаходиться формула. Формула починається зі знаку рівняння =. У формулі можуть використовуватися арифметичні оператори +, –, *, /. Порядок обчислень визначається звичайними математичними законами.</p>
         <p class="text_ab">Приклади формул:     <strong>=(А4+В8)*2</strong>,     <strong>=F7*С14+B12</strong>.</p>
         <p class="text_ab"><strong>Константи</strong> – текстові або числові значення, що уводяться у вічко і не можуть змінюватися під час обчислень.</p>
         <p class="text_ab"><strong>Посилання на вічко або групу вічок</strong> – спосіб, яким можна зазначити конкретне вічко або декілька вічок. Посилання на окреме вічко здійснюється зазначенням його координат. Значення порожнього вічка дорівнює нулю. Посилання на вічка бувають двох типів: </p>
         <p class="text_ab"><strong>відносні</strong> (наприклад: F7) – визначають положення вічка відносно вічка з формулою.</p>
         <p class="text_ab"><strong>абсолютні</strong>, які відрізняються наявністю символу $ перед ім’ям строки або(та) стовпця (наприклад:<strong> $F$7</strong>), – завжди вказують на конкретні вічка таблиці.</p>
         <p class="text_ab">Відносні посилання автоматично корегуються при їх копіюванні, а відносні – ні. Наприклад, якщо пересунути або скопіювати формулу, яка містить обидва наведені посилання, на рядок нижче, то перше посилання автоматично зміниться на <strong>F8</strong>, а друге залишиться без зміни.</p>
         <p class="text_ab">Можливо комбінувати типи посилань, наприклад: <strong>F$7</strong>.</p>
         <p class="text_ab">Для посилання на групу вічок використовують спеціальні символи:</p>
         <p class="text_ab">: (двокрапка) – формує посилання на суцільний діапазон вічок. Наприклад: <strong>С4:С7</strong> звертається до вічок <strong>С4, С5</strong>, <strong>С6, С7</strong>.</p>
         <p class="text_ab">; (крапка з комою) – використовується для об’єднання кількох вічок або діапазонів вічок. Наприклад, <strong>D2:D4;D6</strong> – посилання на вічка <strong>D2, D3, D4 та D6</strong>.</p>
         <p class="text_ab">Для введення формули у вічко потрібно ввести знак = і математичний вираз, значенню якого повинен дорівнювати вміст цього вічка. Результат обчислення зображується безпосередньо у вічку, а текст формули з’являється в рядку редагування при виділенні цього вічка.</p>

                <center><h4> 2. Використання вбудованих функцій.</h4></center>

         <p class="text_ab"><strong>Функціями</strong> в Microsoft Excel називають заздалегідь визначені та іменовані формули, що мають один або декілька аргументів. Аргументами функцій можуть бути числові значення або адреси вічок. Наприклад:</p>
         <p class="text_ab"><strong>=СУММ(А5:А9)</strong> – сума вічок <strong>А5, А6, А7, А8 та А9</strong>;</p>
         <p class="text_ab"><strong>=СРЗНАЧ(G4:G6)</strong> – середнє значення вічок <strong>G4, G5 та G6</strong>.</p>
         <p class="text_ab">Функції можуть входити одна в іншу, наприклад:</p>
         <p class="text_ab"><strong>=ОКРУГЛ(СРЗНАЧ(G4:G6);2)</strong> – середнє значення по діапазону вічок <strong>G4:G6</strong>, округлене до двох десяткових знаків після крапки.</p>
         <p class="text_ab">Для введення функції у вічко необхідно: <br>
         ○ - виділити вічко для формули;<br>
         ○ - викликати <strong>Мастер функций</strong> за допомогою команди <strong>Функция</strong> меню <strong>Вставка</strong> або кнопки  ;<br>
         ○ - у діалоговому вікні, що з’явилося (рис.19), вибрати тип функції в переліку <strong>Категория</strong>, потім – потрібну функцію в переліку <strong>Функция</strong>;<br>
         ○ - натиснути кнопку <strong>ОК</strong>;<br>
         ○ - у полях <strong>Число1</strong>, <strong>Число2</strong> та ін. наступного вікна ввести аргументи функції (числові значення або посилання);<br>
         ○ - щоб не вказувати аргументи з клавіатури, можна натиснути кнопку  , яка знаходиться праворуч поля, і вказати мишею аргументи функції безпосередньо на робочому листі; для виходу з цього режиму слід натиснути кнопку  , яка буде знаходиться під рядком формул;<br>
         ○ - натиснути <strong>ОК</strong>.<br>
         </p>
         <center><img src="images_lec/lec_10.1.png"></center>
         <center>Рис.20.</center><br>
         <p class="text_ab">Швидко вставити у вічко функцію суми <strong>СУММ</strong> можна також за допомогою кнопки  .</p><br>
         <h4><center><strong>Повідомлення про помилки</strong></center></h4>
         <p class="text_ab">Якщо формула у вічку не може бути правильно обчислена, Microsoft Excel виводить у вічко повідомлення про помилку. Якщо у формулі є посилання на вічко, що містить значення помилки, то замість цієї формули також буде виводитися повідомлення про помилку.</p>
         <p class="text_ab">Значення помилок:<br>
         ▬ <strong>####</strong> – ширина вічка не дозволяє відобразити число в заданому форматі;<br>
         ▬ <strong>#ИМЯ?</strong> – Microsoft Excel не зміг розпізнати ім’я, використане в формулі;<br>
         ▬ <strong>#ДЕЛ/0!</strong> – у формулі робиться спроба ділення на нуль;<br>
         ▬ <strong>#ЧИСЛО!</strong> – порушені правила задавання операторів, прийняті в математиці;<br>
         ▬ <strong>#Н/Д</strong> – таке повідомлення може з’явитися, якщо як аргумент задано посилання на порожнє вічко;<br>
         ▬ <strong>#ПУСТО!</strong> – зазначено перетинання двох областей, які насправді не мають загальних вічок;<br>
         ▬ <strong>#ССЫЛКА!</strong> – у формулі задано посилання на неіснуюче вічко;<br>
         ▬ <strong>#ЗНАЧ!</strong> – використаний неприпустимий тип аргументу.<br>
         </p>

                <center><h4> 3. Побудова діаграм.</h4></center>
                
         <p class="text_ab"><strong>Діаграми</strong> використовуються для подання табличних даних у графічному вигляді, зручному для аналізу та порівняння.</p>
         <p class="text_ab">На діаграмі вміст кожного вічка зображується у вигляді крапок, ліній, смуг, стовпчиків, секторів та ін. Групи елементів даних, що відбивають вміст вічок одного рядку або стовпця на робочому аркуші, складають <strong>ряд даних</strong>. Більшість типів діаграм може відображати водночас декілька рядів даних.</p>
         <p class="text_ab">Microsoft Excel дозволяє створювати діаграми понад десяти типів, найчастіше з яких вживаються діаграми, зображені на рис.21:</p>
         <center><img src="images_lec/lec_10.2.png">
         <img src="images_lec/lec_10.3.png">
         <img src="images_lec/lec_10.4.png">
         <img src="images_lec/lec_10.5.png">
         <img src="images_lec/lec_10.6.png"></center>
         <center>Рис.21.</center><br>
         <p class="text_ab">
            • <strong>Кругові діаграми</strong>, які подають ряд даних у вигляді розбитого на сектори кола. Цей тип зручно використовувати для порівняння частин, що у сукупності складають одне ціле.<br>
            • <strong>Крапкові діаграми</strong> – призначені для відображення сукупності крапок, які мають дві числові координати: вертикальну та горизонтальну. Крапки, що складають ряд даних, можна з’єднати лінією – ламаною або згладженою. Цей тип діаграм найчастіше використовують для візуалізації математичних функцій та інших наукових даних.<br>
            • <strong>Графіки</strong> – дуже схожі на крапкові діаграми, але призначені для відо­браження таких сукупностей крапок, в яких лише одна з координат – вертикальна – є числовою, а по горизонтальній осі виводяться на рівній відстані назви стовпців, за якими створена діаграма.<br>
            • <strong>Гістограми та лінійчаті діаграми</strong> – подібні графікам, але відо­бражають дані у вигляді не крапок, а стовпців (гістограми) та горизонтальних смуг (лінійчаті діаграми).<br> 
         </p>
         <center><img src="images_lec/lec_10.7.png"></center>
         <center>Рис.21.</center><br>
         <p class="text_ab">Для створення діаграми необхідно:<br>
         ► на робочому аркуші виділити дані, по яким слід побудувати діаграму, включаючи вічка, що містять імена категорій або рядів, які будуть використовуватися в діаграмі як заголовки;<br>
         ► вибрати команду <strong>Диаграмма</strong> меню <strong>Вставка</strong> або натиснути кнопку  ;<br>
         ► у діалогових вікнах <strong>Майстра діаграм</strong> (рис.36) слід вибрати тип, формат та інші параметри діаграми, а також місце її розташуван­ня (на вже існуючому робочому листі або на новому);<br>
         ► для переходу до наступного кроку <strong>Майстра діаграм</strong> викорис­товується кнопка <strong>Далее</strong> >;<br>
         ► для побудови діаграми на будь-якому кроці можна натиснути кнопку <strong>Готово. Майстер діаграм</strong> самостійно закінчить побудову діаграми, використавши стандартні параметри настройки;<br>
         ► в останньому (4-му) вікні натиснути кнопку <strong>Готово.</strong><br>
         </p>
         <p class="text_ab">Діаграма складається з таких елементів (рис.22):
         <center><img src="images_lec/lec_10.8.png"></center>
         <center>Рис.22.</center><br></p>
         <p class="text_ab">
             • <strong>Область діаграми</strong> (1), в якій у довільних місцях розташовується решта елементів діаграми. Цю область можна заповнювати кольором а також обрамляти лінією.<br>
             • <strong>Область побудови</strong> (2), в якій розміщуються ряди даних. Вона також характеризується заливкою та обрамленням.<br>
             • <strong>Осі</strong> (3)– шкали, які служать для визначення розташування даних на діаграмі. Осі наявні у більшості типів діаграм (окрім кругових та кільцевих). Можна встановити потрібну товщину осі, наявність та зовнішній вигляд позначок ділень та підписів значень поряд з ними.<br>
             • <strong>Заголовки діаграми</strong> (4) та осей (5). Для цих елементів можна встановлювати шрифт, заливку та обрамлення, а також спосіб вирівнювання та орієнтацію тексту. За допомогою миші можна змінювати їх розміри.<br>
             • <strong>Легенда діаграми</strong> (6), яка складається автоматично та служить для розшифровки умовних позначень рядів даних (кольорів секторів чи стовпців, форми крапок та ін.). У легенди діаграми можна змінювати обрамлення, шрифт та заливку.<br>
             • <strong>Елемент даних</strong> (7) – відбиває на діаграмі значення одного вічка таблиці та дозволяє встановлювати для цієї позначки колір, наявність підписів (8) та інші характеристики, перелік яких залежить від типу діаграми.<br>
             • <strong>Ряд даних</strong> (9) – елемент, який дозволяє водночас редагувати вла­стивості усіх елементів даних, які належать до одного ряду даних.<br>
         </p>
         <p class="text_ab">Діаграму можна переміщати за допомогою миші. Команди для зміни типу, формату та інших характеристик діаграми в цілому зосереджені в меню <strong>Диаграмма</strong>.</p>
         <p class="text_ab">Редагування характеристик елементів діаграми здійснюється в діалоговому вікні, вміст якого залежить від того, який саме елемент редагується. Для відображення цього вікна слід або двічі натиснути на елементі діаграми мишею, або виділити елемент та вибрати відповідну команду у контекстному меню чи в меню <strong>Формат</strong>.</p>
         <p class="text_ab"></p>
         <p class="text_ab"></p>
         <p class="text_ab"></p>
         
         <center><span class="povna_ver">►Також ви можете завантажити повну версію цієї лекції◄</span></center><br>

         <center>
            <div class="download">
                <a href="download\Ecologist_lectures\ЛЕКЦІЯ 10.docx" class="button_d" download/>Завантажити</a>
            </div>
         </center>
         </span>

    </div>


    <div id="templatemo_footer_wrapper">
    <div id="templatemo_footer">
    
        <a href="index.html">Головна</a> | <a href="lectures.html">Лекції</a> | <a href="lab_works.html">Лабораторні роботи</a> | <a href="other.html">Інше</a> | <a href="contact.html">Довідка</a><br /><br />

        ХДУ © 2017 <a href="http://ksuonline.kspu.edu/?lang=ru">KSU Online</a> | NewLife <a href="http://www.kspu.edu/default.aspx?lang=uk" target="_parent">ХДУ</a>
    
    </div> <!-- end of templatemo_footer -->
</div>

</body>
</html>