<?php 
    require "../include/db_2.php";
 ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Лекції.Ecologist</title>
<script>document.write('<script src="http://' + (location.host || 'localhost').split(':')[0] + ':35729/livereload.js?snipver=1"></' + 'script>')</script>
<meta name="keywords" content="green home, contact form, contact page, submit button, CSS, HTML" />
<meta name="description" content="Green Home Theme - Contact Page, Contact Form, Submit Button" />
<link href="../templatemo_style.css" rel="stylesheet" type="text/css" />
<link href="style_lecture.css" rel="stylesheet" type="text/css" />

</head>
<body>

<div id="templatemo_document_wrapper">
<div id="templatemo_wrapper">
<?php include '../include/menu/menu_lectures.php' ?>
       
    
	<div id="templatemo_header">
        <div id="site_title">
	        <h1><a href="http://www.templatemo.com"><img src="../images/templatemo_logo.png" alt="Logo" /></a></h1>
        </div> <!-- end of site_title -->    
        <div id="header_content">
        	   <p>"Главная склонность человека направлена на то, что соответствует природе."
                <em><span class="autor_1">Цицерон</span></em></p>
        </div>
    </div>

    <div id="templatemo_main">

        <center><span class="zagolovok">Лекція 2</span></center></br>
                <span class="nazvanie">
                   <strong> Поняття інформаційних технологій та інформації. Основи роботи в операційній системі Windows.</strong> <hr>
                </span>


        <p><em><h5>План</h5>
                1.Поняття про файлову систему <br/>
                2.Призначення та функції операційної системи <br/>    
                3.Користувацький інтерфейс<br/> </em></p><hr>

        <center><h4> 1. Файлова система </h4></center><br>

         <span class="text">
         <p class="text_ab">Уся інформація (програми, документи, таблиці, малюнки та ін.) зберігається на ПК у вигляді іменованих файлів.</p>
         <p class="text_ab"><strong>Файл –</strong>це організована логічна одиниця зберігання інформації на зовнішньому носієві (жорсткому диску, дискеті, компакт-диску). Англійське слово “file” в перекладі українською мовою означає підшивка, картотека.</p>
         <p class="text_ab">Одиницею виміру розміру файлів (та кількості інформації взагалі) є байт. Можна вважати, що байт еквівалентний одному символу тек­стової інформації без урахування особливостей його оформлення. Для виміру більш великих обсягів інформації використовуються такі позасистемні одиниці:1 кілобайт (1Кб) = 210 байт = 1024 байт;
            1 мегабайт (1Мб) = 210 Кб = 1024 Кб;
            1 гігабайт (1Гб) = 210 Мб = 1024 Мб.
            Повне ім’я файлу складається з трьох компонентів:
            шляху доступу до файлу, імені файлу, розширення.</p>
         <p class="text_ab">Шлях доступу до файлу починається з логічного імені диску та включає послідовно імена всіх папок, через які він проходить, розділених символом «\» (зворотна коса риска).
            Логічні імена дисків і дисководів позначаються латинськими літерами з двокрапкою. Імена a: і b: зарезервовано за накопичувачами на гнучких магнітних дисках. Літерами c:, d: і т.д. позначаються розділи (логічні диски) вінчестеру. Останньою літерою іменується CD-ROM або DVD-ROM-дисководи (за наявності).
            Папка Windows відіграє таку ж роль, що і звичайна папка для зберігання документів в діловодстві: вона дозволяє упорядкувати документи, що зберігаються.</p>
         <p class="text_ab">Ім’я файлу або папки – це довільна комбінація букв, цифр, пробілів і розділових знаків, довжина якої не перевищує 256 символів.
            В імені можна використовувати як заголовні так і малі літери. Але ім’я не може містити таких символів:<br><strong>* ? \ / | : < > "</strong></p>
         <p class="text_ab">У деяких випадках (залежно від версії програмного забезпечення, що використовується) цей перелік слід доповнити ще й символами українського алфавіту є та ї.
            Додаткове обмеження накладено на повне ім’я файлу, яке не може мати довжину понад 260 символів. Крім того повне ім’я файлу або папки повинно бути унікальним, тобто не може існувати двох об’єктів з однаковим повним ім’ям (це правило не забороняє існування файлів з однаковим ім’ям, якщо вони вкладені у різні папки або містяться на різних дисках).
            Розширення вказує на тип файлу, тобто дозволяє користувачеві та операційній системі визначати адекватний метод отримання інформації з файлу. Розширення відокремлюється від імені крапкою. При використанні операційної системи Windows перед ім’ям файлу зазвичай зображується значок, який відповідає типу файлу (наведений нижче перелік не є повним):</p>
         <p class="text_ab"><img src="images_lec/lec_1.1.png"> - файли з розширенням <strong>com, exe</strong> – містять програми, безпосередньо придатні до виконання;<br>
         <img src="images_lec/lec_1.2.png"> - файли з розширенням <strong>bat</strong> – пакетні файли (створені користувачем текстові файли, які містять набір інструкцій для виконання операційною системою певних дій);<br>
         <img src="images_lec/lec_1.3.png"> - файли з розширенням <strong>doc</strong> – документи, створені у текстовому редакторі Microsoft Word;<br>
         <img src="images_lec/lec_1.4.png"> - файли з розширенням <strong>xls</strong> – електронні таблиці, створені у табличному процесорі Microsoft Excel;<br>
         <img src="images_lec/lec_1.5.png"> - файли з розширенням <strong>bmp</strong> – малюнки, створені у графічному редакторі Paint;<br>
         <img src="images_lec/lec_1.6.png"> - папки (будь-яке розширення) – містять перелік укладених файлів та папок. <br>Розглянемо приклад: повне ім’я файлу Лекція, який був створений засобами Microsoft Word та міститься на диску с: у папці Методика, яка входить до складу папки Мои документы, має такий вигляд: <br>С:\Мои документы\Методика\Лекція.doc <br>Збереження файлів на диску здійснюється у вигляді ієрархічної файлової структури (див. рис. 3). <br>
         <center><img src="images_lec/lec_1.7.png"></center><br>
         <center>Рис. 3. Ієрархічна файлова структура</center>
         </p>
         <p class="text_ab">На верхньому рівні цієї структури знаходиться єдиний об’єкт Рабочий стол. На другому рівні розташовані об’єкти, які розташовані на Робочому столі. До таких об’єктів відносяться як правило папки Мой компьютер, Мои документы, Корзина. Ці папки являються системними і дещо відрізняються від інших папок (наприклад, їх не можна знищувати або переміщувати). Однак вони, як і інші папки, призначені для зберігання інформації.</p>
         <center><h4> 2. Призначення та функції операційної системи Windows. </h4></center>
         <p class="text_ab">В останні роки серед користувачів IBM-сумісних комп’ютерів велику популярність має операційна система Windows. У перекладі з англійської мови слово Windows означає “вікна”. Ця назва зумовлена тим, що всі додатки до системи Windows оформлено на екрані ПК у вигляді вікон. Таким чином, робота з окремими видами програмного забезпечення Windows означає роботу в тому чи іншому вікні системи.
            Операційна система Microsoft Windows XP - швидкодійна 32-розрядна мережна операційна система з графічним інтерфейсом, вбудованими мережними засобами і орієнтована на роботу в мережі.
            В Windows XP реалізовані наступні архітектурні рішення: переміщуваність, багатозадачність, багатопроцесорність, масштабованість, архітектура клієнт-сервер, об'єктна архітектура, розширюваність, надійність і відмовостійкість, сумісність, доменна архітектура мереж, багаторівнева система безпеки тощо.
            Під переміщуваністю розуміється здатність Windows XP працювати як на CISC-, так і на RISC-процесорах.
            Багатозадачність - використовування одного процесора для роботи безлічі додатків або потоків (ниток, якщо додатки розбиваються на окремі виконувані компоненти).</p>
         <p class="text_ab">Багатопроцесорна обробка припускає наявність декількох процесорів, які можуть одночасно виконувати безліч ниток, по одній на кожний процесор, який є в комп'ютері .</p>
         <p class="text_ab">Масштабованість - можливість автоматичного використовування переваг доданих процесорів. Так, для прискорення роботи додатку операційна система може автоматично підключати додаткові однакові процесори.</p>
         <p class="text_ab">В Windows XP підтримується розподілена модель об'єктних компонентів (Distributed Component Object Model - DCOM). DCOM є системою програмних об'єктів, розроблених для неодноразового використовування і заміни. Вона дозволяє розробникам програмного забезпечення створювати складові додатки. DCOM базується на технології виклику видалених програм, що забезпечує використовування механізмів інтеграції розподілених додатків в мережі.</p>
         <p class="text_ab">Розширюваність Windows XP забезпечена відкритою модульною архітектурою, що дозволяє додавати нові модулі на всі рівні операційної системи. Модульна архітектура забезпечує можливість з'єднання з іншими мережними продуктами. Комп'ютери, що працюють під управлінням Windows NT, можуть взаємодіяти з серверами і клієнтами інших операційних систем.</p>
         <p class="text_ab">Характеристики - надійність і відмовостійкість - указують на те, що архітектура захищає операційну систему і додатки від руйнування.</p>
         <p class="text_ab">Сумісність означає, що Windows XP продовжує підтримувати додатки MS-DOS, Windows 3.x, OS/2, POSIX, а також широкий набір пристроїв і мереж.</p>
         <p class="text_ab">Для захисту інформації, що передається по мережі, використовуються різні методи кодування і є вбудований інтерфейс криптографування - Microsoft Cryptographic Application Program Interface (CryptoAPI).</p>

            <center><span class="povna_ver">►Також ви можете завантажити повну версію цієї лекції◄</span></center><br>

         <center>
            <div class="download">
                <a href="download\Ecologist_lectures\ЛЕКЦІЯ 2.docx" class="button_d" download/>Завантажити</a>
            </div>
         </center>
         </span>

    </div>


    <div id="templatemo_footer_wrapper">
    <div id="templatemo_footer">
    
        <a href="index.html">Головна</a> | <a href="lectures.html">Лекції</a> | <a href="lab_works.html">Лабораторні роботи</a> | <a href="other.html">Інше</a> | <a href="contact.html">Довідка</a><br /><br />

        ХДУ © 2017 <a href="http://ksuonline.kspu.edu/?lang=ru">KSU Online</a> | NewLife <a href="http://www.kspu.edu/default.aspx?lang=uk" target="_parent">ХДУ</a>
    
    </div> <!-- end of templatemo_footer -->
</div>

</body>
</html>