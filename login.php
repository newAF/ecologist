<?php 
    require "include/db.php"
 ?>
 <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Вхід.Ecologist</title>
<script>document.write('<script src="http://' + (location.host || 'localhost').split(':')[0] + ':35729/livereload.js?snipver=1"></' + 'script>')</script>
<meta name="keywords" content="green home, contact form, contact page, submit button, CSS, HTML" />
<meta name="description" content="Green Home Theme - Contact Page, Contact Form, Submit Button" />
<link href="templatemo_style.css" rel="stylesheet" type="text/css" />

</head>
<body>

<div id="templatemo_document_wrapper">
<div id="templatemo_wrapper">

	<?php include'include/menu_login.php' ?>
    
	<div id="templatemo_header">
        <div id="site_title">
	        <h1><img src="images/templatemo_logo.png" alt="Logo" /></a></h1>
        </div> <!-- end of site_title -->    
        <div id="header_content">
                <p>"Головна схильність людини спрямована на те, що відповідає природі."<br>
                    <em><span class="autor_1">-Цицерон-</span></em> </p>
        </div>
    </div>
        
        <div id="templatemo_main">
            <h4><strong style="color: green;">Введіть дані щоб ввійти на свій обліковий запис.</strong></h4>
                <?php 
                    $data = $_POST;
                    if( isset($data['do_login']) )
                    {
                        $errors = array();
                        $user = R::findOne('users', 'email = ?', array($data['email']));

                        if ($user) 
                        {
                            # логин существует
                            if (password_verify($data['password'], $user->password)) 
                            {
                                # логиним пользователя
                                $_SESSION['logged_user'] = $user;
                                echo '<div style="color: green;"><strong>Ви успішно ввійшли на свій обліковий запис. Перейти на <a href="/" style="color: #3AE2CE;">Головну</a></strong></div><hr>';
                            } else
                            {
                                $errors[] = '<strong>Невірно введений пароль!</strong>';
                            }
                        }   else 
                        {
                            $errors[] = '<strong>Користувача з таким Email не знайдено!</strong>';
                        }

                        if (! empty($errors)) 
                        {
                            echo '<div style="color: red;">'.array_shift($errors).'</div><hr>';
                        }
                    }
                ?>
                
                <content>
                    <form action="login.php" method="POST">
                    
                        <label for="email">Email:</label>
                        <input type="email" name="email" value="<?php echo @$data['email']; ?>">
                    
                        <label for="password">Пароль:</label>
                        <input type="password" name="password" value="<?php echo @$data['password']; ?>">
                   
                        <button class="button_d" type="submit" name="do_login">Вхід</button>
                        <a href="/signup.php" class="button_d"/>Зареєструватися</a>
                    
                    </form>
                </content>
    </div>
</div> <!-- end of wrapper -->
</div>

    <div id="templatemo_footer_wrapper">
    <div id="templatemo_footer">
    
        <a href="home.html">Головна</a> | <a href="lectures.html">Лекції</a> | <a href="lab_works.html">Лабораторні роботи</a> | <a href="info.html">Інше</a> | <a href="contact.html">Довідка</a><br /><br />

        Copyright © 2017 <a href="#">Your Company Name</a> | Designed by <a href="http://www.templatemo.com" target="_parent">Free CSS Templates</a>
    
    </div> <!-- end of templatemo_footer -->
</div>

</body>
</html>