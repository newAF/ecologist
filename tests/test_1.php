<?php 
	require "../include/db_2.php";
	$status1 = R::load('tests', 1);
 	$lvl_a = $_SESSION['logged_user']->lvl_a;
 	if ($status1->status == 'Закритий' && $lvl_a <= 1) {
 	  	header ('location: /errors/error_test.php');
 	}
 	if (empty($_SESSION['logged_user'])) {
 	  	header ('location: /errors/error_test.php');
 	}
 ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Тести. Ecologist</title>
<script>document.write('<script src="http://' + (location.host || 'localhost').split(':')[0] + ':35729/livereload.js?snipver=1"></' + 'script>')</script>
<meta name="keywords" content="green home website, free web template, free templates, CSS, HTML" />
<meta name="description" content="Green Home - free HTML CSS template by templatemo.com" />
<link href="../templatemo_style.css" rel="stylesheet" type="text/css" />
<link href="style_test.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../js/timer.js"></script>
</head>
<body onload="startTimer()">

<div id="templatemo_document_wrapper">
	
	<div id="templatemo_wrapper">
		<?php include '../include/menu/menu_test.php'; ?>
		<div id="templatemo_header">
			<div id="site_title">
				<h1><img src="../images/templatemo_logo.png" alt="Logo" /></h1>
			</div> <!-- end of site_title -->    
			<div id="header_content">
				<p>
					"Головна схильність людини спрямована на те, що відповідає природі."<br>
					<em><span class="autor_1">-Цицерон-</span></em>  </p>
				
			</div>
		</div>
		
		<div id="templatemo_main">
			
			<center><span class="zagolovok">Тест 1</span></center><br>
			<span class="nazvanie">
                   <strong> Предмет та основні поняття інформатики.</strong> <hr>
                </span>
				<em class="info">Увага! Питання з зірочкою мають декілька правильних відповідей. </em><hr><br>
				<p>
					<span style="color: green; font-size: 150%; font-weight: bold;  position: relative; float: center;">
						Ви розпочали тест! Залишилось часу-- 
					</span>
					<span id="my_timer" style="color: #f00; font-size: 150%; font-weight: bold; float: center;">
						00:09:00
					</span><hr>
				</p>
			<div class="pd-embed" id="pd_1486199544"></div>
			<form method="post" action="test_a_1.php">
		<div class="element">
			<p class="test_question"><strong>Питання №1:</strong> Вказати вид інформації, що не є її різновидом за способом передачі і сприйняття:</p>
			<p><input type="radio" name="question1" value="візуальна"> візуальна</p>
			<p><input type="radio" name="question1" value="тактильна"> тактильна</p>
			<p><input type="radio" name="question1" value="машинна"> машинна</p>
			<p><input type="radio" name="question1" value="спеціальна"> спеціальна</p>
			<p><input type="radio" name="question1" value="органолептична"> органолептична</p>
		</div><br>
		<div class="element">
			<p class="test_question"><strong>Питання №2:<span class="zir">*</sspan></strong> З наведених прикладів обрати існуючі форми подання інформації:</p>
			<p><input type="checkbox" name="question2_1" value="сигнальна"> сигнальна</p>
			<p><input type="checkbox" name="question2_2" value="символьна"> символьна</p>
			<p><input type="checkbox" name="question2_3" value="цифрова"> цифрова</p>
			<p><input type="checkbox" name="question2_4" value="текстова"> текстова</p>
			<p><input type="checkbox" name="question2_5" value="графічна"> графічна</p>
		</div><br>
		<div class="element">
			<p class="test_question"><strong>Питання №3:</strong> «Ви одержали інструкцію по виконанню завдання, але описане незнайомою мовою». В наведеному прикладі вказати властивість інформації:</p>
			<p><input type="radio" name="question3" value="корисність"> корисність</p>
			<p><input type="radio" name="question3" value="повнота"> повнота</p>
			<p><input type="radio" name="question3" value="зрозумілість"> зрозумілість</p>
			<p><input type="radio" name="question3" value="вірогідність"> вірогідність</p>
			<p><input type="radio" name="question3" value="актуальність"> актуальність</p>
		</div><br>
		<div class="element">
			<p class="test_question"><strong>Питання №4:</strong> «Ви одержали інформацію про розклад руху автобусу, котрий здійснював свій останній рейс вчора». В наведеному прикладі вказати властивість інформації:</p>
			<p><input type="radio" name="question4" value="корисність"> корисність</p>
			<p><input type="radio" name="question4" value="повнота"> повнота</p>
			<p><input type="radio" name="question4" value="зрозумілість"> зрозумілість</p>
			<p><input type="radio" name="question4" value="вірогідність"> вірогідність</p>
			<p><input type="radio" name="question4" value="актуальність"> актуальність</p>
		</div><br>
		<div class="element">
			<p class="test_question"><strong>Питання №5:</strong> В якій формі подається інформація в комп’ютері?</p>
			<p><input type="radio" name="question5" value="десятковій"> десятковій</p>
			<p><input type="radio" name="question5" value="двійковій"> двійковій</p>
			<p><input type="radio" name="question5" value="шістнадцятковій"> шістнадцятковій</p>
		</div><br>
		<div class="element">
			<p class="test_question"><strong>Питання №6:</strong> Найменшою одиницею вимірювання інформації є:</p>
			<p><input type="radio" name="question6" value="байт"> байт</p>
			<p><input type="radio" name="question6" value="біт"> біт</p>
			<p><input type="radio" name="question6" value="Кбайт"> Кбайт</p>
			<p><input type="radio" name="question6" value="символ"> символ</p>
		</div><br>
		<div class="element">
			<p class="test_question"><strong>Питання №7:</strong> Процес перетворення інформації в «машинну мову» називається?</p>
			<p><input type="radio" name="question7" value="кодуванням"> кодуванням</p>
			<p><input type="radio" name="question7" value="перетворенням"> перетворенням</p>
			<p><input type="radio" name="question7" value="записом"> записом</p>
			<p><input type="radio" name="question7" value="шифруванням"> шифруванням</p>
		</div><br>
		<div class="element">
			<p class="test_question"><strong>Питання №8:</strong> Інформація це - :</p>
			<p><input type="radio" name="question8" value="систематизовані поняття, уявлення, судження"> систематизовані поняття, уявлення, судження</p>
			<p><input type="radio" name="question8" value="документовані або публічно оголошені відомості про події та явища, що відображаються у суспільстві"> документовані або публічно оголошені відомості про події та явища, що відображаються у суспільстві</p>
			<p><input type="radio" name="question8" value="повідомлення, подані в формалізованому вигляді"> повідомлення, подані в формалізованому вигляді</p>
		</div><br>
		<div class="element">
			<p class="test_question"><strong>Питання №9:</strong> Вказати властивість інформації, що визначає чи відповідає інформація істинному стану речей з погляду того, хто її сприймає:</p>
			<p><input type="radio" name="question9" value="корисність"> корисність</p>
			<p><input type="radio" name="question9" value="повнота"> повнота</p>
			<p><input type="radio" name="question9" value="зрозумілість"> зрозумілість</p>
			<p><input type="radio" name="question9" value="вірогідність"> вірогідність</p>
			<p><input type="radio" name="question9" value="актуальність"> актуальність</p>
		</div><br>
		<div class="element">
			<p class="test_question"><strong>Питання №10:<span class="zir">*</span></strong> З наведених прикладів вказати інформаційні процеси:</p>
			<p><input type="checkbox" name="question10_1" value="збір"> збір</p>
			<p><input type="checkbox" name="question10_2" value="зберігання"> зберігання</p>
			<p><input type="checkbox" name="question10_3" value="кодування"> кодування</p>
			<p><input type="checkbox" name="question10_4" value="опрацювання"> опрацювання</p>
			<p><input type="checkbox" name="question10_5" value="перетворення"> перетворення</p>
			<p><input type="checkbox" name="question10_6" value="пошук"> пошук</p>
		</div><br>
		<div class="element">
			<p class="test_question"><strong>Питання №11:</strong> Форма подачі інформації, заснована на використанні символів-літер,цифр, знаків і застосовується для передачі нескладних сигналів називається?</p>
			<p><input type="radio" name="question11" value="символьна"> символьна</p>
			<p><input type="radio" name="question11" value="текстова"> текстова</p>
			<p><input type="radio" name="question11" value="сигнальна"> сигнальна</p>
			<p><input type="radio" name="question11" value="графічна"> графічна</p>
		</div><br>
		<div class="element">
			<p class="test_question"><strong>Питання №12:</strong> До якої форми подання інформації відносять креслення, схеми?</p>
			<p><input type="radio" name="question12" value="символьна"> символьна</p>
			<p><input type="radio" name="question12" value="текстова"> текстова</p>
			<p><input type="radio" name="question12" value="сигнальна"> сигнальна</p>
			<p><input type="radio" name="question12" value="графічна"> графічна</p>
		</div><br>

		<div class="element">
			<p><input type="submit" value="Надіслати відповіді" id="submit" /></p>
		</div>
	</form>
			<div class="cleaner"></div>
		</div>
		
	</div> <!-- end of wrapper -->
</div>

<div id="templatemo_footer_wrapper">
    <div id="templatemo_footer">
    
        <a href="../index.php">Головна</a> | <a href="../lectures.php">Лекції</a> | <a href="../lab_works.php">Лабораторні роботи</a> | <a href="../other.php">Інше</a> | <a href="../contact.php">Довідка</a><br /><br />

        ХДУ © 2017 <a href="http://ksuonline.kspu.edu/?lang=ru">KSU Online</a> | NewLife <a href="http://www.kspu.edu/default.aspx?lang=uk" target="_parent">ХДУ</a>
    
    </div> <!-- end of templatemo_footer -->
</div>

</body>
</html>