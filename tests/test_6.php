<?php 
	require "../include/db_2.php";
	$status1 = R::load('tests', 6);
 	$lvl_a = $_SESSION['logged_user']->lvl_a;
 	if ($status1->status == 'Закритий' && $lvl_a <= 1) {
 	  	header ('location: /errors/error_test.php');
 	}
 	if (empty($_SESSION['logged_user'])) {
 	  	header ('location: /errors/error_test.php');
 	}
 ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Головна. Ecologist</title>
<script>document.write('<script src="http://' + (location.host || 'localhost').split(':')[0] + ':35729/livereload.js?snipver=1"></' + 'script>')</script>
<meta name="keywords" content="green home website, free web template, free templates, CSS, HTML" />
<meta name="description" content="Green Home - free HTML CSS template by templatemo.com" />
<link href="../templatemo_style.css" rel="stylesheet" type="text/css" />
<link href="style_test.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../js/timer.js"></script>
</head>
<body onload="startTimer()">

<div id="templatemo_document_wrapper">
	
	<div id="templatemo_wrapper">
	
		<?php include '../include/menu/menu_test.php'; ?>
		
		<div id="templatemo_header">
			<div id="site_title">
				<h1><img src="../images/templatemo_logo.png" alt="Logo" /></h1>
			</div> <!-- end of site_title -->    
			<div id="header_content">
				<p>
					"Головна схильність людини спрямована на те, що відповідає природі."<br>
					<em><span class="autor_1">-Цицерон-</span></em>  </p>
			</div>
		</div>
		
		<div id="templatemo_main">
			
			<center><span class="zagolovok">Тест 6</span></center><br>
			<span class="nazvanie">
                   <strong> Прикладна програма текстовий процесор MS Word.</strong> <hr>
                </span>
                <span class="info">Увага! Питання з зірочкою мають декілька правильних відповідей. </span><hr><br>
                <p>
					<span style="color: green; font-size: 150%; font-weight: bold;  position: relative; float: center;">
						Ви розпочали тест! Залишилось часу-- 
					</span>
					<span id="my_timer" style="color: #f00; font-size: 150%; font-weight: bold; float: center;">
						00:15:00
					</span><hr>
				</p>

			<div class="pd-embed" id="pd_1486199544"></div>
	<form method="post" action="test_a_6.php">
		<div class="element">
			<p class="test_question"><strong>Вопрос №1:</strong> Текстовий процесор це:</p>
			<p><input type="radio" name="question1" value="програма для введення, редагування і збереження неформатованого тексту"> програма для введення, редагування і збереження неформатованого тексту</p>
			<p><input type="radio" name="question1" value="програма, що забезпечує створення, редагування і збереження форматованого тексту у файлах"> програма, що забезпечує створення, редагування і збереження форматованого тексту у файлах</p>
			<p><input type="radio" name="question1" value="програма для обробки графічних зображень"> програма для обробки графічних зображень</p>
		</div><br>
		<div class="element">
			<p class="test_question"><strong>Вопрос №2:</strong> Яка з наведених програм є текстовим процесором?</p>
			<p><input type="radio" name="question2" value="MS Word"> MS Word</p>
			<p><input type="radio" name="question2" value="Paint"> Paint</p>
			<p><input type="radio" name="question2" value="MS Excel"> MS Excel</p>
			<p><input type="radio" name="question2" value="Word Pad"> Word Pad</p>
			<p><input type="radio" name="question2" value="Блокнот"> Блокнот</p>
		</div><br>
		<div class="element">
			<p class="test_question"><strong>Вопрос №3:<span class="zir">*</sspan></strong> Вкажіть можливості, що притаманні текстовим процесорам:</p>
			<p><input type="checkbox" name="question3_1" value="робота з форматованим текстом"> робота з форматованим текстом</p>
			<p><input type="checkbox" name="question3_2" value="робота з векторним зображенням"> робота з векторним зображенням</p>
			<p><input type="checkbox" name="question3_3" value="вставка графічних та інших об’єктів"> вставка графічних та інших об’єктів</p>
			<p><input type="checkbox" name="question3_4" value="засоби оформлення сторінок"> засоби оформлення сторінок</p>
			<p><input type="checkbox" name="question3_5" value="забезпечення поліграфічної якості продукції"> забезпечення поліграфічної якості продукції</p>
		</div><br>
		<div class="element">
			<p class="test_question"><strong>Вопрос №4:<span class="zir">*</sspan></strong> Вказати, які елементи вікна текстового процесора MS Word відсутні у вікні графічного редактора Paint?</p>
			<p><input type="checkbox" name="question4_1" value="область завдань"> область завдань</p>
			<p><input type="checkbox" name="question4_2" value="рядок стану"> рядок стану</p>
			<p><input type="checkbox" name="question4_3" value="лінійки розмітки"> лінійки розмітки</p>
			<p><input type="checkbox" name="question4_4" value="панель інструментів"> панель інструментів</p>
			<p><input type="checkbox" name="question4_5" value="кнопки керування вікном"> кнопки керування вікном</p>
		</div><br>
		<div class="element">
			<p class="test_question"><strong>Вопрос №5:<span class="zir">*</sspan></strong> З яким розширенням за замовчуванням зберігається файл в текстовому процесорі MS Word?</p>
			<p><input type="checkbox" name="question5_1" value="TXT"> TXT</p>
			<p><input type="checkbox" name="question5_2" value="BMP"> BMP</p>
			<p><input type="checkbox" name="question5_3" value="DOS"> DOS</p>
			<p><input type="checkbox" name="question5_4" value="DOT"> DOT</p>
			<p><input type="checkbox" name="question5_5" value="DOC"> DOC</p>
		</div><br>
		<div class="element">
			<p class="test_question"><strong>Вопрос №6:<span class="zir">*</sspan></strong> Вказати, які дії входять до редагування тексту в текстовому процесорі MS Word?</p>
			<p><input type="checkbox" name="question6_1" value="вилучення символу"> вилучення символу</p>
			<p><input type="checkbox" name="question6_2" value="зміна зовнішнього вигляду символів"> зміна зовнішнього вигляду символів</p>
			<p><input type="checkbox" name="question6_3" value="вставка символу"> вставка символу</p>
			<p><input type="checkbox" name="question6_4" value="перевірка правопису"> перевірка правопису</p>
			<p><input type="checkbox" name="question6_5" value="зміна параметрів сторінки"> зміна параметрів сторінки</p>
			<p><input type="checkbox" name="question6_6" value="пошук та заміна символів"> пошук та заміна символів</p>
		</div><br>
		<div class="element">
			<p class="test_question"><strong>Вопрос №7:<span class="zir">*</sspan></strong> Вказати основні об’єкти текстового процесора MS Word:</p>
			<p><input type="checkbox" name="question7_1" value="символ"> символ</p>
			<p><input type="checkbox" name="question7_2" value="сторінка"> сторінка</p>
			<p><input type="checkbox" name="question7_3" value="абзац"> абзац</p>
			<p><input type="checkbox" name="question7_4" value="речення"> речення</p>
			<p><input type="checkbox" name="question7_5" value="малюнок"> малюнок</p>
			<p><input type="checkbox" name="question7_6" value="рядок"> рядок</p>
		</div><br>
		<div class="element">
			<p class="test_question"><strong>Вопрос №8:<span class="zir">*</sspan></strong> Яку комбінацію клавіш використовують для зміни мови в текстовому процесорі MS Word?</p>
			<p><input type="checkbox" name="question8_1" value="Alt+Shift"> Alt+Shift</p>
			<p><input type="checkbox" name="question8_2" value="Alt+Tab"> Alt+Tab</p>
			<p><input type="checkbox" name="question8_3" value="Ctrl+Shift"> Ctrl+Shift</p>
			<p><input type="checkbox" name="question8_4" value="Ctrl+Alt"> Ctrl+Alt</p>
		</div><br>
		<div class="element">
			<p class="test_question"><strong>Вопрос №9:</strong> Яка команда існує для додавання будь-якого символу до тексту в текстовому процесорі MS Word?</p>
			<p><input type="radio" name="question9" value="Правка/Вставити символ"> Правка/Вставити символ</p>
			<p><input type="radio" name="question9" value="Вставка/Символ"> Вставка/Символ</p>
			<p><input type="radio" name="question9" value="Формат/Вставка/Символи"> Формат/Вставка/Символи</p>
			<p><input type="radio" name="question9" value="Формат/Список"> Формат/Список</p>
		</div><br>
		<div class="element">
			<p class="test_question"><strong>Вопрос №10:</strong> Для видалення символу ліворуч від курсору в текстовому процесорі MS Word використовують кнопку:</p>
			<p><input type="radio" name="question10" value="Del"> Del</p>
			<p><input type="radio" name="question10" value="Delete"> Delete</p>
			<p><input type="radio" name="question10" value="Backspace"> Backspace</p>
			<p><input type="radio" name="question10" value="Insert"> Insert</p>
		</div><br>
		<div class="element">
			<p class="test_question"><strong>Вопрос №11:<span class="zir">*</sspan></strong> Як швидко виділити весь абзац в текстовому процесорі MS Word?</p>
			<p><input type="checkbox" name="question11_1" value="двічі клацнути на абзаці ЛКМ"> двічі клацнути на абзаці ЛКМ</p>
			<p><input type="checkbox" name="question11_2" value="натиснути Ctrl та клацнути в будь-якому місці абзацу"> натиснути Ctrl та клацнути в будь-якому місці абзацу</p>
			<p><input type="checkbox" name="question11_3" value="двічі клацнути на вільному полі ліворуч від абзацу"> двічі клацнути на вільному полі ліворуч від абзацу</p>
			<p><input type="checkbox" name="question11_4" value="тричі клацнути в будь-якому місці абзацу"> тричі клацнути в будь-якому місці абзацу</p>
		</div><br>
		<div class="element">
			<p class="test_question"><strong>Вопрос №12:<span class="zir">*</sspan></strong> Вкажіть команду для перевірки правопису?</p>
			<p><input type="checkbox" name="question12_1" value="Сервіс/Правопис"> Сервіс/Правопис</p>
			<p><input type="checkbox" name="question12_2" value="Формат/Правопис"> Формат/Правопис</p>
			<p><input type="checkbox" name="question12_3" value="F7"> F7</p>
			<p><input type="checkbox" name="question12_4" value="Правка/Правопис"> Правка/Правопис</p>
		</div><br>
		<div class="element">
			<p class="test_question"><strong>Вопрос №13:</strong> Форматування тексту в текстовому процесорі MS Word це - :</p>
			<p><input type="radio" name="question13" value="вилучення або додавання символу в тексті"> вилучення або додавання символу в тексті</p>
			<p><input type="radio" name="question13" value="вставка в текст будь-яких об’єктів"> вставка в текст будь-яких об’єктів</p>
			<p><input type="radio" name="question13" value="зміна зовнішнього вигляду тексту"> зміна зовнішнього вигляду тексту</p>
		</div><br>
		<div class="element">
			<p class="test_question"><strong>Вопрос №14:</strong> Вкажіть команду для форматування сторінки:</p>
			<p><input type="radio" name="question14" value="Файл/Параметри сторінки"> Файл/Параметри сторінки</p>
			<p><input type="radio" name="question14" value="Формат/Параметри сторінки"> Формат/Параметри сторінки</p>
			<p><input type="radio" name="question14" value="Формат/Шрифт"> Формат/Шрифт</p>
			<p><input type="radio" name="question14" value="Вид/Зміна параметрів сторінки"> Вид/Зміна параметрів сторінки</p>
		</div><br>
		<div class="element">
			<p class="test_question"><strong>Вопрос №15:</strong> Вкажіть команду для форматування символу:</p>
			<p><input type="radio" name="question15" value="Файл/Параметри сторінки"> Файл/Параметри сторінки</p>
			<p><input type="radio" name="question15" value="Формат/Параметри сторінки"> Формат/Параметри сторінки</p>
			<p><input type="radio" name="question15" value="Формат/Шрифт"> Формат/Шрифт</p>
			<p><input type="radio" name="question15" value="Вид/Зміна параметрів сторінки"> Вид/Зміна параметрів сторінки</p>
		</div><br>
		<div class="element">
			<p class="test_question"><strong>Вопрос №16:</strong> Вкажіть команду для форматування абзацу:</p>
			<p><input type="radio" name="question16" value="Файл/Параметри сторінки"> Файл/Параметри сторінки</p>
			<p><input type="radio" name="question16" value="Формат/Абзац"> Формат/Абзац</p>
			<p><input type="radio" name="question16" value="Формат/Шрифт"> Формат/Шрифт</p>
			<p><input type="radio" name="question16" value="Вид/Зміна параметрів сторінки"> Вид/Зміна параметрів сторінки</p>
		</div><br>
		<div class="element">
			<p class="test_question"><strong>Вопрос №17:</strong> Яка команда існує в текстовому процесорі MS Word для пошуку та заміни символів?</p>
			<p><input type="radio" name="question17" value="Правка/Знайти"> Правка/Знайти</p>
			<p><input type="radio" name="question17" value="Файл/Знайти"> Файл/Знайти</p>
			<p><input type="radio" name="question17" value="Формат/Знайти та замінити"> Формат/Знайти та замінити</p>
		</div><br>
		<div class="element">
			<p class="test_question"><strong>Вопрос №18:<span class="zir">*</sspan></strong> Які основні параметри можна змінити при форматуванні символів?</p>
			<p><input type="checkbox" name="question18_1" value="розмір шрифту"> розмір шрифту</p>
			<p><input type="checkbox" name="question18_2" value="відступи між строчками"> відступи між строчками</p>
			<p><input type="checkbox" name="question18_3" value="колір шрифту"> колір шрифту</p>
			<p><input type="checkbox" name="question18_4" value="вирівнювання абзацу"> вирівнювання абзацу</p>
			<p><input type="checkbox" name="question18_5" value="тип та накреслення шрифту"> тип та накреслення шрифту</p>
			<p><input type="checkbox" name="question18_5" value="поля сторінки"> поля сторінки</p>
		</div><br>
		<div class="element">
			<p class="test_question"><strong>Вопрос №19:</strong> Як до текстового документу додати формулу в текстовому процесорі MS Word?</p>
			<p><input type="radio" name="question19" value="команда Вставка/Обєкт/Microsoft Equation 3.0"> команда Вставка/Обєкт/Microsoft Equation 3.0</p>
			<p><input type="radio" name="question19" value="команда Вставка/Символ"> команда Вставка/Символ</p>
			<p><input type="radio" name="question19" value="команда Вставка/Формала"> команда Вставка/Формала</p>
			<p><input type="radio" name="question19" value="надрукувати необхідну формулу на клавіатурі"> надрукувати необхідну формулу на клавіатурі</p>
		</div><br>
		<div class="element">
			<p class="test_question"><strong>Вопрос №20:</strong> Як до текстового документу додати малюнок з файлу в текстовому процесорі MS Word?</p>
			<p><input type="radio" name="question20" value="команда Вставка/Обєкт"> команда Вставка/Обєкт</p>
			<p><input type="radio" name="question20" value="команда Вставка/Символ"> команда Вставка/Символ</p>
			<p><input type="radio" name="question20" value="команда Вставка/Малюнок/Із файлу"> команда Вставка/Малюнок/Із файлу</p>
			<p><input type="radio" name="question20" value="команда Вставка/Малюнок"> команда Вставка/Малюнок</p>
		</div><br>
		
		<div class="element">
			<p><input type="submit" value="Надіслати відповіді" id="submit" /></p>
		</div>
	</form>
			<div class="cleaner"></div>
		</div>
		
	</div> <!-- end of wrapper -->
</div>

<div id="templatemo_footer_wrapper">
    <div id="templatemo_footer">
    
        <a href="../index.php">Головна</a> | <a href="../lectures.php">Лекції</a> | <a href="../lab_works.php">Лабораторні роботи</a> | <a href="../other.php">Інше</a> | <a href="../contact.php">Довідка</a><br /><br />

        ХДУ © 2017 <a href="http://ksuonline.kspu.edu/?lang=ru">KSU Online</a> | NewLife <a href="http://www.kspu.edu/default.aspx?lang=uk" target="_parent">ХДУ</a>
    
    </div> <!-- end of templatemo_footer -->
</div>

</body>
</html>