<?php 
	require "../include/db_2.php";
 ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Головна. Ecologist</title>
<script>document.write('<script src="http://' + (location.host || 'localhost').split(':')[0] + ':35729/livereload.js?snipver=1"></' + 'script>')</script>
<meta name="keywords" content="green home website, free web template, free templates, CSS, HTML" />
<meta name="description" content="Green Home - free HTML CSS template by templatemo.com" />
<link href="../templatemo_style.css" rel="stylesheet" type="text/css" />

</head>
<body>

<div id="templatemo_document_wrapper">
	
	<div id="templatemo_wrapper">
	
		<?php include '../include/menu/menu_test.php'; ?>
		
		<div id="templatemo_header">
			<div id="site_title">
				<h1><img src="../images/templatemo_logo.png" alt="Logo" /></h1>
			</div> <!-- end of site_title -->    
			<div id="header_content">
				<p>
					"Головна схильність людини спрямована на те, що відповідає природі."<br>
					<em><span class="autor_1">-Цицерон-</span></em>  </p>
				
			</div>
		</div>
		
		<div id="templatemo_main">			
	<?php
	error_reporting(0);
	//1
	$question1 = $_POST['question1'];
	//2
	$question2_1 = $_POST['question2_1']; 
	$question2_2 = $_POST['question2_2']; 
	$question2_3 = $_POST['question2_3']; 
	$question2_4 = $_POST['question2_4']; 
	$question2_5 = $_POST['question2_5']; 
	//3
	$question3 = $_POST['question3'];
	//4
	$question4 = $_POST['question4'];
	//5
	$question5 = $_POST['question5'];
	//6
	$question6 = $_POST['question6'];
	//7
	$question7 = $_POST['question7'];
	//8
	$question8 = $_POST['question8'];
	//9
	$question9 = $_POST['question9'];
	//10
	$question10_1 = $_POST['question10_1']; 
	$question10_2 = $_POST['question10_2']; 
	$question10_3 = $_POST['question10_3']; 
	$question10_4 = $_POST['question10_4']; 
	$question10_5 = $_POST['question10_5'];
	$question10_6 = $_POST['question10_6'];
	//11
	$question11 = $_POST['question11'];
	//12
	$question12 = $_POST['question12'];

	$result = 0; // результат будет в балах правильных ответов
	
	//1
	if ($question1 == "спеціальна") {
		$result += 5;
	}
	
	//2
	$subresult = 0;
		/* если выбрано правильно то увеличиваем счётчик */
	if ($question2_2 != '') {
		$subresult++;
	}
	if ($question2_4 != '') {
		$subresult++;
	}
	if ($question2_5 != '') {
		$subresult++;
	}
		/* если выбрано не правильно то уменьшаем счётчик */
	if ($question2_1 != '') {
		$subresult--;
	}
	if ($question2_3 != '') {
		$subresult--;
	}
	
	if ($subresult == 3) {
		$result += 5;
	}

	//3
	if ($question3 == "зрозумілість") {
		$result += 5;
	}
	//4
	if ($question4 == "актуальність") {
		$result += 5;
	}
	//5
	if ($question5 == "двійковій") {
		$result += 5;
	}
	//6
	if ($question6 == "біт") {
		$result += 5;
	}
	//7
	if ($question7 == "кодуванням") {
		$result += 5;
	}
	//8
	if ($question8 == "документовані або публічно оголошені відомості про події та явища, що відображаються у суспільстві") {
		$result += 5;
	}
	//9
	if ($question9 == "вірогідність") {
		$result += 5;
	}
	//10
	$subresult_10 = 0;
		/* если выбрано правильно то увеличиваем счётчик */
	if ($question10_1 != '') {
		$subresult_10++;
	}
	if ($question10_2 != '') {
		$subresult_10++;
	}
	if ($question10_4 != '') {
		$subresult_10++;
	}
	if ($question10_6 != '') {
		$subresult_10++;
	}
		/* если выбрано не правильно то уменьшаем счётчик */
	if ($question10_3 != '') {
		$subresult_10--;
	}
	if ($question10_5 != '') {
		$subresult_10--;
	}
	
	if ($subresult_10 == 4) {
		$result += 5;
	}
	//11
	if ($question11 == "символьна") {
		$result += 5;
	}
	//12
	if ($question12 == "графічна") {
		$result += 5;
	}

	$ser_bal = round( $result / 12, 1) ;
	if ($ser_bal == 1) {
		$bal = $ser_bal .' бал';
	} elseif ($ser_bal >= 2 && $ser_bal <= 4) {
		$bal = $ser_bal .' бала';
	} else {
		$bal = $ser_bal .' балов';
	}
	$name = $_SESSION['logged_user']->name;
	$number_book = R::getCell("SELECT number_book FROM `users` WHERE name='$name'");
	$name1 = R::getCell("SELECT name FROM `students` WHERE number_book='$number_book'");
	$group = R::getCell("SELECT `group` FROM `students` WHERE number_book='$number_book'");
	$title = R::load('tests', 1 )->title;
	$rating = R::dispense('rating');
	$rating->name = $name1;
	$rating->id_group = $group;
	$rating->rating = $ser_bal;
	$rating->number_test = 1;
	$rating->title = $title;
	$rating->number_book = $number_book;
	R::store($rating);
	echo '<center>Ви пройшли тест на <strong>' . $bal . '.</strong> Ваш результат успішно збережений. Подивитись результати всіх учасників тесту ви можете на сторінці <strong>Інше/Результати тестування</strong></center>';
?>
			
			
			<div class="cleaner"></div>
		</div>
		
	</div> <!-- end of wrapper -->
</div>

<div id="templatemo_footer_wrapper">
    <div id="templatemo_footer">
    
        <a href="home.php">Головна</a> | <a href="lectures.php">Лекції</a> | <a href="lab_works.php">Лабораторні роботи</a> | <a href="other.php">Інше</a> | <a href="contact.php">Довідка</a><br /><br />

        ХДУ © 2017 <a href="http://ksuonline.kspu.edu/?lang=ru">KSU Online</a> | NewLife <a href="http://www.kspu.edu/default.aspx?lang=uk" target="_parent">ХДУ</a>
    
    </div> <!-- end of templatemo_footer -->
</div>

</body>
</html>