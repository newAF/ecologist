<?php 
	require "../include/db_2.php";
	$status1 = R::load('tests', 2);
 	$lvl_a = $_SESSION['logged_user']->lvl_a;
 	if ($status1->status == 'Закритий' && $lvl_a <= 1) {
 	  	header ('location: /errors/error_test.php');
 	}
 	if (empty($_SESSION['logged_user'])) {
 	  	header ('location: /errors/error_test.php');
 	}
 ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Головна. Ecologist</title>
<script>document.write('<script src="http://' + (location.host || 'localhost').split(':')[0] + ':35729/livereload.js?snipver=1"></' + 'script>')</script>
<meta name="keywords" content="green home website, free web template, free templates, CSS, HTML" />
<meta name="description" content="Green Home - free HTML CSS template by templatemo.com" />
<link href="../templatemo_style.css" rel="stylesheet" type="text/css" />
<link href="style_test.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../js/timer.js"></script>
</head>
<body onload="startTimer()">

<div id="templatemo_document_wrapper">
	
	<div id="templatemo_wrapper">
		<?php include '../include/menu/menu_test.php'; ?>
		<div id="templatemo_header">
			<div id="site_title">
				<h1><img src="../images/templatemo_logo.png" alt="Logo" /></h1>
			</div> <!-- end of site_title -->    
			<div id="header_content">
				<p>
					"Головна схильність людини спрямована на те, що відповідає природі."<br>
					<em><span class="autor_1">-Цицерон-</span></em>  </p>
				
			</div>
		</div>
		
		<div id="templatemo_main">
			
			<center><span class="zagolovok">Тест 2</span></center><br>
			<span class="nazvanie">
                   <strong> Архітектура ІВМ-сумісного комп’ютера.</strong> <hr>
                </span>
                <em class="info">Увага! Питання з зірочкою мають декілька правильних відповідей. </em><hr><br>
                <p>
					<span style="color: green; font-size: 150%; font-weight: bold;  position: relative; float: center;">
						Ви розпочали тест! Залишилось часу-- 
					</span>
					<span id="my_timer" style="color: #f00; font-size: 150%; font-weight: bold; float: center;">
						00:11:15
					</span><hr>
				</p>

			<div class="pd-embed" id="pd_1486199544"></div>
	<form method="post" action="test_a_2.php">
		<div class="element">
			<p class="test_question"><strong>Вопрос №1:</strong> Що означає принцип модульності?</p>
			<p><input type="radio" name="question1" value="модернізація персонального комп’ютера відбувається за висхідним напрямом-поколіннями"> модернізація персонального комп’ютера відбувається за висхідним напрямом-поколіннями</p>
			<p><input type="radio" name="question1" value="комп’ютерні модулі можуть виготовлятись різними виробниками"> комп’ютерні модулі можуть виготовлятись різними виробниками</p>
			<p><input type="radio" name="question1" value="персональний комп’ютер проектується як набір окремих функціональних блоків, що взаємодіють один з одним відповідно до стандартів інтерфейсу"> персональний комп’ютер проектується як набір окремих функціональних блоків, що взаємодіють один з одним відповідно до стандартів інтерфейсу</p>
			<p><input type="radio" name="question1" value="комп’ютерні програми, що були розроблені для ПК першого покоління, можуть виконуватись і ПК останнього покоління"> комп’ютерні програми, що були розроблені для ПК першого покоління, можуть виконуватись і ПК останнього покоління</p>
		</div><br>
		<div class="element">
			<p class="test_question"><strong>Вопрос №2:</strong> З наведених прикладів вказати той, що не є пристроєм введення інформації:</p>
			<p><input type="radio" name="question2" value="монітор"> монітор</p>
			<p><input type="radio" name="question2" value="трекбол"> трекбол</p>
			<p><input type="radio" name="question2" value="плоттер"> плоттер</p>
			<p><input type="radio" name="question2" value="сканер"> сканер</p>
		</div><br>
		<div class="element">
			<p class="test_question"><strong>Вопрос №3:</strong> Вказати, які з наведених запам’ятовуючих пристроїв не є зовнішнім:</p>
			<p><input type="radio" name="question3" value="гнучкі магнітні диски"> гнучкі магнітні диски</p>
			<p><input type="radio" name="question3" value="постійні запам’ятовуючі пристрої"> постійні запам’ятовуючі пристрої</p>
			<p><input type="radio" name="question3" value="жорсткі магнітні диски"> жорсткі магнітні диски</p>
			<p><input type="radio" name="question3" value="магнітні стрічки"> магнітні стрічки</p>
		</div><br>
		<div class="element">
			<p class="test_question"><strong>Вопрос №4:</strong> Вказати правильне визначення поняття «загальна шина»:</p>
			<p><input type="radio" name="question4" value="це шина, призначена для обміну даними між ОЗП і зовнішніми пристроями"> це шина, призначена для обміну даними між ОЗП і зовнішніми пристроями</p>
			<p><input type="radio" name="question4" value="шина призначена для передачі адреси даних"> шина призначена для передачі адреси даних</p>
			<p><input type="radio" name="question4" value="шина, що слугує каналом обміну керуючими сигналами між зовнішніми пристроями і центральним процесором"> шина, що слугує каналом обміну керуючими сигналами між зовнішніми пристроями і центральним процесором</p>
			<p><input type="radio" name="question4" value="це центральна інформаційна магістраль, що зв’язує зовнішні пристрої та ЦП"> це центральна інформаційна магістраль, що зв’язує зовнішні пристрої та ЦП</p>
		</div><br>
		<div class="element">
			<p class="test_question"><strong>Вопрос №5:</strong> Який з видів внутрішньої пам’яті забезпечує збереження інформації протягом сеансу роботи, а після вимкнення ПК інформація зникає?</p>
			<p><input type="radio" name="question5" value="кеш-пам'ять"> кеш-пам'ять</p>
			<p><input type="radio" name="question5" value="оперативна пам'ять"> оперативна пам'ять</p>
			<p><input type="radio" name="question5" value="постійна пам'ять"> постійна пам'ять</p>
			<p><input type="radio" name="question5" value="жодна"> жодна</p>
		</div><br>
		<div class="element">
			<p class="test_question"><strong>Вопрос №6:</strong> Який структурний компонент процесора забезпечує виконання арифметичних і логічних операцій над двійковими сигналами?</p>
			<p><input type="radio" name="question6" value="регістри пам’яті"> регістри пам’яті</p>
			<p><input type="radio" name="question6" value="пристрої керування"> пристрої керування</p>
			<p><input type="radio" name="question6" value="арифметико-логічні пристрої"> арифметико-логічні пристрої</p>
			<p><input type="radio" name="question6" value="пристрої введення-виведенн"> пристрої введення-виведенн</p>
		</div><br>
		<div class="element">
			<p class="test_question"><strong>Вопрос №7:</strong> Пристрій для виведення креслень на папір великого формату називається:</p>
			<p><input type="radio" name="question7" value="монітор"> монітор</p>
			<p><input type="radio" name="question7" value="трекбол"> трекбол</p>
			<p><input type="radio" name="question7" value="плоттер"> плоттер</p>
			<p><input type="radio" name="question7" value="сканер"> сканер</p>
		</div><br>
		<div class="element">
			<p class="test_question"><strong>Вопрос №8:</strong> Який пристрій перетворює інформацію з форми, призначеної для користувача у двійковий код?</p>
			<p><input type="radio" name="question8" value="пристрій виведення"> пристрій виведення</p>
			<p><input type="radio" name="question8" value="пристрій введення"> пристрій введення</p>
			<p><input type="radio" name="question8" value="центральний процесор"> центральний процесор</p>
			<p><input type="radio" name="question8" value="пристрій керування"> пристрій керування</p>
		</div><br>
		<div class="element">
			<p class="test_question"><strong>Вопрос №9:</strong> Вказати в якій функціональній зоні клавіш знаходяться кнопки Ctrl, Alt Shift?</p>
			<p><input type="radio" name="question9" value="алфавітно-цифровій"> алфавітно-цифровій</p>
			<p><input type="radio" name="question9" value="цифровій"> цифровій</p>
			<p><input type="radio" name="question9" value="зоні функціональних клавіш"> зоні функціональних клавіш</p>
			<p><input type="radio" name="question9" value="кнопок керування курсором"> кнопок керування курсором</p>
		</div><br>
		<div class="element">
			<p class="test_question"><strong>Вопрос №10:<span class="zir">*</sspan></strong> Вказати в якій функціональній зоні клавіш знаходиться кнопка Enter?</p>
			<p><input type="checkbox" name="question10_1" value="алфавітно-цифровій"> алфавітно-цифровій</p>
			<p><input type="checkbox" name="question10_2" value="цифровій"> цифровій</p>
			<p><input type="checkbox" name="question10_3" value="зоні функціональних клавіш"> зоні функціональних клавіш</p>
			<p><input type="checkbox" name="question10_4" value="кнопок керування курсором"> кнопок керування курсором</p>
		</div><br>
		<div class="element">
			<p class="test_question"><strong>Вопрос №11:</strong>  Вказати в якій функціональній зоні клавіш знаходиться кнопка →?</p>
			<p><input type="radio" name="question11" value="алфавітно-цифровій"> алфавітно-цифровій</p>
			<p><input type="radio" name="question11" value="цифровій"> цифровій</p>
			<p><input type="radio" name="question11" value="зоні функціональних клавіш"> зоні функціональних клавіш</p>
			<p><input type="radio" name="question11" value="кнопок керування курсором"> кнопок керування курсором</p>
		</div><br>
		<div class="element">
			<p class="test_question"><strong>Вопрос №12:</strong> Співпроцесор це –</p>
			<p><input type="radio" name="question12" value="пристрій, що допомагає основному мікропроцесору виконувати математичні операції"> пристрій, що допомагає основному мікропроцесору виконувати математичні операції</p>
			<p><input type="radio" name="question12" value="електронна схема, що виконує всі арифметичні обчислення й інше опрацювання інформації"> електронна схема, що виконує всі арифметичні обчислення й інше опрацювання інформації</p>
			<p><input type="radio" name="question12" value="пристрій, призначений для виконання алгоритмів, збережених в ОЗП у вигляді наборів команд"> пристрій, призначений для виконання алгоритмів, збережених в ОЗП у вигляді наборів команд</p>
		</div><br>
		<div class="element">
			<p class="test_question"><strong>Вопрос №13:</strong> Яка характеристика процесора відповідає за кількість елементарних операцій, що виконуються за одиницю часу?</p>
			<p><input type="radio" name="question13" value="тактова частота"> тактова частота</p>
			<p><input type="radio" name="question13" value="розрядність"> розрядність</p>
			<p><input type="radio" name="question13" value="ступінь інтеграції"> ступінь інтеграції</p>
		<div class="element">
			<p class="test_question"><strong>Вопрос №14:</strong> Які пристрої, розташовані в системному блоці, відповідають за опрацювання інформації і керування роботою комп’ютера?</p>
			<p><input type="radio" name="question14" value="електронні схеми"> електронні схеми</p>
			<p><input type="radio" name="question14" value="блок живлення"> блок живлення</p>
			<p><input type="radio" name="question14" value="накопичувачі"> накопичувачі</p>
		</div><br>
		<div class="element">
			<p class="test_question"><strong>Вопрос №15:<span class="zir">*</sspan></strong> Вказати пристрій, що входить до структури комп’ютера за фон Нейманом:</p>
			<p><input type="checkbox" name="question15_1" value="пам'ять"> пам'ять</p>
			<p><input type="checkbox" name="question15_2" value="пристрої керування"> пристрої керування</p>
			<p><input type="checkbox" name="question15_3" value="арифметико-логічний пристрій"> арифметико-логічний пристрій</p>
			<p><input type="checkbox" name="question15_4" value="акумулятор"> акумулятор</p>
			<p><input type="checkbox" name="question15_5" value="пристрій виведення"> пристрій виведення</p>
			<p><input type="checkbox" name="question15_6" value="жодного"> жодного</p>
		</div><br>
		
		<div class="element">
			<p><input type="submit" value="Надіслати відповіді" id="submit" /></p>
		</div>
	</form>
			<div class="cleaner"></div>
		</div>
		
	</div> <!-- end of wrapper -->
</div>

<div id="templatemo_footer_wrapper">
    <div id="templatemo_footer">
    
        <a href="../index.php">Головна</a> | <a href="../lectures.php">Лекції</a> | <a href="../lab_works.php">Лабораторні роботи</a> | <a href="../other.php">Інше</a> | <a href="../contact.php">Довідка</a><br /><br />

        ХДУ © 2017 <a href="http://ksuonline.kspu.edu/?lang=ru">KSU Online</a> | NewLife <a href="http://www.kspu.edu/default.aspx?lang=uk" target="_parent">ХДУ</a>
    
    </div> <!-- end of templatemo_footer -->
</div>

</body>
</html>