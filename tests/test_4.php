<?php 
	require "../include/db_2.php";
	$status1 = R::load('tests', 4);
 	$lvl_a = $_SESSION['logged_user']->lvl_a;
 	if ($status1->status == 'Закритий' && $lvl_a <= 1) {
 	  	header ('location: /errors/error_test.php');
 	}
 	if (empty($_SESSION['logged_user'])) {
 	  	header ('location: /errors/error_test.php');
 	}
 ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Головна. Ecologist</title>
<script>document.write('<script src="http://' + (location.host || 'localhost').split(':')[0] + ':35729/livereload.js?snipver=1"></' + 'script>')</script>
<meta name="keywords" content="green home website, free web template, free templates, CSS, HTML" />
<meta name="description" content="Green Home - free HTML CSS template by templatemo.com" />
<link href="../templatemo_style.css" rel="stylesheet" type="text/css" />
<link href="style_test.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../js/timer.js"></script>
</head>
<body onload="startTimer()">

<div id="templatemo_document_wrapper">
	
	<div id="templatemo_wrapper">
	
		<?php include '../include/menu/menu_test.php'; ?>
		
		<div id="templatemo_header">
			<div id="site_title">
				<h1><img src="../images/templatemo_logo.png" alt="Logo" /></h1>
			</div> <!-- end of site_title -->    
			<div id="header_content">
				<p>
					"Головна схильність людини спрямована на те, що відповідає природі."<br>
					<em><span class="autor_1">-Цицерон-</span></em>  </p>
			</div>
		</div>
		
		<div id="templatemo_main">
			
			<center><span class="zagolovok">Тест 4</span></center><br>
			<span class="nazvanie">
                   <strong> Операційна система MS Windows.</strong> <hr>
                </span>
                <span class="info">Увага! Питання з зірочкою мають декілька правильних відповідей. </span><hr><br>
                <p>
					<span style="color: green; font-size: 150%; font-weight: bold;  position: relative; float: center;">
						Ви розпочали тест! Залишилось часу-- 
					</span>
					<span id="my_timer" style="color: #f00; font-size: 150%; font-weight: bold; float: center;">
						00:15:00
					</span><hr>
				</p>

			<div class="pd-embed" id="pd_1486199544"></div>
	<form method="post" action="test_a_4.php">
		<div class="element">
			<p class="test_question"><strong>Вопрос №1:</strong> Комплекс програмних засобів, для взаємодії користувача з комп’ютером, здійснення управління його ресурсами називається:</p>
			<p><input type="radio" name="question1" value="сервісною програмою"> сервісною програмою</p>
			<p><input type="radio" name="question1" value="прикладною програмою"> прикладною програмою</p>
			<p><input type="radio" name="question1" value="операційною системою"> операційною системою</p>
			<p><input type="radio" name="question1" value="додатками операційної системи"> додатками операційної системи</p>
			<p><input type="radio" name="question1" value="інформаційною системою"> інформаційною системою</p>
		</div><br>
		<div class="element">
			<p class="test_question"><strong>Вопрос №2:</strong> Програми, які безпосередньо взаємодіють із користувачем і призначені для роботи під керуванням даної ОС називають:</p>
			<p><input type="radio" name="question2" value="додатками операційної системи"> додатками операційної системи</p>
			<p><input type="radio" name="question2" value="сервісною програмою"> сервісною програмою</p>
			<p><input type="radio" name="question2" value="прикладною програмою"> прикладною програмою</p>
			<p><input type="radio" name="question2" value="операційною системою"> операційною системою</p>
			<p><input type="radio" name="question2" value="інформаційною системою"> інформаційною системою</p>
		</div><br>
		<div class="element">
			<p class="test_question"><strong>Вопрос №3:<span class="zir">*</sspan></strong> Який інтерфейс має операційна система Windows:</p>
			<p><input type="checkbox" name="question3_1" value="командний"> командний</p>
			<p><input type="checkbox" name="question3_2" value="діалоговий"> діалоговий</p>
			<p><input type="checkbox" name="question3_3" value="текстовий"> текстовий</p>
			<p><input type="checkbox" name="question3_4" value="графічний"> графічний</p>
		</div><br>
		<div class="element">
			<p class="test_question"><strong>Вопрос №4:</strong> Основну частину екрану Windows займає:</p>
			<p><input type="radio" name="question4" value="вікно"> вікно</p>
			<p><input type="radio" name="question4" value="панель задач"> панель задач</p>
			<p><input type="radio" name="question4" value="головне меню"> головне меню</p>
			<p><input type="radio" name="question4" value="робочий стіл"> робочий стіл</p>
			<p><input type="radio" name="question4" value="значки"> значки</p>
		</div><br>
		<div class="element">
			<p class="test_question"><strong>Вопрос №5:</strong> Прямокутна ділянка екрану, в якій виконується програма-додаток називається:</p>
			<p><input type="radio" name="question5" value="вікно"> вікно</p>
			<p><input type="radio" name="question5" value="панель задач"> панель задач</p>
			<p><input type="radio" name="question5" value="головне меню"> головне меню</p>
			<p><input type="radio" name="question5" value="робочий стіл"> робочий стіл</p>
		</div><br>
		<div class="element">
			<p class="test_question"><strong>Вопрос №6:</strong> За допомогою якого елементу вікна можна перемістити вікно:</p>
			<p><input type="radio" name="question6" value="заголовка"> заголовка</p>
			<p><input type="radio" name="question6" value="кнопки розгортання"> кнопки розгортання</p>
			<p><input type="radio" name="question6" value="рамки вікна"> рамки вікна</p>
			<p><input type="radio" name="question6" value="рядка меню"> рядка меню</p>
		</div><br>
		<div class="element">
			<p class="test_question"><strong>Вопрос №7:<span class="zir">*</sspan></strong> Вказати спосіб за допомогою якого можна закрити вікно:</p>
			<p><input type="checkbox" name="question7_1" value="комбінацією клавіш Alt+F4"> комбінацією клавіш Alt+F4</p>
			<p><input type="checkbox" name="question7_2" value="виконавши команду Выход у пункті меню Файл"> виконавши команду Выход у пункті меню Файл</p>
			<p><input type="checkbox" name="question7_3" value="за допомогою контекстного меню"> за допомогою контекстного меню</p>
			<p><input type="checkbox" name="question7_4" value="натиснувши мишею кнопку закриття вікна"> натиснувши мишею кнопку закриття вікна</p>
		</div><br>
		<div class="element">
			<p class="test_question"><strong>Вопрос №8:</strong> Який тип вікна з’являються, коли прикладна програма або Windows потребує від користувача додаткової інформації для виконання будь-яких дій:</p>
			<p><input type="radio" name="question8" value="програмне"> програмне</p>
			<p><input type="radio" name="question8" value="інформаційне"> інформаційне</p>
			<p><input type="radio" name="question8" value="діалогове"> діалогове</p>
			<p><input type="radio" name="question8" value="діалогове з вкладниками"> діалогове з вкладниками</p>
		</div><br>
		<div class="element">
			<p class="test_question"><strong>Вопрос №9:</strong> Яку комбінація клавіш використовують для переходу між активними додатками:</p>
			<p><input type="radio" name="question9" value="Alt+F4"> Alt+F4</p>
			<p><input type="radio" name="question9" value="Alt+Tab"> Alt+Tab</p>
			<p><input type="radio" name="question9" value="Ctrl+Alt+Del"> Ctrl+Alt+Del</p>
			<p><input type="radio" name="question9" value="Ctrl+V"> Ctrl+V</p>
		</div><br>
		<div class="element">
			<p class="test_question"><strong>Вопрос №10:<span class="zir">*</sspan></strong> Як завантажити довідкову систему:</p>
			<p><input type="checkbox" name="question10_1" value="натиснути кнопку"> натиснути кнопку</p>
			<p><input type="checkbox" name="question10_2" value="виконати команду Сервис/Справка"> виконати команду Сервис/Справка</p>
			<p><input type="checkbox" name="question10_3" value="натиснути клавішу F1"> натиснути клавішу F1</p>
			<p><input type="checkbox" name="question10_4" value="виконати команду Файл/справочнай информация"> виконати команду Файл/справочнай информация</p>
		</div><br>
		<div class="element">
			<p class="test_question"><strong>Вопрос №11:<span class="zir">*</sspan></strong> Спеціальними папками Робочого столу є:</p>
			<p><input type="checkbox" name="question11_1" value="Мой компьютер"> Мой компьютер</p>
			<p><input type="checkbox" name="question11_2" value="Мои рисунки"> Мои рисунки</p>
			<p><input type="checkbox" name="question11_3" value="Internet Explorel"> Internet Explorel</p>
			<p><input type="checkbox" name="question11_4" value="Сетевое окружение"> Сетевое окружение</p>
			<p><input type="checkbox" name="question11_5" value="Корзина"> Корзина</p>
		</div><br>
		<div class="element">
			<p class="test_question"><strong>Вопрос №12:</strong> Яка команда контекстного меню слугує для створення папок:</p>
			<p><input type="radio" name="question12" value="Создать/Папку"> Создать/Папку</p>
			<p><input type="radio" name="question12" value="Вставить/Папку"> Вставить/Папку</p>
			<p><input type="radio" name="question12" value="Свойства/Создать/Папку"> Свойства/Создать/Папку</p>
		</div><br>
		<div class="element">
			<p class="test_question"><strong>Вопрос №13:<span class="zir">*</sspan></strong> Які об’єкти можна створити використовуючи контекстне меню:</p>
			<p><input type="checkbox" name="question13_1" value="архів"> архів</p>
			<p><input type="checkbox" name="question13_2" value="папку"> папку</p>
			<p><input type="checkbox" name="question13_3" value="вікно"> вікно</p>
			<p><input type="checkbox" name="question13_4" value="документ"> документ</p>
			<p><input type="checkbox" name="question13_5" value="диск"> диск</p>
		</div><br>
		<div class="element">
			<p class="test_question"><strong>Вопрос №14:<span class="zir">*</sspan></strong> Які елементи відображаються на панелі завдань?</p>
			<p><input type="checkbox" name="question14_1" value="значки папок"> значки папок</p>
			<p><input type="checkbox" name="question14_2" value="кнопка Пуск"> кнопка Пуск</p>
			<p><input type="checkbox" name="question14_3" value="область індикаторів"> область індикаторів</p>
			<p><input type="checkbox" name="question14_4" value="список файлів робочого столу"> список файлів робочого столу</p>
			<p><input type="checkbox" name="question14_5" value="кнопки відкритих додатків"> кнопки відкритих додатків</p>
		</div><br>
		<div class="element">
			<p class="test_question"><strong>Вопрос №15:</strong> Вкажіть спосіб для запуску панелі управління:</p>
			<p><input type="radio" name="question15" value="виконати команду Пуск/Настройки/Панель управління"> виконати команду Пуск/Настройки/Панель управління</p>
			<p><input type="radio" name="question15" value="вибрати команду Панель управління в контекстному меню"> вибрати команду Панель управління в контекстному меню</p>
			<p><input type="radio" name="question15" value="виконати команду Сервіс/Панель управління"> виконати команду Сервіс/Панель управління</p>
		</div><br>
		<div class="element">
			<p class="test_question"><strong>Вопрос №16:<span class="zir">*</sspan></strong> Серед наведених прикладів вкажіть програми, які є стандартними:</p>
			<p><input type="checkbox" name="question16_1" value="Microsoft Word"> Microsoft Word</p>
			<p><input type="checkbox" name="question16_2" value="Калькулятор"> Калькулятор</p>
			<p><input type="checkbox" name="question16_3" value="WinRar"> WinRar</p>
			<p><input type="checkbox" name="question16_4" value="Проводник"> Проводник</p>
			<p><input type="checkbox" name="question16_5" value="Word Pad"> Word Pad</p>
		</div><br>
		<div class="element">
			<p class="test_question"><strong>Вопрос №17:<span class="zir">*</sspan></strong> Вказати спосіб для копіювання папки:</p>
			<p><input type="checkbox" name="question17_1" value="обрати команду Копировать в контекстному меню"> обрати команду Копировать в контекстному меню</p>
			<p><input type="checkbox" name="question17_2" value="натиснути комбінацію клавіш Ctrl+V"> натиснути комбінацію клавіш Ctrl+V</p>
			<p><input type="checkbox" name="question17_3" value="виконати команду Правка/Копировать->Правка /Вставити в меню вікна"> виконати команду Правка/Копировать->Правка /Вставити в меню вікна</p>
			<p><input type="checkbox" name="question17_4" value="натиснути комбінацію клавіш Ctrl+С"> натиснути комбінацію клавіш Ctrl+С</p>
		</div><br>
		<div class="element">
			<p class="test_question"><strong>Вопрос №18:<span class="zir">*</sspan></strong> Для видалення папки в ОС Windows використовують:</p>
			<p><input type="checkbox" name="question18_1" value="команду Удалить в контекстному меню"> команду Удалить в контекстному меню</p>
			<p><input type="checkbox" name="question18_2" value="комбінацію клавіш Ctrl+V"> комбінацію клавіш Ctrl+V</p>
			<p><input type="checkbox" name="question18_3" value="команду Формат/Удалить"> команду Формат/Удалить</p>
			<p><input type="checkbox" name="question18_4" value="клавищу Delete"> клавищу Delete</p>
		</div><br>
		<div class="element">
			<p class="test_question"><strong>Вопрос №19:<span class="zir">*</sspan></strong> Упорядкування піктограм у вікні папки можна таким способом:</p>
			<p><input type="checkbox" name="question19_1" value="за авторами"> за авторами</p>
			<p><input type="checkbox" name="question19_2" value="за датою"> за датою</p>
			<p><input type="checkbox" name="question19_3" value="за ім’ям"> за ім’ям</p>
			<p><input type="checkbox" name="question19_4" value="за розміром"> за розміром</p>
			<p><input type="checkbox" name="question19_5" value="за списком"> за списком</p>
		</div><br>
		<div class="element">
			<p class="test_question"><strong>Вопрос №20:</strong> Рядок меню вікна призначено для:</p>
			<p><input type="radio" name="question20" value="інформування про властивості об’єктів вікна"> інформування про властивості об’єктів вікна</p>
			<p><input type="radio" name="question20" value="виконання команд"> виконання команд</p>
			<p><input type="radio" name="question20" value="керування вікном за допомогою команд"> керування вікном за допомогою команд</p>
			<p><input type="radio" name="question20" value="зміни розміру вікна"> зміни розміру вікна</p>
		</div><br>
		
		<div class="element">
			<p><input type="submit" value="Надіслати відповіді" id="submit" /></p>
		</div>
	</form>
			<div class="cleaner"></div>
		</div>
		
	</div> <!-- end of wrapper -->
</div>

<div id="templatemo_footer_wrapper">
    <div id="templatemo_footer">
    
        <a href="../index.php">Головна</a> | <a href="../lectures.php">Лекції</a> | <a href="../lab_works.php">Лабораторні роботи</a> | <a href="../other.php">Інше</a> | <a href="../contact.php">Довідка</a><br /><br />

        ХДУ © 2017 <a href="http://ksuonline.kspu.edu/?lang=ru">KSU Online</a> | NewLife <a href="http://www.kspu.edu/default.aspx?lang=uk" target="_parent">ХДУ</a>
    
    </div> <!-- end of templatemo_footer -->
</div>

</body>
</html>