<?php 
	require "../include/db_2.php";
 ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Головна. Ecologist</title>
<script>document.write('<script src="http://' + (location.host || 'localhost').split(':')[0] + ':35729/livereload.js?snipver=1"></' + 'script>')</script>
<meta name="keywords" content="green home website, free web template, free templates, CSS, HTML" />
<meta name="description" content="Green Home - free HTML CSS template by templatemo.com" />
<link href="../templatemo_style.css" rel="stylesheet" type="text/css" />

</head>
<body>

<div id="templatemo_document_wrapper">
	
	<div id="templatemo_wrapper">
	
		<?php include '../include/menu/menu_test.php'; ?>
		
		<div id="templatemo_header">
			<div id="site_title">
				<h1><img src="../images/templatemo_logo.png" alt="Logo" /></h1>
			</div> <!-- end of site_title -->    
			<div id="header_content">
				<p>
					"Головна схильність людини спрямована на те, що відповідає природі."<br>
					<em><span class="autor_1">-Цицерон-</span></em>  </p>
				
			</div>
		</div>
		
		<div id="templatemo_main">			
	<?php
	error_reporting(0);
	//1
	$question1 = $_POST['question1'];
	//2
	$question2_1 = $_POST['question2_1'];
	$question2_2 = $_POST['question2_2'];
	$question2_3 = $_POST['question2_3'];
	$question2_4 = $_POST['question2_4'];
	$question2_5 = $_POST['question2_5'];
	$question2_6 = $_POST['question2_6'];
	$question2_7 = $_POST['question2_7'];  
	//3
	$question3_1 = $_POST['question3_1'];
	$question3_2 = $_POST['question3_2'];
	$question3_3 = $_POST['question3_3'];
	$question3_4 = $_POST['question3_4'];
	$question3_5 = $_POST['question3_5'];
	$question3_6 = $_POST['question3_6'];
	$question3_7 = $_POST['question3_7'];
	//4
	$question4 = $_POST['question4'];
	//5
	$question5 = $_POST['question5'];
	//6
	$question6 = $_POST['question6'];
	//7
	$question7 = $_POST['question7'];
	//8
	$question8 = $_POST['question8'];
	//9
	$question9_1 = $_POST['question9_1'];
	$question9_2 = $_POST['question9_2'];
	$question9_3 = $_POST['question9_3'];
	$question9_4 = $_POST['question9_4'];
	$question9_5 = $_POST['question9_5'];
	$question9_6 = $_POST['question9_6'];
	$question9_7 = $_POST['question9_7'];
	//10
	$question10 = $_POST['question10'];  
	//11
	$question11_1 = $_POST['question11_1'];
	$question11_2 = $_POST['question11_2'];
	$question11_3 = $_POST['question11_3'];
	$question11_4 = $_POST['question11_4'];
	$question11_5 = $_POST['question11_5'];
	$question11_6 = $_POST['question11_6'];
	$question11_7 = $_POST['question11_7'];
	//12
	$question12 = $_POST['question12'];
	//13
	$question13 = $_POST['question13'];
	//14
	$question14 = $_POST['question14'];
	//15
	$question15 = $_POST['question15'];
	//16
	$question16 = $_POST['question16'];
	//17
	$question17_1 = $_POST['question17_1'];
	$question17_2 = $_POST['question17_2'];
	$question17_3 = $_POST['question17_3'];
	$question17_4 = $_POST['question17_4'];
	$question17_5 = $_POST['question17_5'];
	$question17_6 = $_POST['question17_6'];

	$result = 0; // результат будет в балах правильных ответов
	
	//1
	if ($question1 == "системне програмне забезпечення") {
		$result += 5;
	}
	//2
	$subresult = 0;
		/* если выбрано правильно то увеличиваем счётчик */
	if ($question2_1 != '') {
		$subresult++;
	}
	if ($question2_2 != '') {
		$subresult++;
	}
	if ($question2_5 != '') {
		$subresult++;
	}
		/* если выбрано не правильно то уменьшаем счётчик */
	if ($question2_3 != '') {
		$subresult--;
	}
	if ($question2_4 != '') {
		$subresult--;
	}
	if ($question2_6 != '') {
		$subresult--;
	}
	if ($question2_7 != '') {
		$subresult--;
	}
	
	if ($subresult == 3) {
		$result += 5;
	}
	//3
	$subresult = 0;
		/* если выбрано правильно то увеличиваем счётчик */
	if ($question3_3 != '') {
		$subresult++;
	}
	if ($question3_4 != '') {
		$subresult++;
	}
	if ($question3_6 != '') {
		$subresult++;
	}
		/* если выбрано не правильно то уменьшаем счётчик */
	if ($question3_1 != '') {
		$subresult--;
	}
	if ($question3_2 != '') {
		$subresult--;
	}
	if ($question3_5 != '') {
		$subresult--;
	}
	if ($question3_7 != '') {
		$subresult--;
	}

	if ($subresult == 3) {
		$result += 5;
	}
	//4//
	if ($question4 == "утиліти") {
		$result += 5;
	}
	//5//
	if ($question5 == "операційною системою") {
		$result += 5;
	}
	//6//
	if ($question6 == "файлом") {
		$result += 5;
	}
	//7//
	if ($question7 == "командним інтерпретатором") {
		$result += 5;
	}
	//8//
	if ($question8 == "інтерпретатором") {
		$result += 5;
	}
	//9//
	$subresult = 0;
		/* если выбрано правильно то увеличиваем счётчик */
	if ($question9_1 != '') {
		$subresult++;
	}
	if ($question9_3 != '') {
		$subresult++;
	}
	if ($question9_7 != '') {
		$subresult++;
	}
		/* если выбрано не правильно то уменьшаем счётчик */
	if ($question9_2 != '') {
		$subresult--;
	}
	if ($question9_4 != '') {
		$subresult--;
	}
	if ($question9_5 != '') {
		$subresult--;
	}
	if ($question9_6 != '') {
		$subresult--;
	}
	
	if ($subresult == 3) {
		$result += 5;
	}
	
	//10//
	if ($question10 == "вбудовані") {
		$result += 5;
	}
	//11//
	$subresult = 0;
		/* если выбрано правильно то увеличиваем счётчик */
	if ($question11_6 != '') {
		$subresult++;
	}
	if ($question11_7 != '') {
		$subresult++;
	}
		/* если выбрано не правильно то уменьшаем счётчик */
	if ($question11_1 != '') {
		$subresult--;
	}
	if ($question11_2 != '') {
		$subresult--;
	}
	if ($question11_3 != '') {
		$subresult--;
	}
	if ($question11_4 != '') {
		$subresult--;
	}
	if ($question11_5 != '') {
		$subresult--;
	}
	
	if ($subresult == 2) {
		$result += 5;
	}
	//12//
	if ($question12 == "графічний") {
		$result += 5;
	}
	//13//
	if ($question13 == "графічні редактори") {
		$result += 5;
	}
	//14//
	if ($question14 == "текстовий процесор") {
		$result += 5;
	}
	//15//
	if ($question15 == "експертні системи") {
		$result += 5;
	}
	//16//
	if ($question16 == "архіватор") {
		$result += 5;
	}
	//17//
	$subresult = 0;
		/* если выбрано правильно то увеличиваем счётчик */
	if ($question17_1 != '') {
		$subresult++;
	}
	if ($question17_2 != '') {
		$subresult++;
	}
	if ($question17_3 != '') {
		$subresult++;
	}
	if ($question17_6 != '') {
		$subresult++;
	}
		/* если выбрано не правильно то уменьшаем счётчик */
	if ($question17_4 != '') {
		$subresult--;
	}
	if ($question17_5 != '') {
		$subresult--;
	}
	if ($subresult == 4) {
		$result += 5;
	}

	$ser_bal = round( $result / 17, 1) ;
	if ($ser_bal == 1) {
		$bal = $ser_bal .' бал';
	} elseif ($ser_bal >= 2 && $ser_bal <= 4) {
		$bal = $ser_bal .' бала';
	} else {
		$bal = $ser_bal .' балов';
	}
	$name = $_SESSION['logged_user']->name;
	$number_book = R::getCell("SELECT number_book FROM `users` WHERE name='$name'");
	$name1 = R::getCell("SELECT name FROM `students` WHERE number_book='$number_book'");
	$group = R::getCell("SELECT `group` FROM `students` WHERE number_book='$number_book'");
	$title = R::load('tests', 3 )->title;
	$rating = R::dispense('rating');
	$rating->name = $name1;
	$rating->id_group = $group;
	$rating->rating = $ser_bal;
	$rating->number_test = 3;
	$rating->title = $title;
	$rating->number_book = $number_book;
	R::store($rating);
	echo '<center>Ви пройшли тест на <strong>' . $bal . '.</strong> Ваш результат успішно збережений. Подивитись результати всіх учасників тесту ви можете на сторінці <strong>Інше/Результати тестування</strong></center>';
?>
			
			
			<div class="cleaner"></div>
		</div>
		
	</div> <!-- end of wrapper -->
</div>

<div id="templatemo_footer_wrapper">
    <div id="templatemo_footer">
    
        <a href="../home.php">Головна</a> | <a href="../lectures.php">Лекції</a> | <a href="../lab_works.php">Лабораторні роботи</a> | <a href="../other.php">Інше</a> | <a href="../contact.php">Довідка</a><br /><br />

        ХДУ © 2017 <a href="http://ksuonline.kspu.edu/?lang=ru">KSU Online</a> | NewLife <a href="http://www.kspu.edu/default.aspx?lang=uk" target="_parent">ХДУ</a>
    
    </div> <!-- end of templatemo_footer -->
</div>

</body>
</html>