<?php 
	require "../include/db_2.php";
 ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Головна. Ecologist</title>
<script>document.write('<script src="http://' + (location.host || 'localhost').split(':')[0] + ':35729/livereload.js?snipver=1"></' + 'script>')</script>
<meta name="keywords" content="green home website, free web template, free templates, CSS, HTML" />
<meta name="description" content="Green Home - free HTML CSS template by templatemo.com" />
<link href="../templatemo_style.css" rel="stylesheet" type="text/css" />

</head>
<body>

<div id="templatemo_document_wrapper">
	
	<div id="templatemo_wrapper">
	
		<?php include '../include/menu/menu_test.php'; ?>
		
		<div id="templatemo_header">
			<div id="site_title">
				<h1><img src="../images/templatemo_logo.png" alt="Logo" /></h1>
			</div> <!-- end of site_title -->    
			<div id="header_content">
				<p>
					"Головна схильність людини спрямована на те, що відповідає природі."<br>
					<em><span class="autor_1">-Цицерон-</span></em>  </p>
				
			</div>
		</div>
		
		<div id="templatemo_main">			
	<?php
		error_reporting(0);
		//1
		$question1 = $_POST['question1'];
		//2
		$question2 = $_POST['question2'];  
		//3
		$question3_1 = $_POST['question3_1'];
		$question3_2 = $_POST['question3_2'];
		$question3_3 = $_POST['question3_3'];
		$question3_4 = $_POST['question3_4'];
		//4
		$question4 = $_POST['question4'];
		//5
		$question5 = $_POST['question5'];
		//6
		$question6 = $_POST['question6'];
		//7
		$question7_1 = $_POST['question7_1'];
		$question7_2 = $_POST['question7_2'];
		$question7_3 = $_POST['question7_3'];
		$question7_4 = $_POST['question7_4'];
		//8
		$question8 = $_POST['question8'];
		//9
		$question9 = $_POST['question9'];
		//10
		$question10_1 = $_POST['question10_1']; 
		$question10_2 = $_POST['question10_2']; 
		$question10_3 = $_POST['question10_3']; 
		$question10_4 = $_POST['question10_4']; 
		//11
		$question11_1 = $_POST['question11_1'];
		$question11_2 = $_POST['question11_2'];
		$question11_3 = $_POST['question11_3'];
		$question11_4 = $_POST['question11_4'];
		$question11_5 = $_POST['question11_5'];
		//12
		$question12 = $_POST['question12'];
		//13
		$question13_1 = $_POST['question13_1'];
		$question13_2 = $_POST['question13_2'];
		$question13_3 = $_POST['question13_3'];
		$question13_4 = $_POST['question13_4'];
		$question13_5 = $_POST['question13_5'];
		//14
		$question14_1 = $_POST['question14_1'];
		$question14_2 = $_POST['question14_2'];
		$question14_3 = $_POST['question14_3'];
		$question14_4 = $_POST['question14_4'];
		$question14_5 = $_POST['question14_5'];
		//15
		$question15 = $_POST['question15'];
		//16
		$question16_1 = $_POST['question16_1'];
		$question16_2 = $_POST['question16_2'];
		$question16_3 = $_POST['question16_3'];
		$question16_4 = $_POST['question16_4'];
		$question16_5 = $_POST['question16_5'];
		//17
		$question17_1 = $_POST['question17_1'];
		$question17_2 = $_POST['question17_2'];
		$question17_3 = $_POST['question17_3'];
		$question17_4 = $_POST['question17_4'];
		//18
		$question18_1 = $_POST['question18_1'];
		$question18_2 = $_POST['question18_2'];
		$question18_3 = $_POST['question18_3'];
		$question18_4 = $_POST['question18_4'];
		//19
		$question19_1 = $_POST['question19_1'];
		$question19_2 = $_POST['question19_2'];
		$question19_3 = $_POST['question19_3'];
		$question19_4 = $_POST['question19_4'];
		$question19_5 = $_POST['question19_5'];
		//20
		$question20 = $_POST['question20'];

		$result = 0; // результат будет в балах правильных ответов
		
		//1
		if ($question1 == "операційною системою") {
			$result += 5;
		}
		//2
		if ($question2 == "додатками операційної системи") {
			$result += 5;
		}
		//3
		$subresult = 0;
			/* если выбрано правильно то увеличиваем счётчик */
		if ($question3_2 != '') {
			$subresult++;
		}
		if ($question3_4 != '') {
			$subresult++;
		}
			/* если выбрано не правильно то уменьшаем счётчик */
		if ($question3_1 != '') {
			$subresult--;
		}
		if ($question3_3 != '') {
			$subresult--;
		}
		
		if ($subresult == 2) {
			$result += 5;
		}
		//4
		if ($question4 == "робочий стіл") {
			$result += 5;
		}
		//5
		if ($question5 == "вікно") {
			$result += 5;
		}
		//6
		if ($question6 == "заголовка") {
			$result += 5;
		}
		//7
		$subresult = 0;
			/* если выбрано правильно то увеличиваем счётчик */
		if ($question7_1 != '') {
			$subresult++;
		}
		if ($question7_2 != '') {
			$subresult++;
		}
		if ($question7_4 != '') {
			$subresult++;
		}
			/* если выбрано не правильно то уменьшаем счётчик */
		if ($question7_3 != '') {
			$subresult--;
		}
		
		if ($subresult == 3) {
			$result += 5;
		}
		//8
		if ($question8 == "діалогове") {
			$result += 5;
		}
		//9
		if ($question9 == "Alt+Tab") {
			$result += 5;
		}
		//10
		$subresult = 0;
			/* если выбрано правильно то увеличиваем счётчик */
		if ($question10_1 != '') {
			$subresult++;
		}
		if ($question10_3 != '') {
			$subresult++;
		}
			/* если выбрано не правильно то уменьшаем счётчик */
		if ($question10_2 != '') {
			$subresult--;
		}
		if ($question10_4 != '') {
			$subresult--;
		}
		
		if ($subresult == 2) {
			$result += 5;
		}
		//11
		$subresult = 0;
			/* если выбрано правильно то увеличиваем счётчик */
		if ($question11_1 != '') {
			$subresult++;
		}
		if ($question11_4 != '') {
			$subresult++;
		}
		if ($question11_5 != '') {
			$subresult++;
		}
			/* если выбрано не правильно то уменьшаем счётчик */
		if ($question11_3 != '') {
			$subresult--;
		}
		if ($question11_2 != '') {
			$subresult--;
		}
		
		if ($subresult == 3) {
			$result += 5;
		}
		//12
		if ($question12 == "Создать/Папку") {
			$result += 5;
		}
		//13
		$subresult = 0;
			/* если выбрано правильно то увеличиваем счётчик */
		if ($question13_2 != '') {
			$subresult++;
		}
		if ($question13_4 != '') {
			$subresult++;
		}
			/* если выбрано не правильно то уменьшаем счётчик */
		if ($question13_1 != '') {
			$subresult--;
		}
		if ($question13_3 != '') {
			$subresult--;
		}
		if ($question13_5 != '') {
			$subresult--;
		}
		
		if ($subresult == 2) {
			$result += 5;
		}
		//14
		$subresult = 0;
			/* если выбрано правильно то увеличиваем счётчик */
		if ($question14_2 != '') {
			$subresult++;
		}
		if ($question14_3 != '') {
			$subresult++;
		}
		if ($question14_5 != '') {
			$subresult++;
		}
			/* если выбрано не правильно то уменьшаем счётчик */
		if ($question14_1 != '') {
			$subresult--;
		}
		if ($question14_4 != '') {
			$subresult--;
		}
		
		if ($subresult == 3) {
			$result += 5;
		}
		//15
		if ($question15 == "виконати команду Пуск/Настройки/Панель управління") {
			$result += 5;
		}
		//16
		$subresult = 0;
			/* если выбрано правильно то увеличиваем счётчик */
		if ($question16_2 != '') {
			$subresult++;
		}
		if ($question16_4 != '') {
			$subresult++;
		}
		if ($question16_5 != '') {
			$subresult++;
		}
			/* если выбрано не правильно то уменьшаем счётчик */
		if ($question16_1 != '') {
			$subresult--;
		}
		if ($question16_3 != '') {
			$subresult--;
		}
		
		if ($subresult == 3) {
			$result += 5;
		}
		//17
		$subresult = 0;
			/* если выбрано правильно то увеличиваем счётчик */
		if ($question17_1 != '') {
			$subresult++;
		}
		if ($question17_3 != '') {
			$subresult++;
		}
		if ($question17_4 != '') {
			$subresult++;
		}
			/* если выбрано не правильно то уменьшаем счётчик */
		if ($question17_2 != '') {
			$subresult--;
		}
		
		if ($subresult == 3) {
			$result += 5;
		}
		//18
		$subresult = 0;
			/* если выбрано правильно то увеличиваем счётчик */
		if ($question18_1 != '') {
			$subresult++;
		}
		if ($question18_4 != '') {
			$subresult++;
		}
			/* если выбрано не правильно то уменьшаем счётчик */
		if ($question18_2 != '') {
			$subresult--;
		}
		if ($question18_3 != '') {
			$subresult--;
		}
		
		if ($subresult == 2) {
			$result += 5;
		}
		//19
		$subresult = 0;
			/* если выбрано правильно то увеличиваем счётчик */
		if ($question19_3 != '') {
			$subresult++;
		}
		if ($question19_4 != '') {
			$subresult++;
		}
			/* если выбрано не правильно то уменьшаем счётчик */
		if ($question19_1 != '') {
			$subresult--;
		}
		if ($question19_2 != '') {
			$subresult--;
		}
		if ($question19_5 != '') {
			$subresult--;
		}
		
		if ($subresult == 2) {
			$result += 5;
		}
		//20
		if ($question20 == "керування вікном за допомогою команд") {
			$result += 5;
		}

		$ser_bal = round( $result / 20, 1) ;
	if ($ser_bal == 1) {
		$bal = $ser_bal .' бал';
	} elseif ($ser_bal >= 2 && $ser_bal <= 4) {
		$bal = $ser_bal .' бала';
	} else {
		$bal = $ser_bal .' балов';
	}
	$name = $_SESSION['logged_user']->name;
	$number_book = R::getCell("SELECT number_book FROM `users` WHERE name='$name'");
	$name1 = R::getCell("SELECT name FROM `students` WHERE number_book='$number_book'");
	$group = R::getCell("SELECT `group` FROM `students` WHERE number_book='$number_book'");
	$title = R::load('tests', 4 )->title;
	$rating = R::dispense('rating');
	$rating->name = $name1;
	$rating->id_group = $group;
	$rating->rating = $ser_bal;
	$rating->number_test = 4;
	$rating->title = $title;
	$rating->number_book = $number_book;
	R::store($rating);
	echo '<center>Ви пройшли тест на <strong>' . $bal . '.</strong> Ваш результат успішно збережений. Подивитись результати всіх учасників тесту ви можете на сторінці <strong>Інше/Результати тестування</strong></center>';
?>
			
			
			<div class="cleaner"></div>
		</div>
		
	</div> <!-- end of wrapper -->
</div>

<div id="templatemo_footer_wrapper">
    <div id="templatemo_footer">
    
        <a href="../index.php">Головна</a> | <a href="../lectures.php">Лекції</a> | <a href="../lab_works.php">Лабораторні роботи</a> | <a href="../other.php">Інше</a> | <a href="../contact.php">Довідка</a><br /><br />

        ХДУ © 2017 <a href="http://ksuonline.kspu.edu/?lang=ru">KSU Online</a> | NewLife <a href="http://www.kspu.edu/default.aspx?lang=uk" target="_parent">ХДУ</a>
    
    </div> <!-- end of templatemo_footer -->
</div>

</body>
</html>