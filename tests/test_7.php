<?php 
	require "../include/db_2.php";
	$status1 = R::load('tests', 7);
 	$lvl_a = $_SESSION['logged_user']->lvl_a;
 	if ($status1->status == 'Закритий' && $lvl_a <= 1) {
 	  	header ('location: /errors/error_test.php');
 	}
 	if (empty($_SESSION['logged_user'])) {
 	  	header ('location: /errors/error_test.php');
 	}
 ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Головна. Ecologist</title>
<script>document.write('<script src="http://' + (location.host || 'localhost').split(':')[0] + ':35729/livereload.js?snipver=1"></' + 'script>')</script>
<meta name="keywords" content="green home website, free web template, free templates, CSS, HTML" />
<meta name="description" content="Green Home - free HTML CSS template by templatemo.com" />
<link href="../templatemo_style.css" rel="stylesheet" type="text/css" />
<link href="style_test.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../js/timer.js"></script>
</head>
<body onload="startTimer()">

<div id="templatemo_document_wrapper">
	
	<div id="templatemo_wrapper">
	
		<?php include '../include/menu/menu_test.php'; ?>
		
		<div id="templatemo_header">
			<div id="site_title">
				<h1><img src="../images/templatemo_logo.png" alt="Logo" /></h1>
			</div> <!-- end of site_title -->    
			<div id="header_content">
				<p>
					"Головна схильність людини спрямована на те, що відповідає природі."<br>
					<em><span class="autor_1">-Цицерон-</span></em>  </p>
			</div>
		</div>
		
		<div id="templatemo_main">
			
			<center><span class="zagolovok">Тест 7</span></center><br>
			<span class="nazvanie">
                   <strong> Прикладна програма табличний процесор MS Excel.</strong> <hr>
                </span>
                <span class="info">Увага! Питання з зірочкою мають декілька правильних відповідей. </span><hr><br>
                <p>
					<span style="color: green; font-size: 150%; font-weight: bold;  position: relative; float: center;">
						Ви розпочали тест! Залишилось часу-- 
					</span>
					<span id="my_timer" style="color: #f00; font-size: 150%; font-weight: bold; float: center;">
						00:16:30
					</span><hr>
				</p>

			<div class="pd-embed" id="pd_1486199544"></div>
	<form method="post" action="test_a_7.php">
		<div class="element">
			<p class="test_question"><strong>Вопрос №1:</strong> Табличним процесором називають:</p>
			<p><input type="radio" name="question1" value="програма для обробки графічних зображень"> програма для обробки графічних зображень</p>
			<p><input type="radio" name="question1" value="програмою для створення та обробки електронних таблиць"> програмою для створення та обробки електронних таблиць</p>
			<p><input type="radio" name="question1" value="пойменована сукупність зв’язаний даних, організована за певними правилами"> пойменована сукупність зв’язаний даних, організована за певними правилами</p>
		</div><br>
		<div class="element">
			<p class="test_question"><strong>Вопрос №2:</strong> Який режим опрацювання таблиці призначений для остаточного форматування таблиці перед друкуванням?</p>
			<p><input type="checkbox" name="question2_1" value="звичайний"> звичайний</p>
			<p><input type="checkbox" name="question2_2" value="попередній перегляд"> попередній перегляд</p>
			<p><input type="checkbox" name="question2_3" value="розмітка сторінки"> розмітка сторінки</p>
			<p><input type="checkbox" name="question2_4" value="стандартний"> стандартний</p>
		</div><br>
		<div class="element">
			<p class="test_question"><strong>Вопрос №3:</strong> Яка з наведених програм є табличним процесором?</p>
			<p><input type="radio" name="question3" value="MS Word"> MS Word</p>
			<p><input type="radio" name="question3" value="Paint"> Paint</p>
			<p><input type="radio" name="question3" value="MS Excel"> MS Excel</p>
			<p><input type="radio" name="question3" value="Word Pad"> Word Pad</p>
			<p><input type="radio" name="question3" value="MS Access"> MS Access</p>
		</div><br>
		<div class="element">
			<p class="test_question"><strong>Вопрос №4:</strong> Що не являється об’єктом табличного процесора MS Excel?</p>
			<p><input type="checkbox" name="question4_1" value="робоча книга"> робоча книга</p>
			<p><input type="checkbox" name="question4_2" value="робочий аркуш"> робочий аркуш</p>
			<p><input type="checkbox" name="question4_3" value="зображення"> зображення</p>
			<p><input type="checkbox" name="question4_4" value="рядки"> рядки</p>
			<p><input type="checkbox" name="question4_5" value="абзаци"> абзаци</p>
			<p><input type="checkbox" name="question4_5" value="клітинки"> клітинки</p>
		</div><br>
		<div class="element">
			<p class="test_question"><strong>Вопрос №5:</strong> Що може містити клітинка табличного процесора MS Excel?</p>
			<p><input type="checkbox" name="question5_1" value="текст"> текст</p>
			<p><input type="checkbox" name="question5_2" value="число"> число</p>
			<p><input type="checkbox" name="question5_3" value="формулу"> формулу</p>
			<p><input type="checkbox" name="question5_4" value="файл"> файл</p>
		</div><br>
		<div class="element">
			<p class="test_question"><strong>Вопрос №6:</strong> Як називається файл, створений у табличному процесорі MS Excel?</p>
			<p><input type="radio" name="question6" value="робоча книга"> робоча книга</p>
			<p><input type="radio" name="question6" value="робочий аркуш"> робочий аркуш</p>
			<p><input type="radio" name="question6" value="робочий документ"> робочий документ</p>
			<p><input type="radio" name="question6" value="макрос"> макрос</p>
		</div><br>
		<div class="element">
			<p class="test_question"><strong>Вопрос №7:</strong> Яке розширення має робоча книга табличного процесора MS Excel?</p>
			<p><input type="radio" name="question7" value="xls"> xls</p>
			<p><input type="radio" name="question7" value="txt"> txt</p>
			<p><input type="radio" name="question7" value="doc"> doc</p>
			<p><input type="radio" name="question7" value="bmp"> bmp</p>
		</div><br>
		<div class="element">
			<p class="test_question"><strong>Вопрос №8:</strong> Як позначаються стовпці в табличному процесорі MS Excel?</p>
			<p><input type="radio" name="question8" value="малими латинськими літерами"> малими латинськими літерами</p>
			<p><input type="radio" name="question8" value="цифрами"> цифрами</p>
			<p><input type="radio" name="question8" value="великими латинськими літерами"> великими латинськими літерами</p>
			<p><input type="radio" name="question8" value="спеціальними символами"> спеціальними символами</p>
		</div><br>
		<div class="element">
			<p class="test_question"><strong>Вопрос №9:</strong> Як позначаються строчки в табличному процесорі MS Excel?</p>
			<p><input type="radio" name="question9" value="малими латинськими літерами"> малими латинськими літерами</p>
			<p><input type="radio" name="question9" value="цифрами"> цифрами</p>
			<p><input type="radio" name="question9" value="великими латинськими літерами"> великими латинськими літерами</p>
			<p><input type="radio" name="question9" value="спеціальними символами"> спеціальними символами</p>
		</div><br>
		<div class="element">
			<p class="test_question"><strong>Вопрос №10:</strong> За допомогою яких дій можна відредагувати вміст клітинки?</p>
			<p><input type="radio" name="question10" value="двічі клацнути лівою кнопкою миші"> двічі клацнути лівою кнопкою миші</p>
			<p><input type="radio" name="question10" value="натиснути на клавішу Del"> натиснути на клавішу Del</p>
			<p><input type="radio" name="question10" value="натиснути на клавішу F2"> натиснути на клавішу F2</p>
			<p><input type="radio" name="question10" value="натиснути на клавішу Backspace"> натиснути на клавішу Backspace</p>
		</div><br>
		<div class="element">
			<p class="test_question"><strong>Вопрос №11:</strong> За допомогою якою дії можна зробити клітинку активною?</p>
			<p><input type="radio" name="question11" value="встановити вказівник миші у клітинку"> встановити вказівник миші у клітинку</p>
			<p><input type="radio" name="question11" value="клацнути правою кнопкою миші"> клацнути правою кнопкою миші</p>
			<p><input type="radio" name="question11" value="двічі клацнути лівою кнопкою миші"> двічі клацнути лівою кнопкою миші</p>
			<p><input type="radio" name="question11" value="клацнути лівою кнопкою миші"> клацнути лівою кнопкою миші</p>
		</div><br>
		<div class="element">
			<p class="test_question"><strong>Вопрос №12:</strong> Із клавіатури введено 15%. Який тип даних буде призначено?</p>
			<p><input type="radio" name="question12" value="текстовий"> текстовий</p>
			<p><input type="radio" name="question12" value="відсотковий"> відсотковий</p>
			<p><input type="radio" name="question12" value="дробовий"> дробовий</p>
			<p><input type="radio" name="question12" value="загальний"> загальний</p>
		</div><br>
		<div class="element">
			<p class="test_question"><strong>Вопрос №13:</strong> Як можна викликати команду для форматування клітинок в табличному процесорі MS Excel?</p>
			<p><input type="radio" name="question13" value="команда меню Вид/Формат клітинок"> команда меню Вид/Формат клітинок</p>
			<p><input type="radio" name="question13" value="команда меню Правка/Формат клітинок"> команда меню Правка/Формат клітинок</p>
			<p><input type="radio" name="question13" value="команда меню Формат/Формат клітинок"> команда меню Формат/Формат клітинок</p>
			<p><input type="radio" name="question13" value="команда меню Таблиці/Формат клітинок"> команда меню Таблиці/Формат клітинок</p>
		</div><br>
		<div class="element">
			<p class="test_question"><strong>Вопрос №14:</strong> Як об’єднати клітинки в табличному процесорі MS Excel?</p>
			<p><input type="radio" name="question14" value="виконати команду Формат клітинок/Вирівнювання"> виконати команду Формат клітинок/Вирівнювання</p>
			<p><input type="radio" name="question14" value="виконати команду Формат клітинок/вид"> виконати команду Формат клітинок/вид</p>
			<p><input type="radio" name="question14" value="виконати команду Правка/Вирівнювання"> виконати команду Правка/Вирівнювання</p>
			<p><input type="radio" name="question14" value="Виділити клітинки"> Виділити клітинки</p>
		</div><br>
		<div class="element">
			<p class="test_question"><strong>Вопрос №15:</strong> Із клавіатури введено 17,00 грн. Який тип даних буде призначено?</p>
			<p><input type="radio" name="question15" value="текстовий"> текстовий</p>
			<p><input type="radio" name="question15" value="числовий"> числовий</p>
			<p><input type="radio" name="question15" value="грошовий"> грошовий</p>
			<p><input type="radio" name="question15" value="відсотковий"> відсотковий</p>
			<p><input type="radio" name="question15" value="дробовий"> дробовий</p>
		</div><br>
		<div class="element">
			<p class="test_question"><strong>Вопрос №16:<span class="zir">*</sspan></strong> Які оператори використовуються при написанні формули в табличному процесорі MS Excel?</p>
			<p><input type="checkbox" name="question16_1" value="математичні"> математичні</p>
			<p><input type="checkbox" name="question16_2" value="статичні"> статичні</p>
			<p><input type="checkbox" name="question16_3" value="логічні"> логічні</p>
			<p><input type="checkbox" name="question16_4" value="відношень"> відношень</p>
		</div><br>
		<div class="element">
			<p class="test_question"><strong>Вопрос №17:</strong> Яка функція існує для знаходження суми чисел в табличному процесорі MS Excel?</p>
			<p><input type="radio" name="question17" value="СУМ"> СУМ</p>
			<p><input type="radio" name="question17" value="СУММА"> СУММА</p>
			<p><input type="radio" name="question17" value="ЕСЛИ"> ЕСЛИ</p>
			<p><input type="radio" name="question17" value="СРЗНАЧ"> СРЗНАЧ</p>
		</div><br>
		<div class="element">
			<p class="test_question"><strong>Вопрос №18:<span class="zir">*</sspan></strong> Яка функція серед вказаних входить до категорії логічних в табличному процесорі MS Excel?</p>
			<p><input type="checkbox" name="question18_1" value="СЧЕТЕСЛИ"> СЧЕТЕСЛИ</p>
			<p><input type="checkbox" name="question18_2" value="СУММА"> СУММА</p>
			<p><input type="checkbox" name="question18_3" value="ЕСЛИ"> ЕСЛИ</p>
			<p><input type="checkbox" name="question18_4" value="СРЗНАЧ"> СРЗНАЧ</p>
		</div><br>
		<div class="element">
			<p class="test_question"><strong>Вопрос №19:</strong> Яка функція серед вказаних входить до категорії статистичних в табличному процесорі MS Excel?</p>
			<p><input type="radio" name="question19" value="СЧЕТЕСЛИ"> СЧЕТЕСЛИ</p>
			<p><input type="radio" name="question19" value="СУММА"> СУММА</p>
			<p><input type="radio" name="question19" value="ЕСЛИ"> ЕСЛИ</p>
			<p><input type="radio" name="question19" value="МАХ"> МАХ</p>
		</div><br>
		<div class="element">
			<p class="test_question"><strong>Вопрос №20:</strong> Як вставити діаграму до табличного процесора MS Excel?</p>
			<p><input type="radio" name="question20" value="команда меню Вставка/Формат/Діаграма"> команда меню Вставка/Формат/Діаграма</p>
			<p><input type="radio" name="question20" value="команда меню Правка/Вставити діаграму"> команда меню Правка/Вставити діаграму</p>
			<p><input type="radio" name="question20" value="команда меню Вставка/Рисунок/Діаграма"> команда меню Вставка/Рисунок/Діаграма</p>
			<p><input type="radio" name="question20" value="команда меню Таблиці/Формат клітинок"> команда меню Таблиці/Формат клітинок</p>
		</div><br>
		<div class="element">
			<p class="test_question"><strong>Вопрос №21:<span class="zir">*</sspan></strong> Що не є об’єктом діаграми?</p>
			<p><input type="checkbox" name="question21_1" value="кнопка"> кнопка</p>
			<p><input type="checkbox" name="question21_2" value="легенда"> легенда</p>
			<p><input type="checkbox" name="question21_3" value="заголовок"> заголовок</p>
			<p><input type="checkbox" name="question21_4" value="область побудови"> область побудови</p>
			<p><input type="checkbox" name="question21_5" value="панель інструментів"> панель інструментів</p>
			<p><input type="checkbox" name="question21_6" value="підписи осей"> підписи осей</p>
		</div><br>
		<div class="element">
			<p class="test_question"><strong>Вопрос №22:<span class="zir">*</sspan></strong> Які типи діаграм є нестандартними?</p>
			<p><input type="checkbox" name="question22_1" value="логарифмічна"> логарифмічна</p>
			<p><input type="checkbox" name="question22_2" value="трубчата"> трубчата</p>
			<p><input type="checkbox" name="question22_3" value="пелюсткова"> пелюсткова</p>
			<p><input type="checkbox" name="question22_4" value="біржова"> біржова</p>
		</div><br>
		
		<div class="element">
			<p><input type="submit" value="Надіслати відповіді" id="submit" /></p>
		</div>
	</form>
			<div class="cleaner"></div>
		</div>
		
	</div> <!-- end of wrapper -->
</div>

<div id="templatemo_footer_wrapper">
    <div id="templatemo_footer">
    
        <a href="../index.php">Головна</a> | <a href="../lectures.php">Лекції</a> | <a href="../lab_works.php">Лабораторні роботи</a> | <a href="../other.php">Інше</a> | <a href="../contact.php">Довідка</a><br /><br />

        ХДУ © 2017 <a href="http://ksuonline.kspu.edu/?lang=ru">KSU Online</a> | NewLife <a href="http://www.kspu.edu/default.aspx?lang=uk" target="_parent">ХДУ</a>
    
    </div> <!-- end of templatemo_footer -->
</div>

</body>
</html>