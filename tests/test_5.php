<?php 
	require "../include/db_2.php";
	$status1 = R::load('tests', 5);
 	$lvl_a = $_SESSION['logged_user']->lvl_a;
 	if ($status1->status == 'Закритий' && $lvl_a <= 1) {
 	  	header ('location: /errors/error_test.php');
 	}
 	if (empty($_SESSION['logged_user'])) {
 	  	header ('location: /errors/error_test.php');
 	}
 ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Головна. Ecologist</title>
<script>document.write('<script src="http://' + (location.host || 'localhost').split(':')[0] + ':35729/livereload.js?snipver=1"></' + 'script>')</script>
<meta name="keywords" content="green home website, free web template, free templates, CSS, HTML" />
<meta name="description" content="Green Home - free HTML CSS template by templatemo.com" />
<link href="../templatemo_style.css" rel="stylesheet" type="text/css" />
<link href="style_test.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../js/timer.js"></script>
</head>
<body onload="startTimer()">

<div id="templatemo_document_wrapper">
	
	<div id="templatemo_wrapper">
	
		<?php include '../include/menu/menu_test.php'; ?>
		
		<div id="templatemo_header">
			<div id="site_title">
				<h1><img src="../images/templatemo_logo.png" alt="Logo" /></h1>
			</div> <!-- end of site_title -->    
			<div id="header_content">
				<p>
					"Головна схильність людини спрямована на те, що відповідає природі."<br>
					<em><span class="autor_1">-Цицерон-</span></em>  </p>
			</div>
		</div>
		
		<div id="templatemo_main">
			
			<center><span class="zagolovok">Тест 5</span></center><br>
			<span class="nazvanie">
                   <strong> Основи комп'ютерної графіки.</strong> <hr>
                </span>
                <span class="info">Увага! Питання з зірочкою мають декілька правильних відповідей. </span><hr><br>
                <p>
					<span style="color: green; font-size: 150%; font-weight: bold;  position: relative; float: center;">
						Ви розпочали тест! Залишилось часу-- 
					</span>
					<span id="my_timer" style="color: #f00; font-size: 150%; font-weight: bold; float: center;">
						00:15:00
					</span><hr>
				</p>

			<div class="pd-embed" id="pd_1486199544"></div>
	<form method="post" action="test_a_5.php">
		<div class="element">
			<p class="test_question"><strong>Вопрос №1:</strong> Комп’ютерна графіка це:</p>
			<p><input type="radio" name="question1" value="галузь людської діяльності, пов’язана з використанням комп’ютерів для створення і обробки візуальних зображень"> галузь людської діяльності, пов’язана з використанням комп’ютерів для створення і обробки візуальних зображень</p>
			<p><input type="radio" name="question1" value="галузь людської діяльності, пов’язана з використанням комп’ютерів для створення і обробки текстової інформації"> галузь людської діяльності, пов’язана з використанням комп’ютерів для створення і обробки текстової інформації</p>
			<p><input type="radio" name="question1" value="галузь людської діяльності, пов’язана з використанням комп’ютерів для обслуговування телеконференцій та форумів"> галузь людської діяльності, пов’язана з використанням комп’ютерів для обслуговування телеконференцій та форумів</p>
		</div><br>
		<div class="element">
			<p class="test_question"><strong>Вопрос №2:<span class="zir">*</sspan></strong> Які види графічних зображень існують:</p>
			<p><input type="checkbox" name="question2_1" value="растрові"> растрові</p>
			<p><input type="checkbox" name="question2_2" value="цифрові"> цифрові</p>
			<p><input type="checkbox" name="question2_3" value="векторні"> векторні</p>
			<p><input type="checkbox" name="question2_4" value="тривимірні"> тривимірні</p>
		</div><br>
		<div class="element">
			<p class="test_question"><strong>Вопрос №3:</strong> Векторне зображення це:</p>
			<p><input type="radio" name="question3" value="зображення, що являє собою масив пікселів"> зображення, що являє собою масив пікселів</p>
			<p><input type="radio" name="question3" value="зображення, що складається з примітивних анімаційних елементів"> зображення, що складається з примітивних анімаційних елементів</p>
			<p><input type="radio" name="question3" value="зображення, що складається з графічних примітивів(ліній, кіл, кривих), що можна описати математичним рівнянням"> зображення, що складається з графічних примітивів(ліній, кіл, кривих), що можна описати математичним рівнянням</p>
		</div><br>
		<div class="element">
			<p class="test_question"><strong>Вопрос №4:</strong> Піксель це:</p>
			<p><input type="radio" name="question4" value="кількість бітів, що використовуються для кодування кольору"> кількість бітів, що використовуються для кодування кольору</p>
			<p><input type="radio" name="question4" value="графічний примітив (коло, лінія, крива)"> графічний примітив (коло, лінія, крива)</p>
			<p><input type="radio" name="question4" value="неподільний елемент зображення, квадратної форми, який має певний колір"> неподільний елемент зображення, квадратної форми, який має певний колір</p>
		</div><br>
		<div class="element">
			<p class="test_question"><strong>Вопрос №5:<span class="zir">*</sspan></strong> Вказати характеристики растрових зображень:</p>
			<p><input type="checkbox" name="question5_1" value="великі обсяги файлів"> великі обсяги файлів</p>
			<p><input type="checkbox" name="question5_2" value="при збільшенні не втрачається якість зображення"> при збільшенні не втрачається якість зображення</p>
			<p><input type="checkbox" name="question5_3" value="дуже висока якість зображення"> дуже висока якість зображення</p>
			<p><input type="checkbox" name="question5_4" value="малі обсяги файлів"> малі обсяги файлів</p>
			<p><input type="checkbox" name="question5_5" value="у разі збільшення якість зображення погіршується"> у разі збільшення якість зображення погіршується</p>
		</div><br>
		<div class="element">
			<p class="test_question"><strong>Вопрос №6:<span class="zir">*</sspan></strong> Вказати характеристики векторних зображень:</p>
			<p><input type="checkbox" name="question6_1" value="великі обсяги файлів"> великі обсяги файлів</p>
			<p><input type="checkbox" name="question6_2" value="при збільшенні не втрачається якість зображення"> при збільшенні не втрачається якість зображення</p>
			<p><input type="checkbox" name="question6_3" value="дуже висока якість зображення"> дуже висока якість зображення</p>
			<p><input type="checkbox" name="question6_4" value="малі обсяги файлів"> малі обсяги файлів</p>
			<p><input type="checkbox" name="question6_5" value="у разі збільшення якість зображення погіршується"> у разі збільшення якість зображення погіршується</p>
		</div><br>
		<div class="element">
			<p class="test_question"><strong>Вопрос №7:<span class="zir">*</sspan></strong> Який колір не є базовим в колірній системі RGB?</p>
			<p><input type="checkbox" name="question7_1" value="червоний"> червоний</p>
			<p><input type="checkbox" name="question7_2" value="жовтий"> жовтий</p>
			<p><input type="checkbox" name="question7_3" value="пурпуровий"> пурпуровий</p>
			<p><input type="checkbox" name="question7_4" value="зелений"> зелений</p>
		</div><br>
		<div class="element">
			<p class="test_question"><strong>Вопрос №8:<span class="zir">*</sspan></strong> Який колір не є базовим в колірній системі CMYK?</p>
			<p><input type="checkbox" name="question8_1" value="червоний"> червоний</p>
			<p><input type="checkbox" name="question8_2" value="жовтий"> жовтий</p>
			<p><input type="checkbox" name="question8_3" value="пурпуровий"> пурпуровий</p>
			<p><input type="checkbox" name="question8_4" value="зелений"> зелений</p>
		</div><br>
		<div class="element">
			<p class="test_question"><strong>Вопрос №9:<span class="zir">*</sspan></strong> З наведених прикладів вказати формати графічних файлів:</p>
			<p><input type="checkbox" name="question9_1" value="BMP"> BMP</p>
			<p><input type="checkbox" name="question9_2" value="TXT"> TXT</p>
			<p><input type="checkbox" name="question9_3" value="GIF"> GIF</p>
			<p><input type="checkbox" name="question9_4" value="PNG"> PNG</p>
			<p><input type="checkbox" name="question9_5" value="DOC"> DOC</p>
		</div><br>
		<div class="element">
			<p class="test_question"><strong>Вопрос №10:<span class="zir">*</sspan></strong> Яка з наведених програм є графічним редактором:</p>
			<p><input type="checkbox" name="question10_1" value="MS Word"> MS Word</p>
			<p><input type="checkbox" name="question10_2" value="Paint"> Paint</p>
			<p><input type="checkbox" name="question10_3" value="MS Excel"> MS Excel</p>
			<p><input type="checkbox" name="question10_4" value="Corel Draw"> Corel Draw</p>
		</div><br>
		<div class="element">
			<p class="test_question"><strong>Вопрос №11:<span class="zir">*</sspan></strong> Як завантажити програму Paint?</p>
			<p><input type="checkbox" name="question11_1" value="відкривши значок програми Paint на робочому столі"> відкривши значок програми Paint на робочому столі</p>
			<p><input type="checkbox" name="question11_2" value="виконавши команду Пуск/Приложения/ Paint"> виконавши команду Пуск/Приложения/ Paint</p>
			<p><input type="checkbox" name="question11_3" value="виконавши команду Пуск/Программы/Стандартные/ Paint"> виконавши команду Пуск/Программы/Стандартные/ Paint</p>
		</div><br>
		<div class="element">
			<p class="test_question"><strong>Вопрос №12:</strong> Яких з наведених елементів не містить вікно програми Paint?</p>
			<p><input type="radio" name="question12" value="панель інструментів"> панель інструментів</p>
			<p><input type="radio" name="question12" value="область параметрів"> область параметрів</p>
			<p><input type="radio" name="question12" value="область індикаторів"> область індикаторів</p>
			<p><input type="radio" name="question12" value="робоча область"> робоча область</p>
		</div><br>
		<div class="element">
			<p class="test_question"><strong>Вопрос №13:</strong> Яку команду треба виконати для налаштування параметрів малюнка?</p>
			<p><input type="radio" name="question13" value="Файл/Параметри"> Файл/Параметри</p>
			<p><input type="radio" name="question13" value="Малюнок/Атрибути"> Малюнок/Атрибути</p>
			<p><input type="radio" name="question13" value="Правка/Параметри"> Правка/Параметри</p>
			<p><input type="radio" name="question13" value="Вид/Атрибути малюнка"> Вид/Атрибути малюнка</p>
		</div><br>
		<div class="element">
			<p class="test_question"><strong>Вопрос №14:<span class="zir">*</sspan></strong> Яких інструментів не має панель в Paint?</p>
			<p><input type="checkbox" name="question14_1" value="ластик"> ластик</p>
			<p><input type="checkbox" name="question14_2" value="ласо"> ласо</p>
			<p><input type="checkbox" name="question14_3" value="заливка"> заливка</p>
			<p><input type="checkbox" name="question14_4" value="чарівна паличка"> чарівна паличка</p>
			<p><input type="checkbox" name="question14_5" value="округлений прямокутник"> округлений прямокутник</p>
		</div><br>
		<div class="element">
			<p class="test_question"><strong>Вопрос №15:</strong> Вказати команду для зміни масштабу малюнка:</p>
			<p><input type="radio" name="question15" value="Вид/Масштаб"> Вид/Масштаб</p>
			<p><input type="radio" name="question15" value="Малюнок/Масштаб"> Малюнок/Масштаб</p>
			<p><input type="radio" name="question15" value="Формат/Масштаб"> Формат/Масштаб</p>
			<p><input type="radio" name="question15" value="Правка/Масштаб малюнка"> Правка/Масштаб малюнка</p>
		</div><br>
		<div class="element">
			<p class="test_question"><strong>Вопрос №16:</strong> Вказати команду для очищення області малювання:</p>
			<p><input type="radio" name="question16" value="Правка/Очистити"> Правка/Очистити</p>
			<p><input type="radio" name="question16" value="Рисунок/Очистити"> Рисунок/Очистити</p>
			<p><input type="radio" name="question16" value="Формат/Очистити"> Формат/Очистити</p>
		</div><br>
		<div class="element">
			<p class="test_question"><strong>Вопрос №17:</strong> Вказати команду для відображення малюнку:</p>
			<p><input type="radio" name="question17" value="Рисунок/Відобразити/Нахилити"> Рисунок/Відобразити/Нахилити</p>
			<p><input type="radio" name="question17" value="Рисунок/Відобразити/Повернути"> Рисунок/Відобразити/Повернути</p>
			<p><input type="radio" name="question17" value="Файл/Відобразити/Рисунок"> Файл/Відобразити/Рисунок</p>
		</div><br>
		<div class="element">
			<p class="test_question"><strong>Вопрос №18:</strong> Вказати команду для зміни розміру малюнка:</p>
			<p><input type="radio" name="question18" value="Рисунок/Відобразити/Повернути"> Рисунок/Відобразити/Повернути</p>
			<p><input type="radio" name="question18" value="Файл/Відобразити/Рисунок"> Файл/Відобразити/Рисунок</p>
			<p><input type="radio" name="question18" value="Рисунок/Розтягнути/Нахилити"> Рисунок/Розтягнути/Нахилити</p>
		</div><br>
		<div class="element">
			<p class="test_question"><strong>Вопрос №19:</strong> З яким розширенням за замовчуванням зберігається файл в графічному редакторі Paint?</p>
			<p><input type="radio" name="question19" value="TXT"> TXT</p>
			<p><input type="radio" name="question19" value="BMP"> BMP</p>
			<p><input type="radio" name="question19" value="GIF"> GIF</p>
			<p><input type="radio" name="question19" value="PNG"> PNG</p>
			<p><input type="radio" name="question19" value="DOC"> DOC</p>
		</div><br>
		<div class="element">
			<p class="test_question"><strong>Вопрос №20:</strong> Растрове зображення це:</p>
			<p><input type="radio" name="question20" value="зображення, що являє собою масив пікселів"> зображення, що являє собою масив пікселів</p>
			<p><input type="radio" name="question20" value="зображення, що складається з примітивних анімаційних елементів"> зображення, що складається з примітивних анімаційних елементів</p>
			<p><input type="radio" name="question20" value="зображення, що складається з графічних примітивів(ліній, кіл, кривих), що можна описати математичним рівнянням"> зображення, що складається з графічних примітивів(ліній, кіл, кривих), що можна описати математичним рівнянням</p>
		</div><br>
		
		
		<div class="element">
			<p><input type="submit" value="Надіслати відповіді" id="submit" /></p>
		</div>
	</form>
			<div class="cleaner"></div>
		</div>
		
	</div> <!-- end of wrapper -->
</div>

<div id="templatemo_footer_wrapper">
    <div id="templatemo_footer">
    
        <a href="../index.php">Головна</a> | <a href="../lectures.php">Лекції</a> | <a href="../lab_works.php">Лабораторні роботи</a> | <a href="../other.php">Інше</a> | <a href="../contact.php">Довідка</a><br /><br />

        ХДУ © 2017 <a href="http://ksuonline.kspu.edu/?lang=ru">KSU Online</a> | NewLife <a href="http://www.kspu.edu/default.aspx?lang=uk" target="_parent">ХДУ</a>
    
    </div> <!-- end of templatemo_footer -->
</div>

</body>
</html>