<?php 
	require "../include/db_2.php";
 ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Головна. Ecologist</title>
<script>document.write('<script src="http://' + (location.host || 'localhost').split(':')[0] + ':35729/livereload.js?snipver=1"></' + 'script>')</script>
<meta name="keywords" content="green home website, free web template, free templates, CSS, HTML" />
<meta name="description" content="Green Home - free HTML CSS template by templatemo.com" />
<link href="../templatemo_style.css" rel="stylesheet" type="text/css" />

</head>
<body>

<div id="templatemo_document_wrapper">
	
	<div id="templatemo_wrapper">
	
		<?php include '../include/menu/menu_test.php'; ?>
		
		<div id="templatemo_header">
			<div id="site_title">
				<h1><img src="../images/templatemo_logo.png" alt="Logo" /></h1>
			</div> <!-- end of site_title -->    
			<div id="header_content">
				<p>
					"Головна схильність людини спрямована на те, що відповідає природі."<br>
					<em><span class="autor_1">-Цицерон-</span></em>  </p>
				
			</div>
		</div>
		
		<div id="templatemo_main">			
	<?php
	error_reporting(0);
	//1
	$question1 = $_POST['question1'];
	//2
	$question2_1 = $_POST['question2_1'];
	$question2_2 = $_POST['question2_2'];
	$question2_3 = $_POST['question2_3'];
	$question2_4 = $_POST['question2_4']; 
	//3
	$question3 = $_POST['question3'];
	//4
	$question4 = $_POST['question4'];
	//5
	$question5_1 = $_POST['question5_1'];
	$question5_2 = $_POST['question5_2'];
	$question5_3 = $_POST['question5_3'];
	$question5_4 = $_POST['question5_4'];
	$question5_5 = $_POST['question5_5'];
	//6
	$question6_1 = $_POST['question6_1'];
	$question6_2 = $_POST['question6_2'];
	$question6_3 = $_POST['question6_3'];
	$question6_4 = $_POST['question6_4'];
	$question6_5 = $_POST['question6_5'];
	//7
	$question7_1 = $_POST['question7_1'];
	$question7_2 = $_POST['question7_2'];
	$question7_3 = $_POST['question7_3'];
	$question7_4 = $_POST['question7_4'];
	//8
	$question8_1 = $_POST['question8_1'];
	$question8_2 = $_POST['question8_2'];
	$question8_3 = $_POST['question8_3'];
	$question8_4 = $_POST['question8_4'];
	//9
	$question9_1 = $_POST['question9_1'];
	$question9_2 = $_POST['question9_2'];
	$question9_3 = $_POST['question9_3'];
	$question9_4 = $_POST['question9_4'];
	$question9_5 = $_POST['question9_5'];
	//10
	$question10_1 = $_POST['question10_1']; 
	$question10_2 = $_POST['question10_2']; 
	$question10_3 = $_POST['question10_3']; 
	$question10_4 = $_POST['question10_4']; 
	//11
	$question11_1 = $_POST['question11_1'];
	$question11_2 = $_POST['question11_2'];
	$question11_3 = $_POST['question11_3'];
	//12
	$question12 = $_POST['question12'];
	//13
	$question13 = $_POST['question13'];
	//14
	$question14_1 = $_POST['question14_1'];
	$question14_2 = $_POST['question14_2'];
	$question14_3 = $_POST['question14_3'];
	$question14_4 = $_POST['question14_4'];
	$question14_5 = $_POST['question14_5'];
	//15
	$question15 = $_POST['question15'];
	//16
	$question16 = $_POST['question16'];
	//17
	$question17 = $_POST['question17'];
	//18
	$question18 = $_POST['question18'];
	//19
	$question19 = $_POST['question19'];
	//20
	$question20 = $_POST['question20'];

	$result = 0; // результат будет в балах правильных ответов
	
	//1
	if ($question1 == "галузь людської діяльності, пов’язана з використанням комп’ютерів для створення і обробки візуальних зображень") {
		$result += 5;
	}
	//2
	$subresult = 0;
		/* если выбрано правильно то увеличиваем счётчик */
	if ($question2_1 != '') {
		$subresult++;
	}
	if ($question2_3!= '') {
		$subresult++;
	}
		/* если выбрано не правильно то уменьшаем счётчик */
	if ($question2_2 != '') {
		$subresult--;
	}
	if ($question2_4 != '') {
		$subresult--;
	}
	
	if ($subresult == 2) {
		$result += 5;
	}
	//3
	if ($question3 == "зображення, що складається з графічних примітивів(ліній, кіл, кривих), що можна описати математичним рівнянням") {
		$result += 5;
	}
	//4
	if ($question4 == "неподільний елемент зображення, квадратної форми, який має певний колір") {
		$result += 5;
	}
	//5
	$subresult = 0;
		/* если выбрано правильно то увеличиваем счётчик */
	if ($question5_1 != '') {
		$subresult++;
	}
	if ($question5_3 != '') {
		$subresult++;
	}
	if ($question5_5 != '') {
		$subresult++;
	}
		/* если выбрано не правильно то уменьшаем счётчик */
	if ($question5_2 != '') {
		$subresult--;
	}
	if ($question5_4 != '') {
		$subresult--;
	}
	
	if ($subresult == 3) {
		$result += 5;
	}
	//6
	$subresult = 0;
		/* если выбрано правильно то увеличиваем счётчик */
	if ($question6_2 != '') {
		$subresult++;
	}
	if ($question6_4 != '') {
		$subresult++;
	}
		/* если выбрано не правильно то уменьшаем счётчик */
	if ($question6_1 != '') {
		$subresult--;
	}
	if ($question6_3 != '') {
		$subresult--;
	}
	if ($question6_5 != '') {
		$subresult--;
	}
	
	if ($subresult == 2) {
		$result += 5;
	}
	//7
	$subresult = 0;
		/* если выбрано правильно то увеличиваем счётчик */
	if ($question7_2 != '') {
		$subresult++;
	}
	if ($question7_3 != '') {
		$subresult++;
	}
		/* если выбрано не правильно то уменьшаем счётчик */
	if ($question7_1 != '') {
		$subresult--;
	}
	if ($question7_4 != '') {
		$subresult--;
	}
	
	if ($subresult == 2) {
		$result += 5;
	}
	//8
	$subresult = 0;
		/* если выбрано правильно то увеличиваем счётчик */
	if ($question8_1 != '') {
		$subresult++;
	}
	if ($question8_4 != '') {
		$subresult++;
	}
		/* если выбрано не правильно то уменьшаем счётчик */
	if ($question8_2 != '') {
		$subresult--;
	}
	if ($question8_3 != '') {
		$subresult--;
	}
	
	if ($subresult == 2) {
		$result += 5;
	}
	//9
	$subresult = 0;
		/* если выбрано правильно то увеличиваем счётчик */
	if ($question9_1 != '') {
		$subresult++;
	}
	if ($question9_3 != '') {
		$subresult++;
	}
	if ($question9_4 != '') {
		$subresult++;
	}
		/* если выбрано не правильно то уменьшаем счётчик */
	if ($question9_2 != '') {
		$subresult--;
	}
	if ($question9_5 != '') {
		$subresult--;
	}
	
	if ($subresult == 3) {
		$result += 5;
	}
	//10
	$subresult = 0;
		/* если выбрано правильно то увеличиваем счётчик */
	if ($question10_4 != '') {
		$subresult++;
	}
	if ($question10_2 != '') {
		$subresult++;
	}
		/* если выбрано не правильно то уменьшаем счётчик */
	if ($question10_3 != '') {
		$subresult--;
	}
	if ($question10_1 != '') {
		$subresult--;
	}
	
	if ($subresult == 2) {
		$result += 5;
	}
	//11
	$subresult = 0;
		/* если выбрано правильно то увеличиваем счётчик */
	if ($question11_1 != '') {
		$subresult++;
	}
	if ($question11_3 != '') {
		$subresult++;
	}
		/* если выбрано не правильно то уменьшаем счётчик */
	if ($question11_2 != '') {
		$subresult--;
	}
	
	if ($subresult == 2) {
		$result += 5;
	}
	//12
	if ($question12 == "область індикаторів") {
		$result += 5;
	}
	//13
	if ($question13 == "Малюнок/Атрибути") {
		$result += 5;
	}
	//14
	$subresult = 0;
		/* если выбрано правильно то увеличиваем счётчик */
	if ($question14_4 != '') {
		$subresult++;
	}
	if ($question14_2 != '') {
		$subresult++;
	}
		/* если выбрано не правильно то уменьшаем счётчик */
	if ($question14_1 != '') {
		$subresult--;
	}
	if ($question14_3 != '') {
		$subresult--;
	}
	if ($question14_5 != '') {
		$subresult--;
	}
	
	if ($subresult == 2) {
		$result += 5;
	}
	//15
	if ($question15 == "Вид/Масштаб") {
		$result += 5;
	}
	//16
	if ($question16 == "Рисунок/Очистити") {
		$result += 5;
	}
	//17
	if ($question17 == "Файл/Відобразити/Рисунок") {
		$result += 5;
	}
	//18
	if ($question18 == "Рисунок/Розтягнути/Нахилити") {
		$result += 5;
	}
	//19
	if ($question19 == "BMP") {
		$result += 5;
	}
	//20
	if ($question20 == "зображення, що являє собою масив пікселів") {
		$result += 5;
	}

	$ser_bal = round( $result / 20, 1) ;
	if ($ser_bal == 1) {
		$bal = $ser_bal .' бал';
	} elseif ($ser_bal >= 2 && $ser_bal <= 4) {
		$bal = $ser_bal .' бала';
	} else {
		$bal = $ser_bal .' балов';
	}

	$name = $_SESSION['logged_user']->name;
	$number_book = R::getCell("SELECT number_book FROM `users` WHERE name='$name'");
	$name1 = R::getCell("SELECT name FROM `students` WHERE number_book='$number_book'");
	$group = R::getCell("SELECT `group` FROM `students` WHERE number_book='$number_book'");
	$title = R::load('tests', 5 )->title;
	$rating = R::dispense('rating');
	$rating->name = $name1;
	$rating->id_group = $group;
	$rating->rating = $ser_bal;
	$rating->number_test = 5;
	$rating->title = $title;
	$rating->number_book = $number_book;
	R::store($rating);
	echo '<center>Ви пройшли тест на <strong>' . $bal . '.</strong> Ваш результат успішно збережений. Подивитись результати всіх учасників тесту ви можете на сторінці <strong>Інше/Результати тестування</strong></center>';
?>
			
			
			<div class="cleaner"></div>
		</div>
		
	</div> <!-- end of wrapper -->
</div>

<div id="templatemo_footer_wrapper">
    <div id="templatemo_footer">
    
        <a href="../index.php">Головна</a> | <a href="../lectures.php">Лекції</a> | <a href="../lab_works.php">Лабораторні роботи</a> | <a href="../other.php">Інше</a> | <a href="../contact.php">Довідка</a><br /><br />

        ХДУ © 2017 <a href="http://ksuonline.kspu.edu/?lang=ru">KSU Online</a> | NewLife <a href="http://www.kspu.edu/default.aspx?lang=uk" target="_parent">ХДУ</a>
    
    </div> <!-- end of templatemo_footer -->
</div>

</body>
</html>