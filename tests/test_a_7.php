<?php 
	require "../include/db_2.php";
 ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Головна. Ecologist</title>
<script>document.write('<script src="http://' + (location.host || 'localhost').split(':')[0] + ':35729/livereload.js?snipver=1"></' + 'script>')</script>
<meta name="keywords" content="green home website, free web template, free templates, CSS, HTML" />
<meta name="description" content="Green Home - free HTML CSS template by templatemo.com" />
<link href="../templatemo_style.css" rel="stylesheet" type="text/css" />

</head>
<body>

<div id="templatemo_document_wrapper">
	
	<div id="templatemo_wrapper">
	
		<?php include '../include/menu/menu_test.php'; ?>
		
		<div id="templatemo_header">
			<div id="site_title">
				<h1><img src="../images/templatemo_logo.png" alt="Logo" /></h1>
			</div> <!-- end of site_title -->    
			<div id="header_content">
				<p>
					"Головна схильність людини спрямована на те, що відповідає природі."<br>
					<em><span class="autor_1">-Цицерон-</span></em>  </p>
				
			</div>
		</div>
		
		<div id="templatemo_main">			
	<?php
	error_reporting(0);
	//1
	$question1 = $_POST['question1'];
	//2
	$question2_1 = $_POST['question2_1'];
	$question2_2 = $_POST['question2_2'];
	$question2_3 = $_POST['question2_3'];
	$question2_4 = $_POST['question2_4'];  
	//3
	$question3 = $_POST['question3'];
	//4
	$question4_1 = $_POST['question4_1'];
	$question4_2 = $_POST['question4_2'];
	$question4_3 = $_POST['question4_3'];
	$question4_4 = $_POST['question4_4'];
	$question4_5 = $_POST['question4_5'];
	$question4_6 = $_POST['question4_6'];
	//5
	$question5_1 = $_POST['question5_1'];
	$question5_2 = $_POST['question5_2'];
	$question5_3 = $_POST['question5_3'];
	$question5_4 = $_POST['question5_4'];
	//6
	$question6 = $_POST['question6'];
	//7
	$question7 = $_POST['question7'];
	//8
	$question8 = $_POST['question8'];
	//9
	$question9 = $_POST['question9'];
	//10
	$question10 = $_POST['question10'];  
	//11
	$question11 = $_POST['question11'];
	//12
	$question12 = $_POST['question12'];
	//13
	$question13 = $_POST['question13'];
	//14
	$question14 = $_POST['question14'];
	//15
	$question15 = $_POST['question15'];
	//16
	$question16_1 = $_POST['question16_1'];
	$question16_2 = $_POST['question16_2'];
	$question16_3 = $_POST['question16_3'];
	$question16_4 = $_POST['question16_4'];
	//17
	$question17 = $_POST['question17'];
	//18
	$question18_1 = $_POST['question18_1'];
	$question18_2 = $_POST['question18_2'];
	$question18_3 = $_POST['question18_3'];
	$question18_4 = $_POST['question18_4'];
	//19
	$question19 = $_POST['question19'];
	//20
	$question20 = $_POST['question20'];
	//21
	$question21_1 = $_POST['question21_1'];
	$question21_2 = $_POST['question21_2'];
	$question21_3 = $_POST['question21_3'];
	$question21_4 = $_POST['question21_4'];
	$question21_5 = $_POST['question21_5'];
	$question21_6 = $_POST['question21_6'];
	//22
	$question22_1 = $_POST['question22_1'];
	$question22_2 = $_POST['question22_2'];
	$question22_3 = $_POST['question22_3'];
	$question22_4 = $_POST['question22_4'];

	$result = 0; // результат будет в балах правильных ответов
	
	//1
	if ($question1 == "програмою для створення та обробки електронних таблиць") {
		$result += 5;
	}
	//2
	$subresult = 0;
		/* если выбрано правильно то увеличиваем счётчик */
	if ($question2_1 != '') {
		$subresult++;
	}
	if ($question2_3 != '') {
		$subresult++;
	}
		/* если выбрано не правильно то уменьшаем счётчик */
	if ($question2_2 != '') {
		$subresult--;
	}
	if ($question2_4 != '') {
		$subresult--;
	}
	
	if ($subresult == 2) {
		$result += 5;
	}
	//3
	if ($question3 == "MS Excel") {
		$result += 5;
	}
	//4
	$subresult = 0;
		/* если выбрано правильно то увеличиваем счётчик */
	if ($question4_3 != '') {
		$subresult++;
	}
	if ($question4_5 != '') {
		$subresult++;
	}
		/* если выбрано не правильно то уменьшаем счётчик */
	if ($question4_1 != '') {
		$subresult--;
	}
	if ($question4_2 != '') {
		$subresult--;
	}
	if ($question4_4 != '') {
		$subresult--;
	}
	if ($question4_6 != '') {
		$subresult--;
	}
	
	if ($subresult == 2) {
		$result += 5;
	}
	//5
	$subresult = 0;
		/* если выбрано правильно то увеличиваем счётчик */
	if ($question5_1 != '') {
		$subresult++;
	}
	if ($question5_2 != '') {
		$subresult++;
	}
	if ($question5_3 != '') {
		$subresult++;
	}
		/* если выбрано не правильно то уменьшаем счётчик */
	if ($question5_4 != '') {
		$subresult--;
	}
	
	if ($subresult == 3) {
		$result += 5;
	}
	//6
	if ($question6 == "робоча книга") {
		$result += 5;
	}
	//7
	if ($question7 == "xls") {
		$result += 5;
	}
	//8
	if ($question8 == "великими латинськими літерами") {
		$result += 5;
	}
	//9
	if ($question9 == "цифрами") {
		$result += 5;
	}
	//10
	if ($question10 == 'натиснути на клавішу F2') {
		$result += 5;
	}
	//11
	if ($question11 == "клацнути лівою кнопкою миші") {
		$result += 5;
	}
	//12
	if ($question12 == "відсотковий") {
		$result += 5;
	}
	//13
	if ($question13 == "команда меню Формат/Формат клітинок") {
		$result += 5;
	}
	//14
	if ($question14 == "виконати команду Формат клітинок/Вирівнювання") {
		$result += 5;
	}
	//15
	if ($question15 == "грошовий") {
		$result += 5;
	}
	//16
	$subresult = 0;
		/* если выбрано правильно то увеличиваем счётчик */
	if ($question16_1 != '') {
		$subresult++;
	}
	if ($question16_4 != '') {
		$subresult++;
	}
		/* если выбрано не правильно то уменьшаем счётчик */
	if ($question16_3 != '') {
		$subresult--;
	}
	if ($question16_2 != '') {
		$subresult--;
	}
	
	if ($subresult == 2) {
		$result += 5;
	}
	//17
	if ($question17 == "СУММА") {
		$result += 5;
	}
	//18
	$subresult = 0;
		/* если выбрано правильно то увеличиваем счётчик */
	if ($question18_1 != '') {
		$subresult++;
	}
	if ($question18_3 != '') {
		$subresult++;
	}
		/* если выбрано не правильно то уменьшаем счётчик */
	if ($question18_2 != '') {
		$subresult--;
	}
	if ($question18_4 != '') {
		$subresult--;
	}
	
	if ($subresult == 2) {
		$result += 5;
	}
	//19
	if ($question19 == "МАХ") {
		$result += 5;
	}
	//20
	if ($question20 == "команда меню Вставка/Рисунок/Діаграма") {
		$result += 5;
	}
	//21
	$subresult = 0;
		/* если выбрано правильно то увеличиваем счётчик */
	if ($question21_1 != '') {
		$subresult++;
	}
	if ($question21_5 != '') {
		$subresult++;
	}
		/* если выбрано не правильно то уменьшаем счётчик */
	if ($question21_2 != '') {
		$subresult--;
	}
	if ($question21_3 != '') {
		$subresult--;
	}
	if ($question21_4 != '') {
		$subresult--;
	}
	if ($question21_6 != '') {
		$subresult--;
	}
	
	if ($subresult == 2) {
		$result += 5;
	}
	//22
	$subresult = 0;
		/* если выбрано правильно то увеличиваем счётчик */
	if ($question22_1 != '') {
		$subresult++;
	}
	if ($question22_2 != '') {
		$subresult++;
	}
		/* если выбрано не правильно то уменьшаем счётчик */
	if ($question22_3 != '') {
		$subresult--;
	}
	if ($question22_4 != '') {
		$subresult--;
	}
	
	if ($subresult == 2) {
		$result += 5;
	}

	$ser_bal = round( $result / 22, 1) ;
	if ($ser_bal == 1) {
		$bal = $ser_bal .' бал';
	} elseif ($ser_bal >= 2 && $ser_bal <= 4) {
		$bal = $ser_bal .' бала';
	} else {
		$bal = $ser_bal .' балов';
	}

	$name = $_SESSION['logged_user']->name;
	$number_book = R::getCell("SELECT number_book FROM `users` WHERE name='$name'");
	$name1 = R::getCell("SELECT name FROM `students` WHERE number_book='$number_book'");
	$group = R::getCell("SELECT `group` FROM `students` WHERE number_book='$number_book'");
	$title = R::load('tests', 7 )->title;
	$rating = R::dispense('rating');
	$rating->name = $name1;
	$rating->id_group = $group;
	$rating->rating = $ser_bal;
	$rating->number_test = 7;
	$rating->title = $title;
	$rating->number_book = $number_book;
	R::store($rating);
	echo '<center>Ви пройшли тест на <strong>' . $bal . '.</strong> Ваш результат успішно збережений. Подивитись результати всіх учасників тесту ви можете на сторінці <strong>Інше/Результати тестування</strong></center>';
?>
			
			
			<div class="cleaner"></div>
		</div>
		
	</div> <!-- end of wrapper -->
</div>

<div id="templatemo_footer_wrapper">
    <div id="templatemo_footer">
    
        <a href="../index.php">Головна</a> | <a href="../lectures.php">Лекції</a> | <a href="../lab_works.php">Лабораторні роботи</a> | <a href="../other.php">Інше</a> | <a href="../contact.php">Довідка</a><br /><br />

        ХДУ © 2017 <a href="http://ksuonline.kspu.edu/?lang=ru">KSU Online</a> | NewLife <a href="http://www.kspu.edu/default.aspx?lang=uk" target="_parent">ХДУ</a>
    
    </div> <!-- end of templatemo_footer -->
</div>

</body>
</html>