<?php 
	require "../include/db_2.php";
 ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Головна. Ecologist</title>
<script>document.write('<script src="http://' + (location.host || 'localhost').split(':')[0] + ':35729/livereload.js?snipver=1"></' + 'script>')</script>
<meta name="keywords" content="green home website, free web template, free templates, CSS, HTML" />
<meta name="description" content="Green Home - free HTML CSS template by templatemo.com" />
<link href="../templatemo_style.css" rel="stylesheet" type="text/css" />

</head>
<body>

<div id="templatemo_document_wrapper">
	
	<div id="templatemo_wrapper">
	
		<div id="templatemo_menu">
			<ul>
				<li><a href="../index.php">Головна</a></li>
				<li><a href="../lectures.php" >Лекції</a></li>
				<li><a href="../lab_works.php">Лабораторні роботи</a></li>
				<li><a href="../other.php">Інше</a></li>
				<li><a href="../test.php" class="current">Тести</a></li>
				<li><a href="../contact.php">Довідка</a></li>
			</ul>
			    	
		</div> <!-- end of templatemo_menu -->
		
		<div id="templatemo_header">
			<div id="site_title">
				<h1><img src="../images/templatemo_logo.png" alt="Logo" /></h1>
			</div> <!-- end of site_title -->    
			<div id="header_content">
				<p>
					"Головна схильність людини спрямована на те, що відповідає природі."<br>
					<em><span class="autor_1">-Цицерон-</span></em>  </p>
				
			</div>
		</div>
		
		<div id="templatemo_main">			
	<?php
	error_reporting(0);
	//1
	$question1 = $_POST['question1'];
	//2
	$question2 = $_POST['question2'];
	//3
	$question3_1 = $_POST['question3_1'];
	$question3_2 = $_POST['question3_2'];
	$question3_3 = $_POST['question3_3'];
	$question3_4 = $_POST['question3_4'];
	$question3_5 = $_POST['question3_5'];
	//4
	$question4_1 = $_POST['question4_1'];
	$question4_2 = $_POST['question4_2'];
	$question4_3 = $_POST['question4_3'];
	$question4_4 = $_POST['question4_4'];
	$question4_5 = $_POST['question4_5'];
	//5
	$question5_1 = $_POST['question5_1'];
	$question5_2 = $_POST['question5_2'];
	$question5_3 = $_POST['question5_3'];
	$question5_4 = $_POST['question5_4'];
	$question5_5 = $_POST['question5_5'];
	//6
	$question6_1 = $_POST['question6_1'];
	$question6_2 = $_POST['question6_2'];
	$question6_3 = $_POST['question6_3'];
	$question6_4 = $_POST['question6_4'];
	$question6_5 = $_POST['question6_5'];
	$question6_6 = $_POST['question6_6'];
	//7
	$question7_1 = $_POST['question7_1'];
	$question7_2 = $_POST['question7_2'];
	$question7_3 = $_POST['question7_3'];
	$question7_4 = $_POST['question7_4'];
	$question7_5 = $_POST['question7_5'];
	$question7_6 = $_POST['question7_6'];
	//8
	$question8_1 = $_POST['question8_1'];
	$question8_2 = $_POST['question8_2'];
	$question8_3 = $_POST['question8_3'];
	$question8_4 = $_POST['question8_4'];
	//9
	$question9 = $_POST['question9'];
	//10
	$question10 = $_POST['question10'];  
	//11
	$question11_1 = $_POST['question11_1'];
	$question11_2 = $_POST['question11_2'];
	$question11_3 = $_POST['question11_3'];
	$question11_4 = $_POST['question11_4'];
	//12
	$question12_1 = $_POST['question12_1'];
	$question12_2 = $_POST['question12_2'];
	$question12_3 = $_POST['question12_3'];
	$question12_4 = $_POST['question12_4'];
	//13
	$question13 = $_POST['question13'];
	//14
	$question14 = $_POST['question14'];
	//15
	$question15 = $_POST['question15'];
	//16
	$question16 = $_POST['question16'];
	//17
	$question17 = $_POST['question17'];
	//18
	$question18_1 = $_POST['question18_1'];
	$question18_2 = $_POST['question18_2'];
	$question18_3 = $_POST['question18_3'];
	$question18_4 = $_POST['question18_4'];
	$question18_5 = $_POST['question18_5'];
	$question18_6 = $_POST['question18_6'];
	//19
	$question19 = $_POST['question19'];
	//20
	$question20 = $_POST['question20'];
	

	$result = 0; // результат будет в балах правильных ответов
	
	//1
	if ($question1 == "програма, що забезпечує створення, редагування і збереження форматованого тексту у файлах") {
		$result += 5;
	}
	//2
	if ($question2 == "MS Word") {
		$result += 5;
	}
	//3
	$subresult = 0;
		/* если выбрано правильно то увеличиваем счётчик */
	if ($question3_1 != '') {
		$subresult++;
	}
	if ($question3_3 != '') {
		$subresult++;
	}
	if ($question3_4 != '') {
		$subresult++;
	}
		/* если выбрано не правильно то уменьшаем счётчик */
	if ($question3_2 != '') {
		$subresult--;
	}
	if ($question3_5 != '') {
		$subresult--;
	}
	
	if ($subresult == 3) {
		$result += 5;
	}
	//4
	$subresult = 0;
		/* если выбрано правильно то увеличиваем счётчик */
	if ($question4_1 != '') {
		$subresult++;
	}
	if ($question4_3 != '') {
		$subresult++;
	}
		/* если выбрано не правильно то уменьшаем счётчик */
	if ($question4_2 != '') {
		$subresult--;
	}
	if ($question4_4 != '') {
		$subresult--;
	}
	if ($question4_5 != '') {
		$subresult--;
	}
	
	if ($subresult == 2) {
		$result += 5;
	}
	//5
	$subresult = 0;
		/* если выбрано правильно то увеличиваем счётчик */
	if ($question5_4 != '') {
		$subresult++;
	}
	if ($question5_5 != '') {
		$subresult++;
	}
		/* если выбрано не правильно то уменьшаем счётчик */
	if ($question5_1 != '') {
		$subresult--;
	}
	if ($question5_2 != '') {
		$subresult--;
	}
	if ($question5_3 != '') {
		$subresult--;
	}
	
	if ($subresult == 2) {
		$result += 5;
	}
	//6
	$subresult = 0;
		/* если выбрано правильно то увеличиваем счётчик */
	if ($question6_1 != '') {
		$subresult++;
	}
	if ($question6_3 != '') {
		$subresult++;
	}
	if ($question6_6 != '') {
		$subresult++;
	}
		/* если выбрано не правильно то уменьшаем счётчик */
	if ($question6_2 != '') {
		$subresult--;
	}
	if ($question6_4 != '') {
		$subresult--;
	}
	if ($question6_5 != '') {
		$subresult--;
	}
	
	if ($subresult == 3) {
		$result += 5;
	}
	//7
	$subresult = 0;
		/* если выбрано правильно то увеличиваем счётчик */
	if ($question7_1 != '') {
		$subresult++;
	}
	if ($question7_3 != '') {
		$subresult++;
	}
	if ($question7_4 != '') {
		$subresult++;
	}
	if ($question7_6 != '') {
		$subresult++;
	}
		/* если выбрано не правильно то уменьшаем счётчик */
	if ($question7_2 != '') {
		$subresult--;
	}
	if ($question7_5 != '') {
		$subresult--;
	}
	
	if ($subresult == 4) {
		$result += 5;
	}
	//8
	$subresult = 0;
		/* если выбрано правильно то увеличиваем счётчик */
	if ($question8_1 != '') {
		$subresult++;
	}
	if ($question8_3 != '') {
		$subresult++;
	}
		/* если выбрано не правильно то уменьшаем счётчик */
	if ($question8_2 != '') {
		$subresult--;
	}
	if ($question8_4 != '') {
		$subresult--;
	}
	
	if ($subresult == 2) {
		$result += 5;
	}
	//9
	if ($question9 == "Вставка/Символ") {
		$result += 5;
	}
	//10
	if ($question10 == 'Backspace') {
		$result += 5;
	}
	
	//11
	$subresult = 0;
		/* если выбрано правильно то увеличиваем счётчик */
	if ($question11_3 != '') {
		$subresult++;
	}
	if ($question11_3 != '') {
		$subresult++;
	}
		/* если выбрано не правильно то уменьшаем счётчик */
	if ($question11_1 != '') {
		$subresult--;
	}
	if ($question11_2 != '') {
		$subresult--;
	}
	
	if ($subresult == 2) {
		$result += 5;
	}
	//12
	$subresult = 0;
		/* если выбрано правильно то увеличиваем счётчик */
	if ($question12_1 != '') {
		$subresult++;
	}
	if ($question12_3 != '') {
		$subresult++;
	}
		/* если выбрано не правильно то уменьшаем счётчик */
	if ($question12_2 != '') {
		$subresult--;
	}
	if ($question12_4 != '') {
		$subresult--;
	}
	
	if ($subresult == 2) {
		$result += 5;
	}
	//13
	if ($question13 == "зміна зовнішнього вигляду тексту") {
		$result += 5;
	}
	//14
	if ($question14 == "Файл/Параметри сторінки") {
		$result += 5;
	}
	//15
	if ($question15 == "Формат/Шрифт") {
		$result += 5;
	}
	//16
	if ($question16 == "Формат/Абзац") {
		$result += 5;
	}
	//17
	if ($question17 == "Правка/Знайти") {
		$result += 5;
	}
	//18
	$subresult = 0;
		/* если выбрано правильно то увеличиваем счётчик */
	if ($question18_1 != '') {
		$subresult++;
	}
	if ($question18_3 != '') {
		$subresult++;
	}
	if ($question18_5 != '') {
		$subresult++;
	}
		/* если выбрано не правильно то уменьшаем счётчик */
	if ($question18_2 != '') {
		$subresult--;
	}
	if ($question18_4 != '') {
		$subresult--;
	}
	if ($question18_6 != '') {
		$subresult--;
	}
	
	if ($subresult == 3) {
		$result += 5;
	}
	//19
	if ($question19 == "команда Вставка/Обєкт/Microsoft Equation 3.0") {
		$result += 5;
	}
	//20
	if ($question20 == "команда Вставка/Малюнок/Із файлу") {
		$result += 5;
	}

	$ser_bal = round( $result / 20, 1) ;
	if ($ser_bal == 1) {
		$bal = $ser_bal .' бал';
	} elseif ($ser_bal >= 2 && $ser_bal <= 4) {
		$bal = $ser_bal .' бала';
	} else {
		$bal = $ser_bal .' балов';
	}

	$name = $_SESSION['logged_user']->name;
	$number_book = R::getCell("SELECT number_book FROM `users` WHERE name='$name'");
	$name1 = R::getCell("SELECT name FROM `students` WHERE number_book='$number_book'");
	$group = R::getCell("SELECT `group` FROM `students` WHERE number_book='$number_book'");
	$title = R::load('tests', 6 )->title;
	$rating = R::dispense('rating');
	$rating->name = $name1;
	$rating->id_group = $group;
	$rating->rating = $ser_bal;
	$rating->number_test = 6;
	$rating->title = $title;
	$rating->number_book = $number_book;
	R::store($rating);
	echo '<center>Ви пройшли тест на <strong>' . $bal . '.</strong> Ваш результат успішно збережений. Подивитись результати всіх учасників тесту ви можете на сторінці <strong>Інше/Результати тестування</strong></center>';
?>
			
			
			<div class="cleaner"></div>
		</div>
		
	</div> <!-- end of wrapper -->
</div>

<div id="templatemo_footer_wrapper">
    <div id="templatemo_footer">
    
        <a href="../index.php">Головна</a> | <a href="../lectures.php">Лекції</a> | <a href="../lab_works.php">Лабораторні роботи</a> | <a href="../other.php">Інше</a> | <a href="../contact.php">Довідка</a><br /><br />

        ХДУ © 2017 <a href="http://ksuonline.kspu.edu/?lang=ru">KSU Online</a> | NewLife <a href="http://www.kspu.edu/default.aspx?lang=uk" target="_parent">ХДУ</a>
    
    </div> <!-- end of templatemo_footer -->
</div>

</body>
</html>