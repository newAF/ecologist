<?php 
	require "../include/db_2.php";
	$status1 = R::load('tests', 3);
 	$lvl_a = $_SESSION['logged_user']->lvl_a;
 	if ($status1->status == 'Закритий' && $lvl_a <= 1) {
 	  	header ('location: /errors/error_test.php');
 	}
 	if (empty($_SESSION['logged_user'])) {
 	  	header ('location: /errors/error_test.php');
 	}
 ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Головна. Ecologist</title>
<script>document.write('<script src="http://' + (location.host || 'localhost').split(':')[0] + ':35729/livereload.js?snipver=1"></' + 'script>')</script>
<meta name="keywords" content="green home website, free web template, free templates, CSS, HTML" />
<meta name="description" content="Green Home - free HTML CSS template by templatemo.com" />
<link href="../templatemo_style.css" rel="stylesheet" type="text/css" />
<link href="style_test.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../js/timer.js"></script>
</head>
<body onload="startTimer()">

<div id="templatemo_document_wrapper">
	
	<div id="templatemo_wrapper">
		<?php include '../include/menu/menu_test.php'; ?>
		<div id="templatemo_header">
			<div id="site_title">
				<h1><img src="../images/templatemo_logo.png" alt="Logo" /></h1>
			</div> <!-- end of site_title -->    
			<div id="header_content">
				<p>
					"Головна схильність людини спрямована на те, що відповідає природі."<br>
					<em><span class="autor_1">-Цицерон-</span></em>  </p>
			</div>
		</div>
		
		<div id="templatemo_main">
			
			<center><span class="zagolovok">Тест 3</span></center><br>
			<span class="nazvanie">
                   <strong> Програмне забезпечення персонального комп'ютера.</strong> <hr>
                </span>
                <span class="info">Увага! Питання з зірочкою мають декілька правильних відповідей. </span><hr><br>
                <p>
					<span style="color: green; font-size: 150%; font-weight: bold;  position: relative; float: center;">
						Ви розпочали тест! Залишилось часу-- 
					</span>
					<span id="my_timer" style="color: #f00; font-size: 150%; font-weight: bold; float: center;">
						00:12:45
					</span><hr>
				</p>

			<div class="pd-embed" id="pd_1486199544"></div>
	<form method="post" action="test_a_3.php">
		<div class="element">
			<p class="test_question"><strong>Вопрос №1:</strong> Назвіть вид програмного забезпечення, що забезпечує взаємодію інших пристроїв ПК з програмами базового рівня та апаратним забезпеченням:</p>
			<p><input type="radio" name="question1" value="системне програмне забезпечення"> системне програмне забезпечення</p>
			<p><input type="radio" name="question1" value="прикладне програмне забезпечення"> прикладне програмне забезпечення</p>
			<p><input type="radio" name="question1" value="службове програмне забезпечення"> службове програмне забезпечення</p>
		</div><br>
		<div class="element">
			<p class="test_question"><strong>Вопрос №2:<span class="zir">*</sspan></strong> З наведених прикладів програмного забезпечення вкажіть ті, що входять до системного програмне забезпечення:</p>
			<p><input type="checkbox" name="question2_1" value="операційна система"> операційна система</p>
			<p><input type="checkbox" name="question2_2" value="драйвери"> драйвери</p>
			<p><input type="checkbox" name="question2_3" value="браузери"> браузери</p>
			<p><input type="checkbox" name="question2_4" value="програми для обміну миттєвими повідомленнями"> програми для обміну миттєвими повідомленнями</p>
			<p><input type="checkbox" name="question2_5" value="утиліти"> утиліти</p>
			<p><input type="checkbox" name="question2_6" value="графічні редактори"> графічні редактори</p>
			<p><input type="checkbox" name="question2_7" value="середовища програмування"> середовища програмування</p>
		</div><br>
		<div class="element">
			<p class="test_question"><strong>Вопрос №3:<span class="zir">*</sspan></strong> З наведених прикладів програмного забезпечення вкажіть ті, що входять до прикладного програмне забезпечення:</p>
			<p><input type="checkbox" name="question3_1" value="операційна система"> операційна система</p>
			<p><input type="checkbox" name="question3_2" value="драйвери"> драйвери</p>
			<p><input type="checkbox" name="question3_3" value="браузери"> браузери</p>
			<p><input type="checkbox" name="question3_4" value="програми для обміну миттєвими повідомленнями"> програми для обміну миттєвими повідомленнями</p>
			<p><input type="checkbox" name="question3_5" value="утиліти"> утиліти</p>
			<p><input type="checkbox" name="question3_6" value="графічні редактори"> графічні редактори</p>
			<p><input type="checkbox" name="question3_7" value="середовища програмування"> середовища програмування</p>
		</div><br>
		<div class="element">
			<p class="test_question"><strong>Вопрос №4:</strong> З наведених прикладів програмного забезпечення вкажіть ті, що входять до службових програм:</p>
			<p><input type="radio" name="question4" value="операційна система"> операційна система</p>
			<p><input type="radio" name="question4" value="драйвери"> драйвери</p>
			<p><input type="radio" name="question4" value="браузери"> браузери</p>
			<p><input type="radio" name="question4" value="програми для обміну миттєвими повідомленнями"> програми для обміну миттєвими повідомленнями</p>
			<p><input type="radio" name="question4" value="утиліти"> утиліти</p>
			<p><input type="radio" name="question4" value="графічні редактори"> графічні редактори</p>
			<p><input type="radio" name="question4" value="середовища програмування"> середовища програмування</p>
		</div><br>
		<div class="element">
			<p class="test_question"><strong>Вопрос №5:</strong> Базовий комплект програмного забезпечення, що виконує управління апаратним забезпеченням комп’ютера, забезпечує керування обчислювальним процесом і організовує взаємодію з користувачем називається:</p>
			<p><input type="radio" name="question5" value="драйвером"> драйвером</p>
			<p><input type="radio" name="question5" value="операційною системою"> операційною системою</p>
			<p><input type="radio" name="question5" value="прикладною програмою"> прикладною програмою</p>
			<p><input type="radio" name="question5" value="службовою програмою"> службовою програмою</p>
			<p><input type="radio" name="question5" value="утилітами"> утилітами</p>
		</div><br>
		<div class="element">
			<p class="test_question"><strong>Вопрос №6:</strong> Іменований впорядкований набір даних на пристрої зберігання інформації називається:</p>
			<p><input type="radio" name="question6" value="папкою"> папкою</p>
			<p><input type="radio" name="question6" value="системною папкою"> системною папкою</p>
			<p><input type="radio" name="question6" value="каталогом"> каталогом</p>
			<p><input type="radio" name="question6" value="файлом"> файлом</p>
			<p><input type="radio" name="question6" value="ярликом"> ярликом</p>
		</div><br>
		<div class="element">
			<p class="test_question"><strong>Вопрос №7:</strong> Середовище, яке забезпечує інтерфейс з користувачем і виконує команди називається:</p>
			<p><input type="radio" name="question7" value="командою"> командою</p>
			<p><input type="radio" name="question7" value="командним інтерпретатором"> командним інтерпретатором</p>
			<p><input type="radio" name="question7" value="інтерпретатором"> інтерпретатором</p>
		</div><br>
		<div class="element">
			<p class="test_question"><strong>Вопрос №8:</strong> Програма, яка забезпечує розпізнання і виконує інструкції називається:</p>
			<p><input type="radio" name="question8" value="командою"> командою</p>
			<p><input type="radio" name="question8" value="командним інтерпретатором"> командним інтерпретатором</p>
			<p><input type="radio" name="question8" value="інтерпретатором"> інтерпретатором</p>
		</div><br>
		<div class="element">
			<p class="test_question"><strong>Вопрос №9:<span class="zir">*</sspan></strong> За своїм призначенням ОС бувають:</p>
			<p><input type="checkbox" name="question9_1" value="універсальні"> універсальні</p>
			<p><input type="checkbox" name="question9_2" value="вбудовані"> вбудовані</p>
			<p><input type="checkbox" name="question9_3" value="багатозадачні"> багатозадачні</p>
			<p><input type="checkbox" name="question9_4" value="стандартні"> стандартні</p>
			<p><input type="checkbox" name="question9_5" value="відкриті"> відкриті</p>
			<p><input type="checkbox" name="question9_6" value="вільні"> вільні</p>
			<p><input type="checkbox" name="question9_7" value="однокористувацькі"> однокористувацькі</p>
		</div><br>
		<div class="element">
			<p class="test_question"><strong>Вопрос №10:</strong> За способом встановлення ОС бувають:</p>
			<p><input type="radio" name="question10" value="універсальні"> універсальні</p>
			<p><input type="radio" name="question10" value="вбудовані"> вбудовані</p>
			<p><input type="radio" name="question10" value="багатозадачні"> багатозадачні</p>
			<p><input type="radio" name="question10" value="стандартні"> стандартні</p>
			<p><input type="radio" name="question10" value="відкриті"> відкриті</p>
			<p><input type="radio" name="question10" value="вільні"> вільні</p>
			<p><input type="radio" name="question10" value="однокористувацькі"> однокористувацькі</p>
		</div><br>
		<div class="element">
			<p class="test_question"><strong>Вопрос №11:<span class="zir">*</sspan></strong> За відповідно до можливості доступу ОС бувають:</p>
			<p><input type="checkbox" name="question11_1" value="універсальні"> універсальні</p>
			<p><input type="checkbox" name="question11_2" value="вбудовані"> вбудовані</p>
			<p><input type="checkbox" name="question11_3" value="багатозадачні"> багатозадачні</p>
			<p><input type="checkbox" name="question11_4" value="стандартні"> стандартні</p>
			<p><input type="checkbox" name="question11_5" value="відкриті"> відкриті</p>
			<p><input type="checkbox" name="question11_6" value="вільні"> вільні</p>
			<p><input type="checkbox" name="question11_7" value="пропрієтарні"> пропрієтарні</p>
		</div><br>
		<div class="element">
			<p class="test_question"><strong>Вопрос №12:</strong> Який типовий інтерфейс притаманний сучасним ОС?</p>
			<p><input type="radio" name="question12" value="командний"> командний</p>
			<p><input type="radio" name="question12" value="графічний"> графічний</p>
			<p><input type="radio" name="question12" value="текстовий"> текстовий</p>
		</div><br>
		<div class="element">
			<p class="test_question"><strong>Вопрос №13:</strong> Вказати прикладний програмний засіб, що призначений для створення і обробки графічного зображення:</p>
			<p><input type="radio" name="question13" value="текстовий процесор"> текстовий процесор</p>
			<p><input type="radio" name="question13" value="векторні графічні редактори"> векторні графічні редактори</p>
			<p><input type="radio" name="question13" value="графічні редактори"> графічні редактори</p>
			<p><input type="radio" name="question13" value="СУБД"> СУБД</p>
		</div><br>
		<div class="element">
			<p class="test_question"><strong>Вопрос №14:</strong> Який прикладний програмний засіб об’єднує в собі можливість графічного та текстового редактора та призначене для створення Web- сторінок?</p>
			<p><input type="radio" name="question14" value="текстовий процесор"> текстовий процесор</p>
			<p><input type="radio" name="question14" value="графічний редактор"> графічний редактор</p>
			<p><input type="radio" name="question14" value="видавнича система"> видавнича система</p>
			<p><input type="radio" name="question14" value="редактор HTML"> редактор HTML</p>
			<p><input type="radio" name="question14" value="браузери"> браузери</p>
		</div><br>
		<div class="element">
			<p class="test_question"><strong>Вопрос №15:</strong> Який прикладний програмний засіб призначений для аналізу даних, що містяться в базах знань, і видачі результатів при запиті користувача?</p>
			<p><input type="radio" name="question15" value="СУБД"> СУБД</p>
			<p><input type="radio" name="question15" value="експертні системи"> експертні системи</p>
			<p><input type="radio" name="question15" value="браузери"> браузери</p>
			<p><input type="radio" name="question15" value="фінансові аналітичні системи"> фінансові аналітичні системи</p>
		</div><br>
		<div class="element">
			<p class="test_question"><strong>Вопрос №16:</strong> Програма для стиснення та об’єднання файлів називається:</p>
			<p><input type="radio" name="question16" value="антивірус"> антивірус</p>
			<p><input type="radio" name="question16" value="архіватор"> архіватор</p>
			<p><input type="radio" name="question16" value="компілятор"> компілятор</p>
			<p><input type="radio" name="question16" value="інтерпретатор"> інтерпретатор</p>
		</div><br>
		<div class="element">
			<p class="test_question"><strong>Вопрос №17:<span class="zir">*</sspan></strong> Вказати, які типи вірусів бувають:</p>
			<p><input type="checkbox" name="question17_1" value="завантажувальні"> завантажувальні</p>
			<p><input type="checkbox" name="question17_2" value="файлові"> файлові</p>
			<p><input type="checkbox" name="question17_3" value="мережеві"> мережеві</p>
			<p><input type="checkbox" name="question17_4" value="деструктивні"> деструктивні</p>
			<p><input type="checkbox" name="question17_5" value="макровіруси"> макровіруси</p>
			<p><input type="checkbox" name="question17_6" value="віруси-невидимки"> віруси-невидимки</p>
		</div><br>
		
		<div class="element">
			<p><input type="submit" value="Надіслати відповіді" id="submit" /></p>
		</div>
	</form>
			<div class="cleaner"></div>
		</div>
		
	</div> <!-- end of wrapper -->
</div>

<div id="templatemo_footer_wrapper">
    <div id="templatemo_footer">
    
        <a href="../index.php">Головна</a> | <a href="../lectures.php">Лекції</a> | <a href="../lab_works.php">Лабораторні роботи</a> | <a href="../other.php">Інше</a> | <a href="../contact.php">Довідка</a><br /><br />

        ХДУ © 2017 <a href="http://ksuonline.kspu.edu/?lang=ru">KSU Online</a> | NewLife <a href="http://www.kspu.edu/default.aspx?lang=uk" target="_parent">ХДУ</a>
    
    </div> <!-- end of templatemo_footer -->
</div>

</body>
</html>