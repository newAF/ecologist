<?php 
	require "include/db.php"
 ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><Інше.Ecologist</title>
<script>document.write('<script src="http://' + (location.host || 'localhost').split(':')[0] + ':35729/livereload.js?snipver=1"></' + 'script>')</script>
<meta name="keywords" content="green home, services, free web template, free templates, website templates, CSS, HTML" />
<meta name="description" content="Green Home Services - free css template provided by templatemo.com" />
<link href="templatemo_style.css" rel="stylesheet" type="text/css" />

</head>
<body>

<div id="templatemo_document_wrapper">
	<div id="templatemo_wrapper">
	
		<?php include'include/menu_other.php' ?>
		
		<div id="templatemo_header">
			<div id="site_title">
				<h1><img src="images/templatemo_logo.png" alt="Logo" /></h1>
			</div> <!-- end of site_title -->    
			<div id="header_content">
				<p>
					"Головна схильність людини спрямована на те, що відповідає природі."<br>
					<em><span class="autor_1">-Цицерон-</span></em>  </p>
				
			</div>
		</div>
		
		<div id="templatemo_main">
			<span class="zagolovok"><strong>Питання, що виносяться на залік 
				з дисципліни «Інформатика та системологія».</strong></span>
			<div class="cleaner_h30"></div>
		
			<div id="gallery">

				<div class="gallery_box gb_even">
					<div class="left">
						 <img src="images/img_s.png"/>
					</div>
					<div class="right">
					
						<div><a  style="text-decoration: none;"  href="other/other_1.php">
							<h5>Питання, що виносяться на залік з дисципліни «Інформатика та системологія»</h5></a>
						</div>
						<div class="button">
							<a href="other/other_1.php" class="button_d"/>Детальніше</a>
							<a href="download\Questions_to_test\Питання на залік інф 2014.doc" class="button_d" download/>Завантажити</a>  
						</div>
					</div>
					<div class="cleaner"></div>
				</div>
					
				<div class="gallery_box gb_odd">
					<div class="left">
						<img src="images/img_s.png">
					</div>
					<div class="right">
					<div><a  style="text-decoration: none;"  href="other/other_2.php"><br>
					  <h5>Питання на залік.</h5></a><br>
					  </div>
					   <div class="button">
							<a href="other/other_2.php" class="button_d"/>Детальніше</a>
							<a href="download\Questions_to_test\2016_Питання Залик_ИнфСист_екологи.doc" class="button_d" download/>Завантажити</a>  
						</div>
					</div>
					<div class="cleaner"></div>
				</div>

				<div class="gallery_box gb_even">
					<div class="left">
						 <img src="images/img_s.png"/>
					</div>
					<div class="right">
					
						<div>
							<h5>Інформаційні технології в екології: Базовий курс</h5></a><br>
						</div>
						<div class="button">
							<a href="download\Questions_to_test\ИТ_Екол_2010.doc" class="button_d" download/>Завантажити</a>  
						</div>
					</div>
					<div class="cleaner"></div>
				</div>
					
				<div class="gallery_box gb_odd">
					<div class="left">
						<img src="images/img_s.png">
					</div>
					<div class="right">
					<div>
					  <h5>Матеріали для самостійного опрацювання з дисципліни “Інформатика і системологія”</h5></a>
					  </div>
					   <div class="button">
							<a href="download\Questions_to_test\2016_ИнфСист_СамРоб.doc" class="button_d" download/>Завантажити</a>  
						</div>
					</div>
					<div class="cleaner"></div>
				</div>

				<div class="gallery_box gb_even">
					<div class="left">
						 <img src="images/img_s.png"/>
					</div>
					<div class="right">
					
						<div>
							<h5>Анотації лекцій з дисципліни «Інформатика та системологія».</h5></a><br>
						</div>
						<div class="button">
							<a href="download\Questions_to_test\Анотації лекцій Інформатика та  системологія.docx" class="button_d" download/>Завантажити</a>  
						</div>
					</div>
					<div class="cleaner"></div>
				</div>
					
				<div class="gallery_box gb_odd">
					<div class="left">
						<img src="images/img_s.png">
					</div>
					<div class="right">
					<div>
					  <h5>Перелік тем для підготовки реферативних повідомлень з дисципліни «Інформатика і системологія »</h5></a>
					  </div>
					   <div class="button">
							<a href="download\Questions_to_test\2016_ИнфСист_СамРоб.doc" class="button_d" download/>Завантажити</a>  
						</div>
					</div>
					<div class="cleaner"></div>
				</div>

			</div>
		</div>




		<div id="templatemo_main">
			<span class="zagolovok"><strong>Результати тестування.</strong></span>
			<div class="cleaner_h30"></div>
		
			<div id="gallery">

				<div class="gallery_box gb_even">
					<div class="left">
						 <img src="images/rating.png"/>
					</div>
					<div class="right">
					
						<div>
							<h5>Тест 1</h5>
						</div>
						<h6><?php $title = R::load('tests', 1)->title; echo $title; ?></h6>
						<div class="button">
							<a href="other/rating_test_1.php" class="button_d"/>Детальніше</a>
						</div>
					</div>
					<div class="cleaner"></div>
				</div>
					
				<div class="gallery_box gb_odd">
					<div class="left">
						<img src="images/rating.png">
					</div>
					<div class="right">
					<div>
					  <h5>Тест 2</h5>
					  </div>
					  <h6><?php $title = R::load('tests', 2)->title; echo $title; ?></h6>
					   <div class="button">
							<a href="other/rating_test_2.php" class="button_d"/>Детальніше</a>
						</div>
					</div>
					<div class="cleaner"></div>
				</div>

				<div class="gallery_box gb_even">
					<div class="left">
						 <img src="images/rating.png"/>
					</div>
					<div class="right">
					
						<div>
							<h5>Тест 3</h5>
						</div>
						<div class="button">
							<a href="other/rating_test_3.php" class="button_d"/>Детальніше</a>
						</div>
					</div>
					<div class="cleaner"></div>
				</div>
					
				<div class="gallery_box gb_odd">
					<div class="left">
						<img src="images/rating.png">
					</div>
					<div class="right">
					<div>
					  <h5>Тест 4</h5>
					  </div>
					   <div class="button">
							<a href="other/rating_test_4.php" class="button_d"/>Детальніше</a>
						</div>
					</div>
					<div class="cleaner"></div>
				</div>

				<div class="gallery_box gb_even">
					<div class="left">
						 <img src="images/rating.png"/>
					</div>
					<div class="right">
					
						<div>
							<h5>Тест 5</h5>
						</div>
						<div class="button">
							<a href="other/rating_test_5.php" class="button_d"/>Детальніше</a>
						</div>
					</div>
					<div class="cleaner"></div>
				</div>
					
				<div class="gallery_box gb_odd">
					<div class="left">
						<img src="images/rating.png">
					</div>
					<div class="right">
					<div>
					  <h5>Тест 6</h5>
					  </div>
					   <div class="button">
							<a href="other/rating_test_6.php" class="button_d"/>Детальніше</a>
						</div>
					</div>
					<div class="cleaner"></div>
				</div>

				<div class="gallery_box gb_even">
					<div class="left">
						 <img src="images/rating.png"/>
					</div>
					<div class="right">
					
						<div>
							<h5>Тест 7</h5>
						</div>
						<div class="button">
							<a href="other/rating_test_7.php" class="button_d"/>Детальніше</a>
						</div>
					</div>
					<div class="cleaner"></div>
				</div>
					
				<div class="gallery_box gb_odd">
					<div class="left">
						<img src="images/rating.png">
					</div>
					<div class="right">
					<div>
					  <h5>Тест 8</h5>
					  </div>
					   <div class="button">
							<a href="other/rating_test_8.php" class="button_d"/>Детальніше</a>
						</div>
					</div>
					<div class="cleaner"></div>
				</div>

			</div>
			<div class="cleaner"></div>
		</div>
	</div> <!-- end of wrapper -->
</div>



<div id="templatemo_footer_wrapper">
    <div id="templatemo_footer">
    
        <a href="index.php">Головна</a> | <a href="lectures.php">Лекції</a> | <a href="lab_works.php">Лабораторні роботи</a> | <a href="other.php">Інше</a> | <a href="contact.php">Довідка</a><br /><br />

        ХДУ © 2017 <a href="http://ksuonline.kspu.edu/?lang=ru">KSU Online</a> | NewLife <a href="http://www.kspu.edu/default.aspx?lang=uk" target="_parent">ХДУ</a>
    
    </div> <!-- end of templatemo_footer -->
</div>

</body>
</html>