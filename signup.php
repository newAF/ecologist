<?php 
    require "include/db.php"
 ?>
 <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Реєстрація.Ecologist</title>
<script>document.write('<script src="http://' + (location.host || 'localhost').split(':')[0] + ':35729/livereload.js?snipver=1"></' + 'script>')</script>
<meta name="keywords" content="green home, contact form, contact page, submit button, CSS, HTML" />
<meta name="description" content="Green Home Theme - Contact Page, Contact Form, Submit Button" />
<link href="templatemo_style.css" rel="stylesheet" type="text/css" />

</head>
<body>

<div id="templatemo_document_wrapper">
<div id="templatemo_wrapper">

	<?php include'include/menu_signup.php' ?>
    
	<div id="templatemo_header">
        <div id="site_title">
	        <h1><img src="images/templatemo_logo.png" alt="Logo" /></a></h1>
        </div> <!-- end of site_title -->    
        <div id="header_content">
                <p>
                    "Головна схильність людини спрямована на те, що відповідає природі."<br>
                    <em><span class="autor_1">-Цицерон-</span></em>  </p>
            </div>
    </div>
        
        <div id="templatemo_main">
            <div class="signup">
                <h4><strong style="color: green;">Введіть дані щоб зареєструватися</strong></h4>
        
                <?php
                $data = $_POST;
                if ( isset($data['do_signup']) ) {
                    # code...

                    $errors = array();
                    if (trim($data['name']) == '') 
                    {
                        $errors[] = "<strong>Введіть Ім'я та Прізвище</strong>";
                    }

                    if (trim($data['email']) == '') 
                    {
                        $errors[] = '<strong>Введіть номер вашої групи</strong>';
                    }

                    if (trim($data['id_group']) == '') 
                    {
                        $errors[] = '<strong>Введіть Email</strong>';
                    }

                    if ($data['password'] == '') 
                    {
                        $errors[] = '<strong>Введіть пароль</strong>';
                    }

                    if ($data['password_2'] != $data['password']) 
                    {
                        $errors[] = '<strong>Паролі не співпадають</strong>';
                    }

                    if (R::count('users', "surname = ?", array($data['surname'])) > 0 ) 
                    {
                        $errors[] = '<strong>Користувач з таким іменем та прізвищем вже існує</strong>';
                    }

                    if (R::count('users', "email = ?", array($data['email'])) > 0 ) 
                    {
                        $errors[] = '<strong>Користувач з таким email вже існує</strong>';
                    }

                    if (empty($errors)) 
                    {
                        # регестрируем
                        $user = R::dispense('users');
                        $user->name = $data['name'];
                        $user->surname = $data['surname'];
                        $user->email = $data['email'];
                        $user->password = password_hash($data['password'], PASSWORD_DEFAULT);
                        $user->lvl_a = 1;
                        $user->id_group = $data['id_group'];
                        $user->number_book;
                        R::store($user);
                        echo '<div style="color: green;"><strong>Ви успішно зареєструвалися. Ввійти на свій обліковий запис <a href="/login.php" class="button_d"/>Вхід</a></strong></div><hr>';
                    } else 
                    {
                        echo '<div style="color: red;"><img src="images/errors.png" style="vertical-align:middle; margin-right: 5px;">'.array_shift($errors).'</div><hr>';
                    }
                }
                 ?>
                <content>
                    <form action="/signup.php" method="POST">
                    
                        <label for="username">Ваше Ім'я:</label>
                        <input type="text" name="name" placeholder="Ім'я" value="<?php echo @$data['name']; ?>">

                        <label for="username">Ваше Прізвище:</label>
                        <input type="text" name="surname" placeholder="Прізвище" value="<?php echo @$data['name']; ?>">

                        <label for="id_group">Номер вашої групи:</label>
                        <input type="text" required pattern="[0-9]{,3}" name="id_group" placeholder="Група"  maxlength=4 value="<?php echo @$data['id_group']; ?>">
                   
                        <label for="email">Ваш Email:</label>
                        <input type="email" name="email" placeholder="Email" value="<?php echo @$data['email']; ?>">
                    
                        <label for="password">Ваш пароль:</label>
                        <input type="password" name="password" placeholder="Пароль" value="<?php echo @$data['password']; ?>">
                    
                        <label for="password">Введіть ваш пароль ще раз:</label>
                        <input type="password" name="password_2" placeholder="Пароль">
                    
                        <button class="button_d" type="submit" name="do_signup">Зареєструватися</button>
                        <a href="login.php" class="button_d"/>Вхід</a>
                    </form>
                </content>
              
                <div class="cleaner"></div>
        </div>
        
</div> <!-- end of wrapper -->
</div>
        

    <div id="templatemo_footer_wrapper">
    <div id="templatemo_footer">
    
        <a href="home.html">Головна</a> | <a href="lectures.html">Лекції</a> | <a href="lab_works.html">Лабораторні роботи</a> | <a href="info.html">Інше</a> | <a href="contact.html">Довідка</a><br /><br />

        Copyright © 2017 <a href="#">Your Company Name</a> | Designed by <a href="http://www.templatemo.com" target="_parent">Free CSS Templates</a>
    
    </div> <!-- end of templatemo_footer -->
</div>

</body>
</html>
</html>